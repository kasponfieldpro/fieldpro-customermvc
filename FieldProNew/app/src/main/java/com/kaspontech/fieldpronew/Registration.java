package com.kaspontech.fieldpronew;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Registration extends AppCompatActivity implements Spinner.OnItemSelectedListener{


    public   EditText  name,email,contact_num,alt_contact_num,model_num,serial_num,door_no,street,town,landmark,city,state,country,pincode;
    private Spinner company,product,category;
    private ArrayList<String> companyNameList,companyIDList,productIDList,productNameList,categoryNameList,categoryIDList;
    private String KEY_CompanyID = "company_id";
    private String KEY_ProductID = "product_id";
    private String KEY_CategoryID = "cat_id";
    private Button register;
    public static String companyID,productID,categoryID;

    public static  String username,email_id,contact_Num,alt_num,model_Num,serial_Num,door,streetValue,townValue,landValue,cityValue,stateValue,countryValue,pincodeValue;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        companyNameList = new ArrayList<String>();
        companyIDList = new ArrayList<String>();

        productIDList = new ArrayList<String>();
        productNameList = new ArrayList<String>();

        categoryIDList = new ArrayList<String>();
        categoryNameList = new ArrayList<String>();


        name                = (EditText)findViewById(R.id.full_name_input);
        email               = (EditText)findViewById(R.id.email_input);
        contact_num         = (EditText)findViewById(R.id.contact_input);
        alt_contact_num     = (EditText)findViewById(R.id.atl_contact_input);
        model_num           = (EditText)findViewById(R.id.model_number_input);
        serial_num          = (EditText)findViewById(R.id.serial_number_input);
        door_no             = (EditText)findViewById(R.id.door_no_input);
        street              = (EditText)findViewById(R.id.street_input);
        town                = (EditText)findViewById(R.id.town_input);
        landmark            = (EditText)findViewById(R.id.landmark_input);
        city                = (EditText)findViewById(R.id.city_input);
        state               = (EditText)findViewById(R.id.state_input);
        country             = (EditText)findViewById(R.id.country_input);
        pincode             = (EditText)findViewById(R.id.pincode_input);
        register            = (Button)findViewById(R.id.register);
        company             = (Spinner)findViewById(R.id.company_spinner);
        product             = (Spinner)findViewById(R.id.product_spinner);
        category            = (Spinner)findViewById(R.id.category_spinner);


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();

                if(validate()){
                    Register();
                }

            }
        });

        company.setOnItemSelectedListener(this);
        product.setOnItemSelectedListener(this);
        category.setOnItemSelectedListener(this);

        getCompany();

    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


        Spinner spinner = (Spinner) adapterView;

        switch (spinner.getId()){

            case R.id.company_spinner:
                companyID = companyIDList.get(i);
                getProduct(companyID);
                break;
            case R.id.product_spinner:
                productID = productIDList.get(i);
                getCategory(productID,companyID);
                break;
            case R.id.category_spinner:
                categoryID = categoryIDList.get(i);
                break;

        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


/*-----------------------------------------Company API-------------------------------------------------------------------------------------------------------------*/
    private void getCompany() {
            StringRequest stringRequest = new StringRequest(Request.Method.POST,API.LoadCompany_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {

                                JSONArray jary = new JSONArray(response);


                                for (int i = 0; i < jary.length(); i++) {
                                    JSONObject restatusobj = jary.getJSONObject(i);
                                    String companyID = restatusobj.getString("company_id");
                                    String companyName = restatusobj.getString("company_name");
                                    companyNameList.add(companyName);
                                    companyIDList.add(companyID);
                                 }

                                ArrayAdapter<String> adp1 = new ArrayAdapter<String>(Registration.this,android.R.layout.simple_spinner_dropdown_item, companyNameList);
                                adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                company.setAdapter(adp1);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                                Toast.makeText(Registration.this, error+ "Something went wrong please try again later ! ", Toast.LENGTH_SHORT).show();
                        }
                    })
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<String, String>();
                    return map;
                }
            };

            stringRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        }




    /*-----------------------------------------Product API-------------------------------------------------------------------------------------------------------------*/


    private void getProduct(final String companyID) {
        StringRequest sr = new StringRequest(Request.Method.POST,API.LoadProduct_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        String productName,productID;

                        try {

                            JSONObject jobj = new JSONObject(response);
                            int status = jobj.getInt("status");

                            if(status == 1){


//                                Toast.makeText(Registration.this, "product"+ response, Toast.LENGTH_LONG).show();
                                productNameList.clear();
                                JSONArray jaryproduct = jobj.getJSONArray("result");
                                for (int i = 0; i < jaryproduct.length(); i++) {
                                    JSONObject jproduct = jaryproduct.getJSONObject(i);
                                    productName = jproduct.getString("product_name");
                                    productID = jproduct.getString("product_id");
                                    productNameList.add(productName);
                                    productIDList.add(productID);

                                }

                                ArrayAdapter<String> adp2 = new ArrayAdapter<String>(Registration.this,android.R.layout.simple_spinner_dropdown_item, productNameList);
                                adp2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                product.setAdapter(adp2);

                            } else{

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(Registration.this, error+ "Something went wrong please try again later ! ", Toast.LENGTH_SHORT).show();


                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put(KEY_CompanyID, companyID);
                return map;
            }
        };

        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(sr);

    }





    /*-----------------------------------------Category API-------------------------------------------------------------------------------------------------------------*/



    private void getCategory(final String productID,final String companyID) {

        StringRequest sr = new StringRequest(Request.Method.POST,API.LoadCategory_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        String categoryName,categoryID;

                        try {

                            JSONObject jobj1 = new JSONObject(response);
                            int status = jobj1.getInt("status");

                            if(status == 1){

                                categoryNameList.clear();
                                JSONArray jarycat = jobj1.getJSONArray("result");
                                for (int i = 0; i < jarycat.length(); i++) {
                                    JSONObject jcat = jarycat.getJSONObject(i);
                                    categoryName = jcat.getString("cat_name");
                                    categoryID = jcat.getString("cat_id");
                                    categoryNameList.add(categoryName);
                                    categoryIDList.add(categoryID);

                                }

                                ArrayAdapter<String> adp3 = new ArrayAdapter<String>(Registration.this,android.R.layout.simple_spinner_dropdown_item, categoryNameList);
                                /*Toast.makeText(Registration.this, "category" + categoryNameList, Toast.LENGTH_LONG).show();*/
                                adp3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                category.setAdapter(adp3);

                            } else{

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(Registration.this, error+ "Something went wrong please try again later ! ", Toast.LENGTH_SHORT).show();


                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put(KEY_CompanyID, companyID);
                map.put(KEY_ProductID,productID);
                Log.e("svsgv","wsfe"+map);
                return map;
            }
        };

        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(sr);

    }





    /*-----------------------------------------Register API-------------------------------------------------------------------------------------------------------------*/


    private void Register() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,API.Register_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        final AlertDialog.Builder builder1 = new AlertDialog.Builder(Registration.this);
                        builder1.setTitle("Registered Successfully");
                        builder1.setMessage("Login link sent to your Mailid");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent i =new Intent(Registration.this,Login.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });


                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                        try {

                            JSONArray jary = new JSONArray(response);



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(Registration.this, error+ "Something went wrong please try again later ! ", Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put(KEY_CompanyID,companyID);
                map.put(KEY_ProductID,productID);
                map.put(KEY_CategoryID,categoryID);
                map.put("name",username);
                map.put("emailid",email.getText().toString());
                map.put("contact_no",contact_Num);
                map.put("alt_num",alt_num);
                map.put("door_no",door);
                map.put("street",streetValue);
                map.put("town",townValue);
                map.put("landmark",landValue);
                map.put("city",cityValue);
                map.put("state",stateValue);
                map.put("country",countryValue);
                map.put("pincode",pincodeValue);
                map.put("model_no",model_Num);
                map.put("serial_no",serial_Num);
                Log.e("testr","testr"+map);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }



    public boolean validate() {


        boolean valid = true;


        username = name.getText().toString();
        email_id = email.getText().toString();
        contact_Num = contact_num.getText().toString();
        alt_num = alt_contact_num.getText().toString();
        model_Num = model_num.getText().toString();
        serial_Num = serial_num.getText().toString();
        door = door_no.getText().toString();
        streetValue = street.getText().toString();
        townValue = town.getText().toString();
        landValue = landmark.getText().toString();
        cityValue = city.getText().toString();
        stateValue = state.getText().toString();
        countryValue = country.getText().toString();
        pincodeValue = pincode.getText().toString();




        if (alt_num == contact_Num ) {
            alt_contact_num.setError("Contact number and alternate number should not be same");
            valid = false;
        } else {
            alt_contact_num.setError(null);
        }


        if (username.isEmpty() ) {
            name.setError("Enter your name");
            valid = false;
        } else {
            name.setError(null);
        }


        if (email_id.isEmpty() ) {
            email.setError("Enter your email");
            valid = false;
        } else {
            email.setError(null);
        }

        if (!isValidEmail(email_id)) {
            email.setError("Enter valid email address");
            valid = false;
        } else {
            email.setError(null);
        }



        if (door.isEmpty() ) {
            door_no.setError("Enter door number");
            valid = false;
        } else {
            door_no.setError(null);
        }

        if (contact_Num.isEmpty()) {
            contact_num.setError("Enter your contact number");
            valid = false;
        } else {
            contact_num.setError(null);
        }

        if (landValue.isEmpty()) {
            landmark.setError("Enter land mark");
            valid = false;
        } else {
            landmark.setError(null);
        }

        if (streetValue.isEmpty() ) {
            street.setError("Enter street name");
            valid = false;
        } else {
            street.setError(null);
        }

        if (townValue.isEmpty() ) {
            town.setError("Enter town name");
            valid = false;
        } else {
            town.setError(null);
        }


        if (cityValue.isEmpty() ) {
            city.setError("Enter city name");
            valid = false;
        } else {
            city.setError(null);
        }

        if (stateValue.isEmpty() ) {
            state.setError("Enter state name");
            valid = false;
        } else {
            state.setError(null);
        }


        if (pincodeValue.isEmpty() ) {
            pincode.setError("Enter pin code");
            valid = false;
        } else {
            pincode.setError(null);
        }



        if (countryValue.isEmpty() ) {
            country.setError("Enter country name");
            valid = false;
        } else {
            country.setError(null);
        }

        if (model_Num.isEmpty() ) {
            model_num.setError("Enter model no");
            valid = false;
        } else {
            model_num.setError(null);
        }

        if (serial_Num.isEmpty() ) {
            serial_num.setError("Enter serial no");
            valid = false;
        } else {
            serial_num.setError(null);
        }

        return valid;
    }

}
