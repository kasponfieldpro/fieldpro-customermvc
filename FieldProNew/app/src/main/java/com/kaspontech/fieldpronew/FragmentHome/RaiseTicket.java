package com.kaspontech.fieldpronew.FragmentHome;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kaspontech.fieldpronew.API;
import com.kaspontech.fieldpronew.Login;
import com.kaspontech.fieldpronew.Pref_storage;
import com.kaspontech.fieldpronew.R;
import com.kaspontech.fieldpronew.Registration;
import com.kaspontech.fieldpronew.Utility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import static com.kaspontech.fieldpronew.Login.CustomerID;
import static com.kaspontech.fieldpronew.Login.CustomerMail;
import static com.kaspontech.fieldpronew.Login.CustomerName;
import static com.kaspontech.fieldpronew.Login.MobileNumber;
import static com.kaspontech.fieldpronew.Login.alternatenumber;
import static com.kaspontech.fieldpronew.Login.company_id;
import static com.kaspontech.fieldpronew.Login.land;
import static com.kaspontech.fieldpronew.Registration.alt_num;

/**
 * Created by BalajiPrabhu on 9/13/2017.
 */

public class RaiseTicket extends Fragment implements Spinner.OnItemSelectedListener {

    Button attach_image;
    private Uri fileUri;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    public static final String IMAGE_DIRECTORY_NAME = "SpotAudit_Images";
    static String filename = "";
    Context context;
    Spinner product_spinner,sub_category_spinner,contract_type_spinner,call_category_spinner;
    private String KEY_CompanyID = "company_id";
    private String KEY_ProductID = "product_id";
    private String KEY_SubCat = "cat_id";
    private String KEY_cust_id = "cust_id";
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private Button riseticket;
    private ImageView ivImage;
    private String userChoosenTask;
    public static String companyID,productID,subcatID,contractID,calcatID,cat_name,amc_type,model_num;
    public static CheckBox location_yes,location_no;
    Pref_storage pref_storage;
    public static String door_no,street,town,landmark,city,state,country,pincode,imagepath,prob_des;
    public TextView date,time,model_number;
    public EditText prob_dec;
    EditText loc_door_no,loc_street,loc_town,loc_landmark,loc_city,loc_state,loc_country,loc_pincode;


    private ArrayList<String> productIDList,
            productNameList,sub_categoryNameList,sub_categoryIDList,contractTypeIDList,contractTypeName,call_categoryNameList,call_categoryIDList;



    public RaiseTicket() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rise_ticket, container, false);

        attach_image = (Button)view.findViewById(R.id.attach_image_button);
        riseticket = (Button)view.findViewById(R.id.raiseticket);

        imagepath = "";


        model_number = (TextView)view.findViewById(R.id.model_number);
        prob_dec = (EditText)view.findViewById(R.id.problem_description_input);

        product_spinner = (Spinner)view.findViewById(R.id.product_spinner);
        sub_category_spinner = (Spinner)view.findViewById(R.id.sub_category_spinner);
        contract_type_spinner = (Spinner)view.findViewById(R.id.contract_type_spinner);
        call_category_spinner = (Spinner)view.findViewById(R.id.call_category_spinner);
        pref_storage=new Pref_storage();

        location_yes = (CheckBox)view.findViewById(R.id.checkbox_yes);
        location_no = (CheckBox)view.findViewById(R.id.checkbox_no);


        ivImage = (ImageView) view.findViewById(R.id.ivImage);



        productIDList = new ArrayList<String>();
        productNameList = new ArrayList<String>();

        sub_categoryIDList = new ArrayList<String>();
        sub_categoryNameList = new ArrayList<String>();


        contractTypeIDList = new ArrayList<String>();
        contractTypeName = new ArrayList<String>();

        call_categoryNameList = new ArrayList<String>();
        call_categoryIDList = new ArrayList<String>();


        product_spinner.setOnItemSelectedListener(this);
        sub_category_spinner.setOnItemSelectedListener(this);
        contract_type_spinner.setOnItemSelectedListener(this);
        call_category_spinner.setOnItemSelectedListener(this);


        getProductRaiseTicket();


        prob_des = prob_dec.getText().toString();

        attach_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                rl_main.setVisibility(View.GONE);
//                rl_image.setVisibility(View.VISIBLE);
                String BX1 = android.os.Build.MANUFACTURER;
                if (BX1.equalsIgnoreCase("samsung")) {

                  selectImage();
                } else if (BX1.equalsIgnoreCase("asus")) {
                    selectImage();
                } else if (BX1.equalsIgnoreCase("OnePlus")) {
                    selectImage();
                } else if (BX1.equalsIgnoreCase("HTC")) {
                    selectImage();
                } else {
                    selectImage();

                }
            }

        });

        riseticket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                validateRiseTicketsForm();

                if(validateRiseTicketsForm()){
                    registerticket();
                }
            }
        });


        location_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(location_yes.isChecked()){

                    door_no     = pref_storage.getDetail(getActivity(),"door_no");
                    street      = pref_storage.getDetail(getActivity(), "street");
                    town        = pref_storage.getDetail(getActivity(), "town");
                    city        = pref_storage.getDetail(getActivity(), "city");
                    state       = pref_storage.getDetail(getActivity(), "state");
                    country     = pref_storage.getDetail(getActivity(), "country");
                    pincode     = pref_storage.getDetail(getActivity(), "pincode");

                }

                location_no.setChecked(false);
                location_yes.setChecked(true);
            }


        });


        location_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.location_alertbox);
                dialog.setTitle("Enter your Address");



                Button save;

                TextView close;



                loc_door_no             = (EditText)dialog.findViewById(R.id.door_no_input);
                loc_street              = (EditText)dialog.findViewById(R.id.street_input);
                loc_town                = (EditText)dialog.findViewById(R.id.town_input);
                loc_landmark            = (EditText)dialog.findViewById(R.id.landmark_input);
                loc_city                = (EditText)dialog.findViewById(R.id.city_input);
                loc_state               = (EditText)dialog.findViewById(R.id.state_input);
                loc_country             = (EditText)dialog.findViewById(R.id.country_input);
                loc_pincode             = (EditText)dialog.findViewById(R.id.pincode_input);



                save = (Button)dialog.findViewById(R.id.register);
                close = (TextView)dialog.findViewById(R.id.close);

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        location_yes.setChecked(true);
                        location_no.setChecked(false);

                    }
                });


                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        validate();

                        if(validate() == true){
                            door_no    = loc_door_no.getText().toString();
                            street     = loc_street.getText().toString();
                            town       = loc_town.getText().toString();
                            landmark   = loc_landmark.getText().toString();
                            city       = loc_city.getText().toString();
                            state      = loc_state.getText().toString();
                            country    = loc_country.getText().toString();
                            pincode    = loc_pincode.getText().toString();
                            dialog.cancel();
                        }

                    }
                });


                 dialog.show();

                location_no.setChecked(true);
                location_yes.setChecked(false);

            }
        });

        date = (TextView)view.findViewById(R.id.date_id);
        time = (TextView)view.findViewById(R.id.time_id);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                String frmdate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                                date.setText(frmdate);
                                date.setError(null);
                                Log.e("frmdate", "frmdate" + frmdate);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });



        time.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {


                                        Calendar mcurrentTime = Calendar.getInstance();
                                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                                        int minute = mcurrentTime.get(Calendar.MINUTE);
                                        TimePickerDialog mTimePicker;
                                        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                                            @Override
                                            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                                time.setText(selectedHour + ":" + selectedMinute);
                                                time.setError(null);
                                            }
                                        }, hour, minute, true);//Yes 24 hour time
                                        mTimePicker.setTitle("Select Time");
                                        mTimePicker.show();

                                    }

                                });

// builderSingle.show().getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,WindowManager.LayoutParams.WRAP_CONTENT);

// }
//
// });


        return view;

    }

    private boolean validateRiseTicketsForm() {

        boolean valid = true;


        prob_des = prob_dec.getText().toString();

        if(prob_des.isEmpty()){
            prob_dec.setError("Please write the problem description");
            valid = false;
        }else {
            prob_dec.setError(null);
        }

        if ((date.getText().toString()).isEmpty()){
            date.setError("Please select the date");
            valid = false;
        }else{
            date.setError(null);
        }

        if ((time.getText().toString()).isEmpty()){
            time.setError("Please select the date");
            valid = false;
        }else{
            time.setError(null);
        }

       /* if(ivImage.getDrawable() == null){
            if(getActivity()!= null){
                Toast.makeText(getActivity(), "Please add image", Toast.LENGTH_SHORT).show();
            }
            valid = false;
        }*/

        if(location_no.isChecked() || location_yes.isChecked() ){
        }else{
            valid = false;
        }


        return valid;

    }

    private boolean validate() {
        boolean valid = true;





        if (loc_door_no.getText().toString().isEmpty() ) {
            loc_door_no.setError("Enter door number");
            valid = false;
        } else {
            loc_door_no.setError(null);
        }


        if (loc_landmark.getText().toString().isEmpty()) {
            loc_landmark.setError("Enter land mark");
            valid = false;
        } else {
            loc_landmark.setError(null);
        }

        if (loc_street.getText().toString().isEmpty() ) {
            loc_street.setError("Enter street name");
            valid = false;
        } else {
            loc_street.setError(null);
        }

        if (loc_town.getText().toString().isEmpty() ) {
            loc_town.setError("Enter town name");
            valid = false;
        } else {
            loc_town.setError(null);
        }


        if (loc_city.getText().toString().isEmpty() ) {
            loc_city.setError("Enter city name");
            valid = false;
        } else {
            loc_city.setError(null);
        }

        if (loc_state.getText().toString().isEmpty() ) {
            loc_state.setError("Enter state name");
            valid = false;
        } else {
            loc_state.setError(null);
        }


        if (loc_pincode.getText().toString().isEmpty() ) {
            loc_pincode.setError("Enter pin code");
            valid = false;
        } else {
            loc_pincode.setError(null);
        }

        if (loc_country.getText().toString().isEmpty() ) {
            loc_country.setError("Enter country name");
            valid = false;
        } else {
            loc_country.setError(null);
        }


        return valid;
    }

    private void registerticket() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, API.RaiseTicket,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try {


                            JSONObject jobj = new JSONObject(response);
                            Log.d("zfnsdkfd","sdkfhsdkl"+response);
                            //  Toast.makeText(context, "response"+response, Toast.LENGTH_SHORT).show();
                            int status = jobj.getInt("status");

                            if(status == 1){

                                String message = jobj.getString("msg");
                                AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
                                builder.setTitle("Alert!!!");
                                builder.setMessage(message);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        date.setText("");
                                        time.setText("");
                                         model_number.setText("");
                                        location_yes.setChecked(false);
                                        location_no.setChecked(false);
                                        dialogInterface.dismiss();
                                    }
                                });
                                AlertDialog alt=builder.create();
                                alt.show();

                            } else{
                                String msg = jobj.getString("msg");
                                AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
                                builder.setTitle("Alert!!!");
                                builder.setMessage(msg);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        dialogInterface.dismiss();
                                    }
                                });
                                AlertDialog alt=builder.create();
                                alt.show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

//                        Toast.makeText(getActivity(), "ticket" + error, Toast.LENGTH_SHORT).show();

                        final AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                        builder1.setTitle("Failed");
                        builder1.setMessage("Please make sure that all the fields are filled");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                    }
                                });


                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put(KEY_CompanyID, company_id);
                map.put(KEY_ProductID, productID);
                map.put(KEY_cust_id, CustomerID);
                map.put("name",CustomerName );
                map.put("emailid", CustomerMail);
                map.put("contact_no",MobileNumber );
                map.put("alt_no",alternatenumber );
                map.put("door_no", door_no);
                map.put("street", street);
                map.put("town", town);
                map.put("landmark", land);
                map.put("city", city);
                map.put("state", state);
                map.put("country", country);
                map.put("pincode", pincode);
                map.put("cat_id", subcatID);
                map.put("contract_type",cat_name );
                map.put("pref_date",date.getText().toString() );
                map.put("pref_time",time.getText().toString() );
                map.put("call_tag", calcatID);
                map.put("ticket_image",imagepath);
                map.put("prob_desc",prob_des);
                map.put("model_no",model_num);
                Log.e("testing","test->"+map);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);



    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Spinner spinner = (Spinner) adapterView;

        switch (spinner.getId()){


            case R.id.product_spinner:
                productID = productIDList.get(i);
                getCategoryRaiseTicket(productID);

                break;

            case R.id.sub_category_spinner:
                subcatID = sub_categoryIDList.get(i);
                getContractType();
                getModelNumber(productID,subcatID);
                break;

            case R.id.contract_type_spinner:
                cat_name = contractTypeName.get(i);
                getCallCategoryTicket();
                break;
            case R.id.call_category_spinner:
                calcatID = call_categoryIDList.get(i);
                break;

        }
    }

    private void getModelNumber(final String productID, final String subcatID) {



        StringRequest stringRequest = new StringRequest(Request.Method.POST, API.ModelNumber,
                new Response.Listener<String>() {

            @Override
                    public void onResponse(String response) {
                       /* Toast.makeText(getActivity(), "asc"+response, Toast.LENGTH_SHORT).show();*/

                        try {

                            JSONObject jobj = new JSONObject(response);
                            int status = jobj.getInt("status");

                            if(status == 1){

                                JSONArray jaryproduct = jobj.getJSONArray("result");
                                productNameList.clear();
                                for (int i = 0; i < jaryproduct.length(); i++) {

                                    JSONObject restatusobj = jaryproduct.getJSONObject(i);
                                    model_num = restatusobj.getString("model_no");
                                    model_number.setText(model_num);


                                }

                            } else{

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getActivity(), error+ "Something went wrong please try again later ! ", Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put(KEY_CompanyID, company_id);
                map.put(KEY_cust_id, CustomerID);
                map.put(KEY_ProductID, productID);
                map.put(KEY_SubCat, subcatID);

                Log.e("ModelNumber","ModelNumber->"+map);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);


    }


    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }





    private void getProductRaiseTicket() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, API.RaiseTicket_Product,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       /* Toast.makeText(getActivity(), "asc"+response, Toast.LENGTH_SHORT).show();*/

                        try {

                            JSONObject jobj = new JSONObject(response);
                            int status = jobj.getInt("status");

                            if(status == 1){

                                JSONArray jaryproduct = jobj.getJSONArray("result");
                                productNameList.clear();
                                for (int i = 0; i < jaryproduct.length(); i++) {
                                    JSONObject restatusobj = jaryproduct.getJSONObject(i);
                                    String product_id = restatusobj.getString("product_id");
                                    String product_name = restatusobj.getString("product_name");
                                    productIDList.add(product_id);
                                    productNameList.add(product_name);

                                }

                                if (getActivity()!=null){
                                    ArrayAdapter<String> adp2 = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item, productNameList);
                               /* Toast.makeText(context, "product"+productNameList, Toast.LENGTH_SHORT).show();*/
                                    adp2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    product_spinner.setAdapter(adp2);
                                }



                            } else{

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getActivity(), error+ "Something went wrong please try again later ! ", Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put(KEY_CompanyID, company_id);
                map.put(KEY_cust_id, CustomerID);
//                Log.e("svsgv","wsfe"+map);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }






    private void getCategoryRaiseTicket(final String productID) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, API.RaiseTicket_Category,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jobj = new JSONObject(response);
                            int status = jobj.getInt("status");

                            if (status == 1) {

                                JSONArray jaryproduct = jobj.getJSONArray("result");
                                sub_categoryNameList.clear();
                                for (int i = 0; i < jaryproduct.length(); i++) {
                                    JSONObject restatusobj = jaryproduct.getJSONObject(i);
                                    String sub_cat_id = restatusobj.getString("cat_id");
                                    String sub_cat_name = restatusobj.getString("cat_name");
                                    sub_categoryIDList.add(sub_cat_id);
                                    sub_categoryNameList.add(sub_cat_name);

                                }
                                if (getActivity()!=null){
                                    ArrayAdapter<String> adp2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, sub_categoryNameList);
                                    adp2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    sub_category_spinner.setAdapter(adp2);
                                }




                            } else {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getActivity(), error + "Something went wrong please try again later ! ", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put(KEY_CompanyID, company_id);
                map.put(KEY_ProductID, productID);
                map.put(KEY_cust_id, CustomerID);
                Log.e("hai", "jdjd" + map);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);


    }
    private void getContractType() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, API.RaiseTicket_Contractortype,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

//                            Toast.makeText(getActivity(), "cattype"+response, Toast.LENGTH_SHORT).show();


                            JSONObject jobj = new JSONObject(response);
                            int status = jobj.getInt("status");

                            if(status == 1){

                                JSONArray jaryproduct = jobj.getJSONArray("result");
                                contractTypeName.clear();
                                for (int i = 0; i < jaryproduct.length(); i++) {
                                    JSONObject restatusobj = jaryproduct.getJSONObject(i);
                                     amc_type = restatusobj.getString("amc_type");
                                     contractID = restatusobj.getString("id");
                                    contractTypeIDList.add(contractID);
                                    contractTypeName.add(amc_type);
                                }
                                if(contractTypeName.isEmpty()){
                                    contractTypeName.add("On-Demand");
                                }

                                if (getActivity()!=null){
                                    ArrayAdapter<String> adp2 = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item, contractTypeName);
                                    adp2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    contract_type_spinner.setAdapter(adp2);
                                }


                            } else{

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getActivity(), error+ "Something went wrong please try again later ! ", Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put(KEY_cust_id, CustomerID);
                map.put(KEY_CompanyID, company_id);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }


    private void getCallCategoryTicket() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, API.RaiseTicket_CallCategory,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

//                            Toast.makeText(getActivity(), "cattype"+response, Toast.LENGTH_SHORT).show();


                            JSONObject jobj = new JSONObject(response);
                            int status = jobj.getInt("status");

                            if(status == 1){

                                JSONArray jaryproduct = jobj.getJSONArray("result");
                                call_categoryNameList.clear();
                                for (int i = 0; i < jaryproduct.length(); i++) {
                                    JSONObject restatusobj = jaryproduct.getJSONObject(i);
                                    String call = restatusobj.getString("call");
                                    String id = restatusobj.getString("id");
                                    call_categoryIDList.add(id);
                                    call_categoryNameList.add(call);
                                }
                                if (getActivity()!=null){
                                    ArrayAdapter<String> adp2 = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item, call_categoryNameList);
                                    adp2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    call_category_spinner.setAdapter(adp2);
                                }



                            } else{

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getActivity(), error+ "Something went wrong please try again later ! ", Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put(KEY_CompanyID, company_id);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }








    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(getActivity());

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask ="Choose from Library";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        imagepath=String.valueOf(destination);
        ivImage.setImageBitmap(thumbnail);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Uri selectedImage = data.getData();
        String[] filePath = {MediaStore.Images.Media.DATA};
        Cursor c = getActivity().getContentResolver().query(selectedImage, filePath, null, null, null);

        c.moveToFirst();
        int columnIndex = c.getColumnIndex(filePath[0]);
        String picturePath = c.getString(columnIndex);
        c.close();
        Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
        Log.w("path", picturePath + "" + thumbnail);

        Log.e("path of .","pathfile" + picturePath);
        // viewImage.setImageBitmap(thumbnail);
        imagepath = picturePath;

            ivImage.setImageBitmap(thumbnail);
    }




}

