package com.kaspontech.fieldpronew.FragmentHome;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kaspontech.fieldpronew.API;
import com.kaspontech.fieldpronew.Adapters.ProductAdapter;
import com.kaspontech.fieldpronew.Adapters.TicketHistoryAdapter;
import com.kaspontech.fieldpronew.Bean.Product;
import com.kaspontech.fieldpronew.Bean.Ticket;
import com.kaspontech.fieldpronew.Login;
import com.kaspontech.fieldpronew.Pref_storage;
import com.kaspontech.fieldpronew.R;
import com.kaspontech.fieldpronew.Registration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.kaspontech.fieldpronew.Login.CustomerID;
import static com.kaspontech.fieldpronew.Login.company_id;
import static com.kaspontech.fieldpronew.Registration.categoryID;
import static com.kaspontech.fieldpronew.Registration.companyID;
import static com.kaspontech.fieldpronew.Registration.productID;

/**
 * Created by BalajiPrabhu on 9/13/2017.
 */

public class MyProduct extends Fragment implements Spinner.OnItemSelectedListener{


    View view;
    RecyclerView recyclerView;
    private List<Product> productList = new ArrayList<>();
    private ProductAdapter productAdapter;
    public static Context context;
    Spinner product_spinner,sub_category_spinner;
    private String KEY_CompanyID = "company_id";
    private String KEY_ProductID = "product_id";
    private String KEY_cust_id = "cust_id";
    String categoryName,categoryID;
    String productName,productID,companyName,companyID;
    private ProductAdapter productadapter;
    Product product;
    EditText serial_no,model_no,retailer_name;
    TextView purcharseDate,contract_type_spinner;

    public static String product_name,category,model,serial,contract_type,contract_typeID,contract_typeName,expiry_date,purchase_date,retailerName;

    Button addProduct,confirm,cancel;
    RelativeLayout addproductForm;
    ArrayList<Product> ProductArrayList = new ArrayList<>();
    private ArrayList<String> productNameList,productIDList,sub_categoryNameList,sub_categoryIDList,contractTypeName,companyNameList,companyIDList,exp_dateList;



    public MyProduct() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view  = inflater.inflate(R.layout.fragment_my_product, container, false);
        context = view.getContext();

        addproductForm = (RelativeLayout)view.findViewById(R.id.top_spinner);
        addProduct = (Button)view.findViewById(R.id.add_product);
        confirm = (Button)view.findViewById(R.id.confirm);
        cancel = (Button)view.findViewById(R.id.cancel);

        product_spinner         = (Spinner)view.findViewById(R.id.product_spinner);
        sub_category_spinner    = (Spinner)view.findViewById(R.id.sub_category_spinner);
        contract_type_spinner   = (TextView)view.findViewById(R.id.contract_type_spinner);

        serial_no = (EditText)view.findViewById(R.id.serial_number_input);
        model_no = (EditText)view.findViewById(R.id.model_number_input);
        retailer_name = (EditText)view.findViewById(R.id.retailerName);


        purcharseDate = (TextView) view.findViewById(R.id.purchase_date_id);



        productNameList = new ArrayList<String>();
        productIDList = new ArrayList<String>();
        sub_categoryNameList = new ArrayList<String>();
        sub_categoryIDList = new ArrayList<String>();
        contractTypeName = new ArrayList<String>();
        companyNameList = new ArrayList<String>();
        companyIDList = new ArrayList<String>();
        exp_dateList = new ArrayList<String>();


        product_spinner.setOnItemSelectedListener(this);



        purcharseDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                String frmdate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                                purcharseDate.setText(frmdate);

                                Log.e("frmdate", "frmdate" + frmdate);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });


        addProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                recyclerView.setVisibility(View.GONE);
                addproductForm.setVisibility(View.VISIBLE);
                addProduct.setVisibility(view.GONE);
                getCompany();
                getProduct(companyID);
                /*getCategory(productID,companyID);*/
//                getContractType();

            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkValiadation();

                if(checkValiadation()){
                    getDetails();
                    recyclerView.setVisibility(View.VISIBLE);
                    addproductForm.setVisibility(View.GONE);
                    addProduct.setVisibility(view.VISIBLE);
                }else{

                    final AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                    builder1.setTitle("Failed");
                    builder1.setMessage("Please fill all the fields");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });


                    AlertDialog alert11 = builder1.create();
                    alert11.show();


                }


            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                recyclerView.setVisibility(View.VISIBLE);
                addproductForm.setVisibility(View.GONE);
                addProduct.setVisibility(view.VISIBLE);

            }
        });

        recyclerView = (RecyclerView) view.findViewById(R.id.product_list);
        LinearLayoutManager LayoutManager  = new LinearLayoutManager(getActivity().getBaseContext());
        recyclerView.setLayoutManager(LayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        getProductsall();

        return view;
    }

    public boolean checkValiadation(){

        boolean valid = true;

        serial = serial_no.getText().toString();
        model = model_no.getText().toString();
        retailerName = retailer_name.getText().toString();



        if ((purcharseDate.getText().toString()).isEmpty()) {
            serial_no.setError("Please select the purchase date");
            valid = false;
        } else {
            serial_no.setError(null);
        }


        if (serial.isEmpty() ) {
            serial_no.setError("Please enter serial number");
            valid = false;
        } else {
            serial_no.setError(null);
        }

        if (retailerName.isEmpty() ) {
            retailer_name.setError("Please enter retailer number");
            valid = false;
        } else {
            serial_no.setError(null);
        }

        if (model.isEmpty() ) {
            model_no.setError("Please enter model number");
            valid = false;
        } else {
            model_no.setError(null);
        }

        return valid;
    }



    private void getProductsall() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, API.LoadProduct,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {


                            JSONObject jobj = new JSONObject(response);
                            int status = jobj.getInt("status");

                            if(status == 1){

                                JSONArray jaryproduct = jobj.getJSONArray("result");
                                ProductArrayList.clear();
                                for (int i = 0;i<jaryproduct.length();i++){
                                    product=new Product();
                                    JSONObject restatusobj = jaryproduct.getJSONObject(i);
                                    product_name       = restatusobj.getString("product");
                                    category           = restatusobj.getString("category");
                                    model            = restatusobj.getString("model");
                                    serial           = restatusobj.getString("serial");
                                    contract_type    = restatusobj.getString("contract_type");
                                    expiry_date       = restatusobj.getString("expiry_date");
                                    product.setProduct(product_name);
                                    product.setSub_category(category);
                                    product.setModel_no(model);
                                    product.setContract_type(contract_type);
                                    product.setSerial_number(serial);
                                    ProductArrayList.add(product);
                                   /* Toast.makeText(context, "cbbe"+ProductArrayList.size(), Toast.LENGTH_SHORT).show();*/

                                }
                                if(ProductArrayList!=null) {
                                    productadapter = new ProductAdapter(ProductArrayList, context);
                                    recyclerView.setHasFixedSize(true);
                                    recyclerView.setAdapter(productadapter);
                                    productadapter.notifyDataSetChanged();
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getActivity(), error+ "Something went wrong please try again later ! ", Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put(KEY_CompanyID,company_id);
                map.put(KEY_cust_id,CustomerID);
                Log.e("test","test"+map);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);


    }


//    private void getContractType(){
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, API.ContractType,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//
//                        try {
//
//                            JSONObject jobj = new JSONObject(response);
//                            int status = jobj.getInt("status");
//
//                            if(status == 1){
//                                JSONArray jaryproduct = jobj.getJSONArray("result");
//                                for (int i = 0;i<jaryproduct.length();i++){
//                                    JSONObject restatusobj = jaryproduct.getJSONObject(i);
//
//                                    contract_typeName = restatusobj.getString("amc_type");
//                                    contract_typeID = restatusobj.getString("id");
//                                }
//                            }
//                            if(getActivity()!=null) {
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//
//                        Toast.makeText(getActivity(), error+ "Something went wrong please try again later ! ", Toast.LENGTH_SHORT).show();
//                    }
//                })
//        {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> map = new HashMap<String, String>();
//                return map;
//            }
//        };
//
//        stringRequest.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {
//                return 50000;
//            }
//
//            @Override
//            public int getCurrentRetryCount() {
//                return 50000;
//            }
//
//            @Override
//            public void retry(VolleyError error) throws VolleyError {
//
//            }
//        });
//        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
//        requestQueue.add(stringRequest);
//
//
//    }


    private void getCompany() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,API.LoadCompany_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONArray jary = new JSONArray(response);


                            for (int i = 0; i < jary.length(); i++) {

                                JSONObject restatusobj = jary.getJSONObject(i);
                                 companyID = restatusobj.getString("company_id");
                                 companyName = restatusobj.getString("company_name");
                                companyNameList.add(companyName);
                                companyIDList.add(companyID);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getActivity(), error+ "Something went wrong please try again later ! ", Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                return map;
            }
        };

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }


    private void getProduct(final String companyID) {
        StringRequest sr = new StringRequest(Request.Method.POST,API.LoadProduct_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

/*
                        Toast.makeText(getActivity(), "prod"+response, Toast.LENGTH_SHORT).show();
*/


                        try {

                            JSONObject jobj = new JSONObject(response);
                            int status = jobj.getInt("status");

                            if(status == 1){


//                                Toast.makeText(Registration.this, "product"+ response, Toast.LENGTH_LONG).show();
                                JSONArray jaryproduct = jobj.getJSONArray("result");
                                productNameList.clear();
                                for (int i = 0; i < jaryproduct.length(); i++) {
                                    JSONObject jproduct = jaryproduct.getJSONObject(i);
                                    productName = jproduct.getString("product_name");
                                    productID = jproduct.getString("product_id");
                                    productNameList.add(productName);
                                    productIDList.add(productID);

                                }
                                if(getActivity()!=null) {
                                    ArrayAdapter<String> adp2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, productNameList);
                                    adp2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    product_spinner.setAdapter(adp2);
                                }

                            } else{

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getActivity(), error+ "Something went wrong please try again later ! ", Toast.LENGTH_SHORT).show();


                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put(KEY_CompanyID, company_id);
                Log.e("test","test"+map);

                return map;
            }
        };

        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(sr);

    }








    private void getCategory(final String productID,final String companyID) {

        StringRequest sr = new StringRequest(Request.Method.POST,API.LoadCategory_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try {

                            /*Toast.makeText(getActivity(), "cat"+response, Toast.LENGTH_SHORT).show();*/

                            JSONObject jobj1 = new JSONObject(response);
                            int status = jobj1.getInt("status");

                            if(status == 1){

                                JSONArray jarycat = jobj1.getJSONArray("result");
                                sub_categoryNameList.clear();
                                for (int i = 0; i < jarycat.length(); i++) {
                                    JSONObject jcat = jarycat.getJSONObject(i);
                                    categoryName = jcat.getString("cat_name");
                                    categoryID = jcat.getString("cat_id");
                                    sub_categoryNameList.add(categoryName);
                                    sub_categoryIDList.add(categoryID);

                                }
                                if(getActivity()!=null) {
                                    ArrayAdapter<String> adp3 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, sub_categoryNameList);
                                /*Toast.makeText(Registration.this, "category" + categoryNameList, Toast.LENGTH_LONG).show();*/
                                    adp3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    sub_category_spinner.setAdapter(adp3);
                                }

                            } else{

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getActivity(), error+ "Something went wrong please try again later ! ", Toast.LENGTH_SHORT).show();


                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put(KEY_CompanyID, company_id);
                map.put(KEY_ProductID,productID);
                Log.e("svsgv","wsfe"+map);
                return map;
            }
        };

        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(sr);

    }








    private void getDetails() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, API.AddProductDetails,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Toast.makeText(getActivity(), "API Hit"+response, Toast.LENGTH_SHORT).show();
                        try {

                            final AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                            builder1.setMessage("Added Successfully");
                            builder1.setCancelable(true);

                            builder1.setPositiveButton(
                                    "ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            serial_no.setText("");
                                            model_no.setText("");
                                            retailer_name.setText("");
                                            purcharseDate.setText("");

                                        }
                                    });


                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                            JSONObject jobj = new JSONObject(response);
                            int status = jobj.getInt("status");

                            if(status == 1){

                                JSONArray jaryproduct = jobj.getJSONArray("result");

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getActivity(), error+ "Something went wrong please try again later ! ", Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put(KEY_CompanyID,company_id);
                map.put(KEY_ProductID,productID);
                map.put(KEY_cust_id, CustomerID);
                map.put("cat_id", categoryID);
                map.put("model_no",model_no.getText().toString());
                map.put("serial_no", serial_no.getText().toString());
                map.put("contract_type","Warranty Support");
                map.put("purchase_date",purcharseDate.getText().toString());
                map.put("retailer_name",retailer_name.getText().toString());
                Log.e("test","test"+map);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }




    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        Spinner spinner = (Spinner) adapterView;

        switch (spinner.getId()){

            case R.id.product_spinner:
                productID = productIDList.get(i);
                getCategory(productID,companyID);
                break;
            case R.id.category_spinner:
                categoryID = sub_categoryIDList.get(i);
                break;

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
