package com.kaspontech.fieldpronew;

import android.app.ActionBar;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import static com.kaspontech.fieldpronew.Login.CustomerMail;
import static com.kaspontech.fieldpronew.Login.CustomerName;
import static com.kaspontech.fieldpronew.Login.MobileNumber;
import static com.kaspontech.fieldpronew.Login.land;

public class MyProfile extends AppCompatActivity {

    TextView user_name,mail_id,mobile_number,location;
    String username, mail, mobile, loc,emailin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        user_name=(TextView)findViewById(R.id.user_name);
        mail_id=(TextView)findViewById(R.id.mail_id);
        mobile_number=(TextView)findViewById(R.id.mobile_number);
        location=(TextView)findViewById(R.id.location);



        SharedPreferences pref = this.getSharedPreferences("Login", 0);
        SharedPreferences.Editor editor2 = pref.edit();
        username = pref.getString("CustName", null);
        mail = pref.getString("CustMail", null);
        mobile = pref.getString("Mobile", null);
        loc = pref.getString("Location", null);
        String photo = pref.getString("user_photo", null);
        String reporting = pref.getString("reporter_name", null);

        user_name.setText(CustomerName);
        mail_id.setText(CustomerMail);
        mobile_number.setText(MobileNumber);
        location.setText(land);



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout_profile) {

            new AlertDialog.Builder(this).setIcon(R.drawable.exit).setTitle("Logout")
                    .setMessage("Are you sure you want to Logout?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Intent intent = new Intent(getApplicationContext(),Login.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();

                        }
                    }).setNegativeButton("No", null).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(MyProfile.this,Home.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

}
