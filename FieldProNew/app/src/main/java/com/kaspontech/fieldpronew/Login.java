package com.kaspontech.fieldpronew;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {


    private Button login;
    public static int l = 100;
    TextView signUp;
    TextInputLayout email_layout,password_layout;
    TextInputEditText email, pwd;
    String KEY_USERNAME = "username";
    String KEY_PASSWORD = "password";
    String KEY_REGID = "reg_id";
    Pref_storage pref_storage;
    public  static  String CustomerID,company_id,CustomerName,CustomerMail,MobileNumber,land,alternatenumber,Doorno,Street,Town,Landmark,City,State,country,pincode;
    RelativeLayout login_main_layout;
    String msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        pref_storage = new Pref_storage();




        email_layout = (TextInputLayout) findViewById(R.id.emaillayout);
        password_layout = (TextInputLayout) findViewById(R.id.passwordlayout);


        email = (TextInputEditText) findViewById(R.id.editmail_id);
        pwd = (TextInputEditText) findViewById(R.id.editpwd_id);
        login_main_layout = (RelativeLayout) findViewById(R.id.login_main_layout);

        login = (Button) findViewById(R.id.login_button);
        signUp = (TextView) findViewById(R.id.register);

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this, Registration.class);
                startActivity(intent);
            }
        });
        Resources r = getResources();
        AssetManager assetManager = r.getAssets();

        //First time lauch app in mobile...................................
        final String PREFS_NAME = "MyPreFile";
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);


        if (settings.getBoolean("my_first_time", true)) {
            //the app is being launched for first time, do something
            Log.d("Comments", "First time");
            if (Build.VERSION.SDK_INT >= 23) {
                if (ContextCompat.checkSelfPermission(Login.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Login.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Login.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Login.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.CALL_PHONE},
                            1);
                }
            }


            // first time task
            //**//*  File f = new File(Environment.getExternalStorageDirectory(), "CCD1.xml");*//**//

          /*  enterCCD();*/

            // record the fact that the app has been started at least once
            settings.edit().putBoolean("my_first_time", false).commit();
        }



        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String mail = email.getText().toString();
                String passwd = pwd.getText().toString();

                if (mail.isEmpty()) {
                    email_layout.setError("Please enter your email");
                } else if (passwd.isEmpty()) {
                    password_layout.setError("Please enter your password");
                } else if (isValidEmail(mail)) {
                    loginusingVolley(mail, passwd);
                } else {

                    email_layout.setError("Please enter valid email address");
                }


            }
        });


    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }


    private void loginusingVolley(final String mail, final String passwd) {


        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, API.LOGIN_URL,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();

                      /*  Toast.makeText(Login.this, "" + response, Toast.LENGTH_SHORT).show();*/
                        try {
                            JSONObject jobj = new JSONObject(response);
                         /*   Toast.makeText(Login.this, "" + jobj.toString(), Toast.LENGTH_SHORT).show();*/
                            int st = jobj.getInt("status");


                            try {
                                if (st == 1) {


                                    JSONObject j2obj = jobj.getJSONObject("result");

                                    msg= j2obj.getString("msg");
                                    CustomerID= j2obj.getString("cust_id");
                                    CustomerName = j2obj.getString("cust_name");
                                    CustomerMail = j2obj.getString("emailid");
                                    MobileNumber = j2obj.getString("mobile");
                                    company_id= j2obj.getString("company_id");
                                    String door_no = j2obj.getString("door_no");
                                    String street = j2obj.getString("street");
                                    String town = j2obj.getString("town");
                                    land = j2obj.getString("landmark");
                                    String city = j2obj.getString("city");
                                    String state = j2obj.getString("state");
                                    String country = j2obj.getString("country");
                                    String pincode = j2obj.getString("pincode");
                                    alternatenumber = j2obj.getString("alternate_number");


                                    pref_storage.setDetail(Login.this, "message", msg);
                                    pref_storage.setDetail(Login.this, "CustID", CustomerID);
                                    pref_storage.setDetail(Login.this, "CustName", CustomerName);
                                    pref_storage.setDetail(Login.this, "CustMail", CustomerMail);
                                    pref_storage.setDetail(Login.this, "Mobile", MobileNumber);
                                    pref_storage.setDetail(Login.this, "company_id", company_id);
                                    pref_storage.setDetail(Login.this, "door_no", door_no);
                                    pref_storage.setDetail(Login.this, "street", street);
                                    pref_storage.setDetail(Login.this, "town", town);
                                    pref_storage.setDetail(Login.this, "city", city);
                                    pref_storage.setDetail(Login.this, "state", state);
                                    pref_storage.setDetail(Login.this, "country", country);
                                    pref_storage.setDetail(Login.this, "pincode", pincode);
                                    pref_storage.setDetail(Login.this, "pincode", pincode);


                                    Intent in = new Intent(getApplicationContext(), Home.class);

                                    startActivity(in);
                                    Toast.makeText(Login.this, "Login Successfully", Toast.LENGTH_SHORT).show();
                                    finish();
                                    return;
                                }if(st == 0){
                                    Snackbar snackbar =Snackbar.make(login_main_layout,"Please provide correct details!!!",Snackbar.LENGTH_LONG);
                                    snackbar.setActionTextColor(Color.RED);
                                    View view = snackbar.getView();
                                    TextView textview =(TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                                    textview.setTextColor(Color.WHITE);
                                    snackbar.show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();


                        AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                        builder.setTitle("No Internet Connection");
                        builder.setMessage("You need to have Mobile data or WiFi to access this Application. Press OK to Enable");
                        builder.setIcon(R.drawable.wifi);
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                                startActivity(intent);
                            }
                        });

                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        builder.show();

                    }

                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put(KEY_USERNAME, mail);
                map.put(KEY_PASSWORD, passwd);
                map.put(KEY_REGID, KEY_REGID);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
