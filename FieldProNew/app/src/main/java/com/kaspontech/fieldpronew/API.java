package com.kaspontech.fieldpronew;

/**
 * Created by BalajiPrabhu on 9/15/2017.
 */

public class API {

    public static  String Register_URL = "http://52.35.246.28/fieldpro/index.php?/controller_cust/register";
    public static  String LoadCompany_URL = "http://52.35.246.28/fieldpro/index.php?/controller_cust/load_company";
    public static  String LoadProduct_URL = "http://52.35.246.28/fieldpro/index.php?/controller_cust/load_product";
    public static  String LoadCategory_URL = "http://52.35.246.28/fieldpro/index.php?/controller_cust/load_category";
    public static  String LOGIN_URL = "http://52.35.246.28/fieldpro/index.php?/controller_cust/login";
    public static  String RaiseTicket_Product = "http://52.35.246.28/fieldpro/index.php?/controller_cust/load_cust_product";
    public static  String RaiseTicket_Category = "http://52.35.246.28/fieldpro/index.php?/controller_cust/load_cust_category";
    public static  String RaiseTicket_Contractortype = "http://52.35.246.28/fieldpro/index.php?/controller_cust/load_cust_service_category";
    public static  String RaiseTicket_CallCategory = "http://52.35.246.28/fieldpro/index.php?/controller_cust/load_call_tags";
    public static  String RaiseTicket = "http://52.35.246.28/fieldpro/index.php?/controller_cust/raise_tkt";
    public static  String AddProductDetails = "http://52.35.246.28/fieldpro/index.php?/controller_cust/add_product";
    public static  String LoadTickets = "http://52.35.246.28/fieldpro/index.php?/controller_cust/load_tickets";
    public static  String LoadProduct = "http://52.35.246.28/fieldpro/index.php?/controller_cust/load_customer_details";
    public static  String ContractType = "http://52.35.246.28/fieldpro/index.php?/controller_cust/load_service_category";
    public static  String ModelNumber = "http://52.35.246.28/fieldpro/index.php?/controller_cust/load_model";
    public static  String AMCcontract = "http://52.35.246.28/fieldpro/index.php?/controller_cust/raise_amc";

}
