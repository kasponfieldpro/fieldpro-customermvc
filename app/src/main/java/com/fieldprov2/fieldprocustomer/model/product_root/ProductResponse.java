package com.fieldprov2.fieldprocustomer.model.product_root;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ProductResponse {

	@JsonProperty("response")

	public Response getResponse() {
		return this.response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}
	Response response;

}
