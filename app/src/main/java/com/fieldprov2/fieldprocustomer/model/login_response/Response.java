package com.fieldprov2.fieldprocustomer.model.login_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class   Response{

	@SerializedName("response_code")
	@Expose
	private String responseCode;
	@SerializedName("token")
	@Expose
	private String token;
	@SerializedName("customer_details")
	@Expose
	private CustomerDetails customerDetails;
	@SerializedName("message")
	@Expose
	private String message;

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public CustomerDetails getCustomerDetails() {
		return customerDetails;
	}

	public void setCustomerDetails(CustomerDetails customerDetails) {
		this.customerDetails = customerDetails;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


}