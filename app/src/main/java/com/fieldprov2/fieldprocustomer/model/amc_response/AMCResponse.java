package com.fieldprov2.fieldprocustomer.model.amc_response;

import com.google.gson.annotations.SerializedName;

public class AMCResponse{

	@SerializedName("response")
	private Response response;

	public Response getResponse(){
		return response;
	}
}