package com.fieldprov2.fieldprocustomer.model.location_res;

import com.google.gson.annotations.SerializedName;

public class LocationResponse{

	@SerializedName("response")
	private Response response;

	public Response getResponse(){
		return response;
	}
}