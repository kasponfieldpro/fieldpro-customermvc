package com.fieldprov2.fieldprocustomer.model.registration_res;

import com.google.gson.annotations.SerializedName;

public class RegistrationResponse{

	@SerializedName("response")
	private Response response;

	public Response getResponse(){
		return response;
	}
}