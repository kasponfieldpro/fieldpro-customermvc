package com.fieldprov2.fieldprocustomer.model.sub_product_root;

import com.google.gson.annotations.SerializedName;

public class SubProductResponse{

	@SerializedName("response")
	private Response response;

	public Response getResponse(){
		return response;
	}
}