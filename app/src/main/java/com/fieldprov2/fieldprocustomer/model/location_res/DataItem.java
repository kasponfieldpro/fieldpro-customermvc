package com.fieldprov2.fieldprocustomer.model.location_res;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("location_name")
	private String locationName;

	@SerializedName("location_id")
	private int locationId;

	public String getLocationName(){
		return locationName;
	}

	public int getLocationId(){
		return locationId;
	}
}