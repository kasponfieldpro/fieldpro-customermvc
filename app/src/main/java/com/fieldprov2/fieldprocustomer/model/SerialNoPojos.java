package com.fieldprov2.fieldprocustomer.model;

public class SerialNoPojos {

    private String serial_no;

    public SerialNoPojos(String serial_no) {
        this.serial_no = serial_no;
    }

    public String getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }
}
