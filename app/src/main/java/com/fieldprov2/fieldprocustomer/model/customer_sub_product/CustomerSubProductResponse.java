package com.fieldprov2.fieldprocustomer.model.customer_sub_product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerSubProductResponse {

    @SerializedName("response")
    @Expose
    private Response response;

    /**
     * No args constructor for use in serialization
     *
     */
    public CustomerSubProductResponse() {
    }

    /**
     *
     * @param response
     */
    public CustomerSubProductResponse(Response response) {
        super();
        this.response = response;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

}