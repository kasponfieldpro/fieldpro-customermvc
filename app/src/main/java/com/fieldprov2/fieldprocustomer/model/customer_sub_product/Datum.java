package com.fieldprov2.fieldprocustomer.model.customer_sub_product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("product_sub_id")
    @Expose
    private Integer productSubId;
    @SerializedName("product_sub_name")
    @Expose
    private String productSubName;

    /**
     * No args constructor for use in serialization
     *
     */
    public Datum() {
    }

    /**
     *
     * @param productSubId
     * @param productSubName
     */
    public Datum(Integer productSubId, String productSubName) {
        super();
        this.productSubId = productSubId;
        this.productSubName = productSubName;
    }

    public Integer getProductSubId() {
        return productSubId;
    }

    public void setProductSubId(Integer productSubId) {
        this.productSubId = productSubId;
    }

    public String getProductSubName() {
        return productSubName;
    }

    public void setProductSubName(String productSubName) {
        this.productSubName = productSubName;
    }

}