package com.fieldprov2.fieldprocustomer.model.raiseticket_res;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RaiseTicketResponse
{

    @SerializedName("response")
    @Expose
    private Response response;

    public RaiseTicketResponse(Response response) {
        this.response = response;
    }

    public RaiseTicketResponse() {
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

}
