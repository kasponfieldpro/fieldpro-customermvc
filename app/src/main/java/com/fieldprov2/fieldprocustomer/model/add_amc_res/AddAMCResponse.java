package com.fieldprov2.fieldprocustomer.model.add_amc_res;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddAMCResponse {

    @SerializedName("response")
    @Expose
    private Response response;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

}