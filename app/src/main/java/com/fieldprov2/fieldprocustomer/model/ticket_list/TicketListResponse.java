package com.fieldprov2.fieldprocustomer.model.ticket_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TicketListResponse {

    @SerializedName("response")
    @Expose
    private Response response;

    /**
     * No args constructor for use in serialization
     *
     */
    public TicketListResponse() {
    }

    /**
     *
     * @param response
     */
    public TicketListResponse(Response response) {
        super();
        this.response = response;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

}