package com.fieldprov2.fieldprocustomer.model.ticket_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("ticket_id")
    @Expose
    private String ticketId;
    @SerializedName("product")
    @Expose
    private String product;
    @SerializedName("sub_product")
    @Expose
    private String subProduct;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("serial_no")
    @Expose
    private String serialNo;
    @SerializedName("technicain_name")
    @Expose
    private Object technicainName;
    @SerializedName("technician_number")
    @Expose
    private Object technicianNumber;
    @SerializedName("raised_date_time")
    @Expose
    private String raisedDateTime;
    @SerializedName("work_type")
    @Expose
    private String workType;
    @SerializedName("problem_desc")
    @Expose
    private String problemDesc;
    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("status_name")
    @Expose
    private String statusName;

    /**
     * No args constructor for use in serialization
     *
     */
    public Datum() {
    }

    /**
     *
     * @param subProduct
     * @param product
     * @param technicainName
     * @param workType
     * @param statusName
     * @param model
     * @param problemDesc
     * @param technicianNumber
     * @param ticketId
     * @param serialNo
     * @param raisedDateTime
     * @param statusCode
     */
    public Datum(String ticketId, String product, String subProduct, String model, String serialNo, Object technicainName, Object technicianNumber, String raisedDateTime, String workType, String problemDesc, Integer statusCode, String statusName) {
        super();
        this.ticketId = ticketId;
        this.product = product;
        this.subProduct = subProduct;
        this.model = model;
        this.serialNo = serialNo;
        this.technicainName = technicainName;
        this.technicianNumber = technicianNumber;
        this.raisedDateTime = raisedDateTime;
        this.workType = workType;
        this.problemDesc = problemDesc;
        this.statusCode = statusCode;
        this.statusName = statusName;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getSubProduct() {
        return subProduct;
    }

    public void setSubProduct(String subProduct) {
        this.subProduct = subProduct;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public Object getTechnicainName() {
        return technicainName;
    }

    public void setTechnicainName(Object technicainName) {
        this.technicainName = technicainName;
    }

    public Object getTechnicianNumber() {
        return technicianNumber;
    }

    public void setTechnicianNumber(Object technicianNumber) {
        this.technicianNumber = technicianNumber;
    }

    public String getRaisedDateTime() {
        return raisedDateTime;
    }

    public void setRaisedDateTime(String raisedDateTime) {
        this.raisedDateTime = raisedDateTime;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getProblemDesc() {
        return problemDesc;
    }

    public void setProblemDesc(String problemDesc) {
        this.problemDesc = problemDesc;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

}