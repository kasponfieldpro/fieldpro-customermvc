package com.fieldprov2.fieldprocustomer.model.amc_response;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("duration")
	private int duration;

	@SerializedName("cost")
	private double cost;

	@SerializedName("amc_id")
	private int amcId;

	@SerializedName("amc_type")
	private String amcType;

	@SerializedName("status")
	private int status;

	public int getDuration(){
		return duration;
	}

	public double getCost(){
		return cost;
	}

	public int getAmcId(){
		return amcId;
	}

	public String getAmcType(){
		return amcType;
	}

	public int getStatus(){
		return status;
	}
}