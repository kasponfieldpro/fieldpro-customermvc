package com.fieldprov2.fieldprocustomer.model.profile_activity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileActivityResponse {

    @SerializedName("response")
    @Expose
    private Response response;

    /**
     * No args constructor for use in serialization
     *
     */
    public ProfileActivityResponse() {
    }

    /**
     *
     * @param response
     */
    public ProfileActivityResponse(Response response) {
        super();
        this.response = response;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

}