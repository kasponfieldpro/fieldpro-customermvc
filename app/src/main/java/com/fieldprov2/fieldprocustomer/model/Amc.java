package com.fieldprov2.fieldprocustomer.model;

public class Amc {
    public Amc() {
    }

    public String getProduct() {
        return product;
    }

    public Amc(String product, String subCategory, String amcType, String serialNumber) {
        this.product = product;
        this.subCategory = subCategory;
        this.amcType = amcType;
        this.serialNumber = serialNumber;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getAmcType() {
        return amcType;
    }

    public void setAmcType(String amcType) {
        this.amcType = amcType;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    String product, subCategory, amcType, serialNumber;
}
