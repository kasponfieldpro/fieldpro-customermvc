package com.fieldprov2.fieldprocustomer.model.login_response;

import com.google.gson.annotations.SerializedName;

public class LoginResponse{

	@SerializedName("response")
	private Response response;

	public Response getResponse(){
		return response;
	}


}