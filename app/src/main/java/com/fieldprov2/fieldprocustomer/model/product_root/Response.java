package com.fieldprov2.fieldprocustomer.model.product_root;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Response {
    @JsonProperty("response_code")
    public String getResponse_code() {
        return this.response_code;
    }

    public void setResponse_code(String response_code) {
        this.response_code = response_code;
    }

    String response_code;

    @JsonProperty("data")
    public List<Datum> getData() {
        return this.data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    List<Datum> data;

}