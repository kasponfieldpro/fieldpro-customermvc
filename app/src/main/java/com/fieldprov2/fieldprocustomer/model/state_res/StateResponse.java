package com.fieldprov2.fieldprocustomer.model.state_res;

import com.google.gson.annotations.SerializedName;

public class StateResponse{

	@SerializedName("response")
	private Response response;

	public Response getResponse(){
		return response;
	}
}