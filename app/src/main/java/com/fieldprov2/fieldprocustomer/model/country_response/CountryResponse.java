package com.fieldprov2.fieldprocustomer.model.country_response;

import com.google.gson.annotations.SerializedName;

public class CountryResponse{

	@SerializedName("response")
	private Response response;

	public Response getResponse(){
		return response;
	}
}