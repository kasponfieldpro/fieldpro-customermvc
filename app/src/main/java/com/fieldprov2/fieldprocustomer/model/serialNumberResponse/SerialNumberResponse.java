package com.fieldprov2.fieldprocustomer.model.serialNumberResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SerialNumberResponse {

    @SerializedName("response")
    @Expose
    private Response response;

    /**
     * No args constructor for use in serialization
     *
     */
    public SerialNumberResponse() {
    }

    /**
     *
     * @param response
     */
    public SerialNumberResponse(Response response) {
        super();
        this.response = response;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

}