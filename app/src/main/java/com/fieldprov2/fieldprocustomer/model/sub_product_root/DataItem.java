package com.fieldprov2.fieldprocustomer.model.sub_product_root;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("product")
	private Product product;

	@SerializedName("product_sub_id")
	private int productSubId;

	@SerializedName("product_sub_model")
	private String productSubModel;

	@SerializedName("product_sub_description")
	private String productSubDescription;

	@SerializedName("product_sub_name")
	private String productSubName;

	@SerializedName("product_sub_image")
	private String productSubImage;

	public Product getProduct(){
		return product;
	}

	public int getProductSubId(){
		return productSubId;
	}

	public String getProductSubModel(){
		return productSubModel;
	}

	public String getProductSubDescription(){
		return productSubDescription;
	}

	public String getProductSubName(){
		return productSubName;
	}

	public String getProductSubImage(){
		return productSubImage;
	}
}