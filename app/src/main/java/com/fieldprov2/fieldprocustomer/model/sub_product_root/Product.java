package com.fieldprov2.fieldprocustomer.model.sub_product_root;

import com.google.gson.annotations.SerializedName;

public class Product{

	@SerializedName("product_id")
	private int productId;

	@SerializedName("product_name")
	private String productName;

	public int getProductId(){
		return productId;
	}

	public String getProductName(){
		return productName;
	}
}