package com.fieldprov2.fieldprocustomer.model.login_response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerDetails {

    @SerializedName("customer_code")
    @Expose
    private String customerCode;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("email_id")
    @Expose
    private String emailId;
    @SerializedName("contact_number")
    @Expose
    private String contactNumber;
    @SerializedName("alternate_number")
    @Expose
    private String alternateNumber;
    @SerializedName("plot_number")
    @Expose
    private String plotNumber;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("landmark")
    @Expose
    private String landmark;
    @SerializedName("post_code")
    @Expose
    private Integer postCode;
    @SerializedName("country_id")
    @Expose
    private Integer countryId;
    @SerializedName("country_name")
    @Expose
    private String countryName;
    @SerializedName("state_id")
    @Expose
    private Integer stateId;
    @SerializedName("state_name")
    @Expose
    private String stateName;
    @SerializedName("city_id")
    @Expose
    private Integer cityId;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("location_id")
    @Expose
    private Integer locationId;
    @SerializedName("location_name")
    @Expose
    private String locationName;

    /**
     * No args constructor for use in serialization
     *
     */
    public CustomerDetails() {
    }

    /**
     *
     * @param locationName
     * @param stateId
     * @param customerCode
     * @param emailId
     * @param cityId
     * @param customerName
     * @param countryId
     * @param cityName
     * @param stateName
     * @param street
     * @param locationId
     * @param contactNumber
     * @param plotNumber
     * @param alternateNumber
     * @param postCode
     * @param countryName
     * @param landmark
     */
    public CustomerDetails(String customerCode, String customerName, String emailId, String contactNumber, String alternateNumber, String plotNumber, String street, String landmark, Integer postCode, Integer countryId, String countryName, Integer stateId, String stateName, Integer cityId, String cityName, Integer locationId, String locationName) {
        super();
        this.customerCode = customerCode;
        this.customerName = customerName;
        this.emailId = emailId;
        this.contactNumber = contactNumber;
        this.alternateNumber = alternateNumber;
        this.plotNumber = plotNumber;
        this.street = street;
        this.landmark = landmark;
        this.postCode = postCode;
        this.countryId = countryId;
        this.countryName = countryName;
        this.stateId = stateId;
        this.stateName = stateName;
        this.cityId = cityId;
        this.cityName = cityName;
        this.locationId = locationId;
        this.locationName = locationName;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getAlternateNumber() {
        return alternateNumber;
    }

    public void setAlternateNumber(String alternateNumber) {
        this.alternateNumber = alternateNumber;
    }

    public String getPlotNumber() {
        return plotNumber;
    }

    public void setPlotNumber(String plotNumber) {
        this.plotNumber = plotNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public Integer getPostCode() {
        return postCode;
    }

    public void setPostCode(Integer postCode) {
        this.postCode = postCode;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

}