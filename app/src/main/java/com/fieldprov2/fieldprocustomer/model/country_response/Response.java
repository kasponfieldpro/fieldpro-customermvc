package com.fieldprov2.fieldprocustomer.model.country_response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Response{

	@SerializedName("response_code")
	private String responseCode;

	@SerializedName("data")
	private List<DataItem> data;

	public String getResponseCode(){
		return responseCode;
	}

	public List<DataItem> getData(){
		return data;
	}
}