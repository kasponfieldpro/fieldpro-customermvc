package com.fieldprov2.fieldprocustomer.model;

import java.util.List;

public class AddProductPojo
{

    private String customer_code;
    private String customer_name;
    private String email_id;
    private String contact_number;
    private String alternate_number;
    private Integer product_id;
    private Integer product_sub_id;
    private String model_no;
    private List<SerialNoPojos> serial_array = null;
    private Integer contract_type;
    private Integer contract_duration;
    private String plot_number;
    private String street;
    private Integer post_code;
    private Integer country_id;
    private Integer state_id;
    private Integer city_id;
    private Integer location_id;
    private String landmark;
    private String invoice_id;
    private String cust_preference_date;
    private Integer ammount;
    private Integer contract_period;
    private Integer workTypeId;
    private String priority;

    public String getCustomerCode() {
        return customer_code;
    }

    public void setCustomerCode(String customerCode) {
        this.customer_code = customerCode;
    }

    public String getCustomerName() {
        return customer_name;
    }

    public void setCustomerName(String customerName) {
        this.customer_name = customerName;
    }

    public String getEmailId() {
        return email_id;
    }

    public void setEmailId(String emailId) {
        this.email_id = emailId;
    }

    public String getContactNumber() {
        return contact_number;
    }

    public void setContactNumber(String contactNumber) {
        this.contact_number = contactNumber;
    }

    public String getAlternateNumber() {
        return alternate_number;
    }

    public void setAlternateNumber(String alternateNumber) {
        this.alternate_number = alternateNumber;
    }

    public Integer getProductId() {
        return product_id;
    }

    public void setProductId(Integer productId) {
        this.product_id = productId;
    }

    public Integer getProductSubId() {
        return product_sub_id;
    }

    public void setProductSubId(Integer productSubId) {
        this.product_sub_id = productSubId;
    }

    public String getModelNo() {
        return model_no;
    }

    public void setModelNo(String modelNo) {
        this.model_no = modelNo;
    }

    public List<SerialNoPojos> getSerialArray() {
        return serial_array;
    }

    public void setSerialArray(List<SerialNoPojos> serialArray) {
        this.serial_array = serialArray;
    }

    public Integer getContractType() {
        return contract_type;
    }

    public void setContractType(Integer contractType) {
        this.contract_type = contractType;
    }

    public Integer getContractDuration() {
        return contract_duration;
    }

    public void setContractDuration(Integer contractDuration) {
        this.contract_duration = contractDuration;
    }

    public String getPlotNumber() {
        return plot_number;
    }

    public void setPlotNumber(String plotNumber) {
        this.plot_number = plotNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getPostCode() {
        return post_code;
    }

    public void setPostCode(Integer postCode) {
        this.post_code = postCode;
    }

    public Integer getCountryId() {
        return country_id;
    }

    public void setCountryId(Integer countryId) {
        this.country_id = countryId;
    }

    public Integer getStateId() {
        return state_id;
    }

    public void setStateId(Integer stateId) {
        this.state_id = stateId;
    }

    public Integer getCityId() {
        return city_id;
    }

    public void setCityId(Integer cityId) {
        this.city_id = cityId;
    }

    public Integer getLocationId() {
        return location_id;
    }

    public void setLocationId(Integer locationId) {
        this.location_id = locationId;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getInvoiceId() {
        return invoice_id;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoice_id = invoiceId;
    }

    public String getCustPreferenceDate() {
        return cust_preference_date;
    }

    public void setCustPreferenceDate(String custPreferenceDate) {
        this.cust_preference_date = custPreferenceDate;
    }

    public Integer getAmmount() {
        return ammount;
    }

    public void setAmmount(Integer ammount) {
        this.ammount = ammount;
    }

    public Integer getContractPeriod() {
        return contract_period;
    }

    public void setContractPeriod(Integer contractPeriod) {
        this.contract_period = contractPeriod;
    }

    public Integer getWorkTypeId() {
        return workTypeId;
    }

    public void setWorkTypeId(Integer workTypeId) {
        this.workTypeId = workTypeId;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }
}
