package com.fieldprov2.fieldprocustomer.model.work_type_id;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("work_type_id")
    @Expose
    private Integer workTypeId;
    @SerializedName("work_type")
    @Expose
    private String workType;

    /**
     * No args constructor for use in serialization
     *
     */
    public Datum() {
    }

    /**
     *
     * @param workTypeId
     * @param workType
     */
    public Datum(Integer workTypeId, String workType) {
        super();
        this.workTypeId = workTypeId;
        this.workType = workType;
    }

    public Integer getWorkTypeId() {
        return workTypeId;
    }

    public void setWorkTypeId(Integer workTypeId) {
        this.workTypeId = workTypeId;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

}