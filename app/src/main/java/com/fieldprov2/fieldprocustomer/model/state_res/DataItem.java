package com.fieldprov2.fieldprocustomer.model.state_res;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("state_name")
	private String stateName;

	@SerializedName("state_id")
	private int stateId;

	public String getStateName(){
		return stateName;
	}

	public int getStateId(){
		return stateId;
	}
}