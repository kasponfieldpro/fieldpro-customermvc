package com.fieldprov2.fieldprocustomer.model.get_details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("customer_contract_id")
    @Expose
    private Integer customerContractId;
    @SerializedName("model_no")
    @Expose
    private String modelNo;
    @SerializedName("serial_no")
    @Expose
    private String serialNo;
    @SerializedName("contract_type_id")
    @Expose
    private Integer contractTypeId;
    @SerializedName("contract_type")
    @Expose
    private String contractType;
    @SerializedName("duration")
    @Expose
    private Integer duration;

    public Integer getCustomerContractId() {
        return customerContractId;
    }

    public void setCustomerContractId(Integer customerContractId) {
        this.customerContractId = customerContractId;
    }

    public String getModelNo() {
        return modelNo;
    }

    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public Integer getContractTypeId() {
        return contractTypeId;
    }

    public void setContractTypeId(Integer contractTypeId) {
        this.contractTypeId = contractTypeId;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

}