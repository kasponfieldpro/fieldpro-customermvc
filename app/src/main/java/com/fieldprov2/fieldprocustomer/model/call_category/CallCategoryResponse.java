package com.fieldprov2.fieldprocustomer.model.call_category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CallCategoryResponse {

    @SerializedName("response")
    @Expose
    private Response response;

    /**
     * No args constructor for use in serialization
     *
     */
    public CallCategoryResponse() {
    }

    /**
     *
     * @param response
     */
    public CallCategoryResponse(Response response) {
        super();
        this.response = response;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

}