package com.fieldprov2.fieldprocustomer.model.amc_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {


    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("product")
    @Expose
    private String product;
    @SerializedName("sub_product")
    @Expose
    private String subProduct;
    @SerializedName("amc_type")
    @Expose
    private String amcType;
    @SerializedName("model_no")
    @Expose
    private String modelNo;
    @SerializedName("serial_no")
    @Expose
    private String serialNo;
    @SerializedName("flag")
    @Expose
    private Integer flag;
    @SerializedName("days_left")
    @Expose
    private Integer daysLeft;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getSubProduct() {
        return subProduct;
    }

    public void setSubProduct(String subProduct) {
        this.subProduct = subProduct;
    }

    public String getAmcType() {
        return amcType;
    }

    public void setAmcType(String amcType) {
        this.amcType = amcType;
    }

    public String getModelNo() {
        return modelNo;
    }

    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Integer getDaysLeft() {
        return daysLeft;
    }

    public void setDaysLeft(Integer daysLeft) {
        this.daysLeft = daysLeft;
    }

}