package com.fieldprov2.fieldprocustomer.model.city_res;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("city_name")
	private String cityName;

	@SerializedName("city_id")
	private int cityId;

	public String getCityName(){
		return cityName;
	}

	public int getCityId(){
		return cityId;
	}
}