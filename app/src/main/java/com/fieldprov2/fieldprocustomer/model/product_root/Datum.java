package com.fieldprov2.fieldprocustomer.model.product_root;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Datum {
	@JsonProperty("product_id")
	public int getProduct_id() {
		return this.product_id;
	}
	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
	int product_id;
	@JsonProperty("product_name")
	public String getProduct_name() {
		return this.product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	String product_name;
	@JsonProperty("product_model")
	public String getProduct_model() {
		return this.product_model;
	}
	public void setProduct_model(String product_model) {
		this.product_model = product_model;
	}
	String product_model;
	@JsonProperty("product_description")
	public String getProduct_description() {
		return this.product_description;
	}
	public void setProduct_description(String product_description) {
		this.product_description = product_description;
	}
	String product_description;
	@JsonProperty("product_image")
	public String getProduct_image() {
		return this.product_image;
	}
	public void setProduct_image(String product_image) {
		this.product_image = product_image;
	}
	String product_image;
}
