package com.fieldprov2.fieldprocustomer.model.work_type_id;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WorkTypeResponse {

    @SerializedName("response")
    @Expose
    private Response response;

    /**
     * No args constructor for use in serialization
     *
     */
    public WorkTypeResponse() {
    }

    /**
     *
     * @param response
     */
    public WorkTypeResponse(Response response) {
        super();
        this.response = response;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

}