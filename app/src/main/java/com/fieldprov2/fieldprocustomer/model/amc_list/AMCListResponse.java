package com.fieldprov2.fieldprocustomer.model.amc_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AMCListResponse {

    @SerializedName("response")
    @Expose
    private Response response;

    /**
     * No args constructor for use in serialization
     *
     */
    public AMCListResponse() {
    }

    /**
     *
     * @param response
     */
    public AMCListResponse(Response response) {
        super();
        this.response = response;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

}