package com.fieldprov2.fieldprocustomer.model.country_response;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("country_name")
	private String countryName;

	@SerializedName("country_id")
	private int countryId;

	public String getCountryName(){
		return countryName;
	}

	public int getCountryId(){
		return countryId;
	}
}