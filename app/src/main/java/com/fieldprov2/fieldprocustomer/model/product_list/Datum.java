package com.fieldprov2.fieldprocustomer.model.product_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("product")
    @Expose
    private String product;
    @SerializedName("sub_product")
    @Expose
    private String subProduct;
    @SerializedName("serial_no")
    @Expose
    private String serialNo;
    @SerializedName("model_no")
    @Expose
    private String modelNo;
    @SerializedName("contract_type")
    @Expose
    private String contractType;

    /**
     * No args constructor for use in serialization
     *
     */
    public Datum() {
    }

    /**
     *
     * @param subProduct
     * @param product
     * @param contractType
     * @param modelNo
     * @param serialNo
     */
    public Datum(String product, String subProduct, String serialNo, String modelNo, String contractType) {
        super();
        this.product = product;
        this.subProduct = subProduct;
        this.serialNo = serialNo;
        this.modelNo = modelNo;
        this.contractType = contractType;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getSubProduct() {
        return subProduct;
    }

    public void setSubProduct(String subProduct) {
        this.subProduct = subProduct;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getModelNo() {
        return modelNo;
    }

    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

}