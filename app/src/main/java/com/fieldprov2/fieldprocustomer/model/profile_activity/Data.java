package com.fieldprov2.fieldprocustomer.model.profile_activity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("email_id")
    @Expose
    private String emailId;
    @SerializedName("contact_number")
    @Expose
    private String contactNumber;

    /**
     * No args constructor for use in serialization
     *
     */
    public Data() {
    }

    /**
     *
     * @param contactNumber
     * @param emailId
     * @param customerName
     */
    public Data(String customerName, String emailId, String contactNumber) {
        super();
        this.customerName = customerName;
        this.emailId = emailId;
        this.contactNumber = contactNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

}