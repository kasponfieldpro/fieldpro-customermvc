package com.fieldprov2.fieldprocustomer.model;

public class Tickets {

    String ticketid;
    String ticketRaiseDate;
    String workType;
    String status;

    public Tickets() {
    }

    public Tickets(String ticketid, String ticketRaiseDate, String workType, String status) {
        this.ticketid = ticketid;
        this.ticketRaiseDate = ticketRaiseDate;
        this.workType = workType;
        this.status = status;
    }

    public String getTicketid() {
        return ticketid;
    }

    public void setTicketid(String ticketid) {
        this.ticketid = ticketid;
    }

    public String getTicketRaiseDate() {
        return ticketRaiseDate;
    }

    public void setTicketRaiseDate(String ticketRaiseDate) {
        this.ticketRaiseDate = ticketRaiseDate;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
