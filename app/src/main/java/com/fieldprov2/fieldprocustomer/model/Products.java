package com.fieldprov2.fieldprocustomer.model;

public class Products {

    String product;
    String subProduct;

    public String getProduct() {
        return product;
    }

    public Products() {
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getSubProduct() {
        return subProduct;
    }

    public void setSubProduct(String subProduct) {
        this.subProduct = subProduct;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getModelNo() {
        return modelNo;
    }

    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public Products(String product, String subProduct, String serialNumber, String modelNo, String contractType) {
        this.product = product;
        this.subProduct = subProduct;
        this.serialNumber = serialNumber;
        this.modelNo = modelNo;
        this.contractType = contractType;
    }

    String serialNumber;
    String modelNo;
    String contractType;

}
