package com.fieldprov2.fieldprocustomer.model.call_category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("call_category_id")
    @Expose
    private Integer callCategoryId;
    @SerializedName("call_category")
    @Expose
    private String callCategory;
    @SerializedName("mttr")
    @Expose
    private String mttr;

    /**
     * No args constructor for use in serialization
     *
     */
    public Datum() {
    }

    /**
     *
     * @param mttr
     * @param callCategoryId
     * @param callCategory
     */
    public Datum(Integer callCategoryId, String callCategory, String mttr) {
        super();
        this.callCategoryId = callCategoryId;
        this.callCategory = callCategory;
        this.mttr = mttr;
    }

    public Integer getCallCategoryId() {
        return callCategoryId;
    }

    public void setCallCategoryId(Integer callCategoryId) {
        this.callCategoryId = callCategoryId;
    }

    public String getCallCategory() {
        return callCategory;
    }

    public void setCallCategory(String callCategory) {
        this.callCategory = callCategory;
    }

    public String getMttr() {
        return mttr;
    }

    public void setMttr(String mttr) {
        this.mttr = mttr;
    }

}