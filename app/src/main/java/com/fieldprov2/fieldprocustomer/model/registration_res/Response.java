package com.fieldprov2.fieldprocustomer.model.registration_res;

import com.google.gson.annotations.SerializedName;

public class Response{

	@SerializedName("response_code")
	private String responseCode;

	@SerializedName("message")
	private String message;

	@SerializedName("token")
	private String token;

	public String getResponseCode(){
		return responseCode;
	}

	public String getMessage(){
		return message;
	}

	public String getToken(){
		return token;
	}
}