package com.fieldprov2.fieldprocustomer;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.fieldprov2.fieldprocustomer.controller.LogoutController;
import com.fieldprov2.fieldprocustomer.model.logout.LogoutResponse;
import com.fieldprov2.fieldprocustomer.model.profile_activity.ProfileActivityResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;
import com.fieldprov2.fieldprocustomer.network.RetrofitClient;
import com.fieldprov2.fieldprocustomer.view.MainActivity;
import com.fieldprov2.fieldprocustomer.view.login.LoginActivity;
import com.fieldprov2.fieldprocustomer.view.profile.ProfileActivity;

public class ExitDialogBox extends AppCompatDialogFragment {

    private Activity activity;

    public ExitDialogBox(Activity activity) {
        this.activity = activity;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Field Pro")
                .setMessage("Do You Want Exit The App")
                .setNeutralButton("No", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                dialog.dismiss();
                            }
                        }
                )
                .setPositiveButton("yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        activity.finishAffinity();
                        System.exit(0);
                    }
                });
        return builder.create();
    }



}
