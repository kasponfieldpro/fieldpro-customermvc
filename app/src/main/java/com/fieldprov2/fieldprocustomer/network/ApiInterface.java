package com.fieldprov2.fieldprocustomer.network;


import com.fieldprov2.fieldprocustomer.model.AddProductPojo;
import com.fieldprov2.fieldprocustomer.model.EditCustomer.ProfileDetailsResponse;
import com.fieldprov2.fieldprocustomer.model.add_amc_res.AddAMCResponse;
import com.fieldprov2.fieldprocustomer.model.add_product_res.AddProductResponse;
import com.fieldprov2.fieldprocustomer.model.amc_list.AMCListResponse;
import com.fieldprov2.fieldprocustomer.model.amc_response.AMCResponse;
import com.fieldprov2.fieldprocustomer.model.call_category.CallCategoryResponse;
import com.fieldprov2.fieldprocustomer.model.change_password.ChangePasswordResponse;
import com.fieldprov2.fieldprocustomer.model.city_res.CityResponse;
import com.fieldprov2.fieldprocustomer.model.country_response.CountryResponse;
import com.fieldprov2.fieldprocustomer.model.customer_product.Example;
import com.fieldprov2.fieldprocustomer.model.customer_sub_product.CustomerSubProductResponse;
import com.fieldprov2.fieldprocustomer.model.get_details.GetDetailsResponse;
import com.fieldprov2.fieldprocustomer.model.location_res.LocationResponse;
import com.fieldprov2.fieldprocustomer.model.login_response.LoginResponse;
import com.fieldprov2.fieldprocustomer.model.logout.LogoutResponse;
import com.fieldprov2.fieldprocustomer.model.product_list.ProductListResponse;
import com.fieldprov2.fieldprocustomer.model.product_root.ProductResponse;
import com.fieldprov2.fieldprocustomer.model.profile_activity.ProfileActivityResponse;
import com.fieldprov2.fieldprocustomer.model.raiseticket_res.RaiseTicketResponse;
import com.fieldprov2.fieldprocustomer.model.registration_res.RegistrationResponse;
import com.fieldprov2.fieldprocustomer.model.serialNumberResponse.SerialNumberResponse;
import com.fieldprov2.fieldprocustomer.model.state_res.StateResponse;
import com.fieldprov2.fieldprocustomer.model.sub_product_root.SubProductResponse;
import com.fieldprov2.fieldprocustomer.model.ticket_list.TicketListResponse;
import com.fieldprov2.fieldprocustomer.model.work_type_id.WorkTypeResponse;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {


    @POST("customer_login/")
    Call<LoginResponse> login(@Body JsonObject jsonObject);

    @GET("get_product_details/")
    Call<ProductResponse> getProduct();

    @GET("get_subproduct_details/")
    Call<SubProductResponse> getSubProduct(@Query("product_id") int product_id);

    @GET("get_amc_details/")
    Call<AMCResponse> getAMC();

    @GET("get_country/")
    Call<CountryResponse> getCountry();

    @GET("get_state/")
    Call<StateResponse> getState(@Query("country_id") int country_id);

    @GET("get_city/")
    Call<CityResponse> getCity(@Query("state_id") int state_id);

    @GET("get_location_details/")
    Call<LocationResponse> getLocation(@Query("city_id") int city_id);

    @POST("customer_registration/")
    Call<RegistrationResponse> postCustomerRegistration(@Body JsonObject jsonObject);

    @GET("load_customer_product/")
    Call<Example> getUProduct(@Header ("Token") String login_token , @Query("customer_code") String customer_code_id);

    @GET("load_customer_subproduct/")
    Call<CustomerSubProductResponse> getUSubProduct(@Header("Token") String login_token, @Query("customer_code") String customer_code_id, @Query("product_id") int product_id );

    @GET("get_call_category/")
    Call<CallCategoryResponse> getCallCategory();

    @GET("get_servicegroup/")
    Call<WorkTypeResponse> getWorkType();

    @GET("customer_details/")
    Call<GetDetailsResponse> getWorkType(@Header("Token") String login_token, @Query("customer_code") String customer_code_id, @Query("product_id") String product_id, @Query("product_sub_id") String sub_product_id);

    @POST("cus_raise_ticket/")
    Call<RaiseTicketResponse> postCustomerRaiseTicket(@Header("Token") String details_token, @Body JsonObject jsonObject);

    @POST("change_password/")
    Call<ChangePasswordResponse> postChangePassword(@Body JsonObject jsonObject);

    @POST("customer_forget_password/")
    Call<ChangePasswordResponse> postForgotPassword(@Body JsonObject jsonObject);

    @GET("customer_profile/")
    Call<ProfileActivityResponse> getProfileActivity(@Header("Token") String details_token, @Query("customer_code") String customer_code_id);

    @GET("load_tickets/")
    Call<TicketListResponse> getTicketList(@Header("Token") String profileactivity_token, @Query("customer_code") String customer_code_id);

    @GET("customer_logout/")
    Call<LogoutResponse> getLogout(@Query("email") String email_id);

    @POST("add_customer_product/")
    Call<AddProductResponse> postAddProduct(@Header("Token") String ticketlist_token, @Body JsonObject jsonObject);

    @GET("my_products/")
    Call<ProductListResponse> getProductList(@Header("Token") String token , @Query("email") String email);

    @POST("load_contract_details/")
    Call<AMCListResponse> getAMCList(@Header("Token") String token , @Body JsonObject jsonObject);

    @POST("customer_amc/")
    Call<AddAMCResponse> postAddAMC(@Header("Token") String ticketlist_token, @Body AddProductPojo addProductPojo);

    @POST("validate_serial_no/")
    Call<SerialNumberResponse> postSerialnumberResponse(@Body JsonObject jsonObject);

    @POST("edit_customer_details/")
    Call<ProfileDetailsResponse> setProfileDetails(@Header("Token") String token, @Body JsonObject jsonObject);

}
