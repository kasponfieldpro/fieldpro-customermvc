package com.fieldprov2.fieldprocustomer.controller;

import android.util.Log;

import com.fieldprov2.fieldprocustomer.model.change_password.ChangePasswordResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordController
{
    public void postForgotPassword(ApiInterface api, JsonObject jsonObject, final ForgotPasswordController.ForgotPasswordResCallbacks forgotpasswordResCallbacks )
    {
        api.postForgotPassword(jsonObject ).enqueue(new Callback<ChangePasswordResponse>() {
            @Override
            public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response)
            {
                Log.e("hell","_______"+response.code());
                if (response.body() != null && response.code() == 200)
                {
                    forgotpasswordResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {

            }
        });
    }


    public interface ForgotPasswordResCallbacks {
        void onSuccess(ChangePasswordResponse value);

        void onError(Throwable throwable);
    }
}
