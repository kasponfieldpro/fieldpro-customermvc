package com.fieldprov2.fieldprocustomer.controller;

import android.util.Log;

import com.fieldprov2.fieldprocustomer.model.amc_list.AMCListResponse;
import com.fieldprov2.fieldprocustomer.model.product_list.ProductListResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AMCListController
{
    public void getAMCList(ApiInterface api, JsonObject jsonObject, final AMCListController.AMCListResCallbacks amcListResCallbacks, String token ) {

        api.getAMCList(token,jsonObject).enqueue(new Callback<AMCListResponse>() {
            @Override
            public void onResponse(Call<AMCListResponse> call, Response<AMCListResponse> response) {
                Log.d("TAG", response.code() + "");
                if (response.body() != null) {
                    amcListResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<AMCListResponse> call, Throwable t) {

            }
        });

    }

    public interface AMCListResCallbacks
    {
        void onSuccess(AMCListResponse value);

        void onError(Throwable throwable);
    }
}
