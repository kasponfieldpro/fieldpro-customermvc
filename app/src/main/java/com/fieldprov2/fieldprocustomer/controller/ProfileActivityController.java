package com.fieldprov2.fieldprocustomer.controller;

import android.util.Log;

import com.fieldprov2.fieldprocustomer.model.EditCustomer.ProfileDetailsResponse;
import com.fieldprov2.fieldprocustomer.model.change_password.ChangePasswordResponse;
import com.fieldprov2.fieldprocustomer.model.profile_activity.ProfileActivityResponse;
import com.fieldprov2.fieldprocustomer.model.sub_product_root.SubProductResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivityController
{
    public void getProfileActivity(ApiInterface api, final ProfileActivityController.ProfileActivityResCallbacks profileActivityResCallbacks,String details_token, String customer_code) {

        api.getProfileActivity(details_token,customer_code).enqueue(new Callback<ProfileActivityResponse>() {
            @Override
            public void onResponse(Call<ProfileActivityResponse> call, Response<ProfileActivityResponse> response) {
                Log.d("TAG", response.code() + "");
                if (response.body() != null) {
                    profileActivityResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<ProfileActivityResponse> call, Throwable t) {

            }
        });

    }


    public void setProfileDetails(ApiInterface api, final ProfileActivityController.ProfileDetailsResCallbacks profileDetailsResCallbacks, String details_token, JsonObject jsonObject) {

        api.setProfileDetails(details_token,jsonObject).enqueue(new Callback<ProfileDetailsResponse>() {
            @Override
            public void onResponse(Call<ProfileDetailsResponse> call, Response<ProfileDetailsResponse> response) {
                Log.d("TAG", response.code() + "");
                if (response.body() != null) {
                    profileDetailsResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<ProfileDetailsResponse> call, Throwable t) {

            }
        });

    }

    public interface ProfileActivityResCallbacks {
        void onSuccess(ProfileActivityResponse value);

        void onError(Throwable throwable);
    }

    public interface ProfileDetailsResCallbacks {
        void onSuccess(ProfileDetailsResponse value);

        void onError(Throwable throwable);
    }
}
