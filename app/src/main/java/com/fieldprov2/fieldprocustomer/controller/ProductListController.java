package com.fieldprov2.fieldprocustomer.controller;

import android.util.Log;

import com.fieldprov2.fieldprocustomer.model.product_list.ProductListResponse;
import com.fieldprov2.fieldprocustomer.model.ticket_list.TicketListResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductListController
{
    public void getProductList(ApiInterface api, final ProductListController.ProductListResCallbacks productListResCallbacks, String token, String email) {

        api.getProductList(token,email).enqueue(new Callback<ProductListResponse>() {
            @Override
            public void onResponse(Call<ProductListResponse> call, Response<ProductListResponse> response) {
                Log.d("TAG", response.code() + "");
                if (response.body() != null) {
                    productListResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<ProductListResponse> call, Throwable t) {

            }
        });

    }

    public interface ProductListResCallbacks {
        void onSuccess(ProductListResponse value);

        void onError(Throwable throwable);
    }

}
