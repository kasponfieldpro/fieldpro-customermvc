package com.fieldprov2.fieldprocustomer.controller;

import android.util.Log;

import com.fieldprov2.fieldprocustomer.model.logout.LogoutResponse;
import com.fieldprov2.fieldprocustomer.model.profile_activity.ProfileActivityResponse;
import com.fieldprov2.fieldprocustomer.model.ticket_list.TicketListResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogoutController
{
    public void getLogout(ApiInterface api, final LogoutController.LogoutResCallbacks logoutResCallbacks, String email_id) {

        api.getLogout(email_id).enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                Log.d("TAG", response.code() + "");
                if (response.body() != null) {
                    logoutResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<LogoutResponse> call, Throwable t) {

            }
        });

    }

    public interface LogoutResCallbacks {
        void onSuccess(LogoutResponse value);

        void onError(Throwable throwable);
    }
}
