package com.fieldprov2.fieldprocustomer.controller;

import android.util.Log;

import com.fieldprov2.fieldprocustomer.model.AddProductPojo;
import com.fieldprov2.fieldprocustomer.model.add_amc_res.AddAMCResponse;
import com.fieldprov2.fieldprocustomer.model.add_product_res.AddProductResponse;
import com.fieldprov2.fieldprocustomer.model.amc_response.AMCResponse;
import com.fieldprov2.fieldprocustomer.model.city_res.CityResponse;
import com.fieldprov2.fieldprocustomer.model.country_response.CountryResponse;
import com.fieldprov2.fieldprocustomer.model.location_res.LocationResponse;
import com.fieldprov2.fieldprocustomer.model.product_root.ProductResponse;
import com.fieldprov2.fieldprocustomer.model.registration_res.RegistrationResponse;
import com.fieldprov2.fieldprocustomer.model.serialNumberResponse.SerialNumberResponse;
import com.fieldprov2.fieldprocustomer.model.state_res.StateResponse;
import com.fieldprov2.fieldprocustomer.model.sub_product_root.SubProductResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;
import com.fieldprov2.fieldprocustomer.view.AddProductActivity;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegistrationController {


    public void getProducts(ApiInterface api, final ProductResCallbacks productResCallbacks) {

        api.getProduct().enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                Log.d("TAG", response.code() + "");
                if (response.body() != null) {
                    productResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {

            }
        });

    }

    public void getSubProducts(ApiInterface api, final SubProductResCallbacks subProductResCallbacks, int productCode) {

        api.getSubProduct(productCode).enqueue(new Callback<SubProductResponse>() {
            @Override
            public void onResponse(Call<SubProductResponse> call, Response<SubProductResponse> response) {
                Log.d("TAG", response.code() + "");
                if (response.body() != null) {
                    if(response.body().getResponse().getResponseCode().equals("200"))
                    {
                        subProductResCallbacks.onSuccess(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<SubProductResponse> call, Throwable t) {

            }
        });

    }

    public void getAMC(ApiInterface api, final AMCResCallbacks amcResCallbacks) {

        api.getAMC().enqueue(new Callback<AMCResponse>() {
            @Override
            public void onResponse(Call<AMCResponse> call, Response<AMCResponse> response) {
                if (response.body() != null) {
                    amcResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<AMCResponse> call, Throwable t) {

            }
        });

    }

    public void getCountry(ApiInterface api, final CountryResCallbacks countryResCallbacks) {

        api.getCountry().enqueue(new Callback<CountryResponse>() {
            @Override
            public void onResponse(Call<CountryResponse> call, Response<CountryResponse> response) {
                if (response.body() != null) {
                    countryResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<CountryResponse> call, Throwable t) {

            }
        });

    }

    public void getState(ApiInterface api, final StateResCallbacks stateResCallbacks, int country_id) {

        api.getState(country_id).enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                if (response.body() != null) {
                    stateResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {

            }
        });

    }

    public void getCity(ApiInterface api, final CityResCallbacks cityResCallbacks, int state_id) {

        api.getCity(state_id).enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                Log.d("TAG", response.code() + "");
                if (response.body() != null) {
                    cityResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {

            }
        });

    }

    public void getLocation(ApiInterface api, final LocationResCallbacks locationResCallbacks, int city_id) {

        api.getLocation(city_id).enqueue(new Callback<LocationResponse>() {
            @Override
            public void onResponse(Call<LocationResponse> call, Response<LocationResponse> response) {
                Log.d("TAG", response.code() + "");
                if (response.body() != null)
                {
                    if(response.body().getResponse().getResponseCode().equals("200")) {
                        locationResCallbacks.onSuccess(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<LocationResponse> call, Throwable t) {

            }
        });

    }

    public void postCustomerRegistration(ApiInterface api, JsonObject jsonObject, final RegistrationResCallbacks registrationResCallbacks)
    {
        api.postCustomerRegistration(jsonObject).enqueue(new Callback<RegistrationResponse>() {
            @Override
            public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
                Log.e("hell","_______"+response.code());
                if (response.body() != null && response.code() == 200) {
                    registrationResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<RegistrationResponse> call, Throwable t) {

            }
        });
    }

    public void postAddProduct(ApiInterface api, JsonObject jsonObject, final AddProductResCallbacks addproductResCallbacks,String ticketlist_token)
    {
        api.postAddProduct(ticketlist_token,jsonObject).enqueue(new Callback<AddProductResponse>() {
            @Override
            public void onResponse(Call<AddProductResponse> call, Response<AddProductResponse> response) {
                Log.e("hell","_______"+response.code());
                if (response.body() != null && response.code() == 200) {
                    addproductResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<AddProductResponse> call, Throwable t) {

            }
        });
    }

    public void postAddAMC(ApiInterface api, AddProductPojo addProductPojo, final AddAMCResCallbacks addAMCResCallbacks, String ticketlist_token)
    {
        api.postAddAMC(ticketlist_token,addProductPojo).enqueue(new Callback<AddAMCResponse>() {
            @Override
            public void onResponse(Call<AddAMCResponse> call, Response<AddAMCResponse> response) {
                Log.e("hell","_______"+response.code());
                if (response.body() != null && response.code() == 200) {
                    addAMCResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<AddAMCResponse> call, Throwable t) {

            }
        });
    }

    public void postserialNumberResponse(ApiInterface api,JsonObject jsonObject, final SerialNumberResponseCallbacks serialnumberresponseCallbacks)
    {
        api.postSerialnumberResponse(jsonObject).enqueue(new Callback<SerialNumberResponse>()
        {
            @Override
            public void onResponse(Call<SerialNumberResponse> call, Response<SerialNumberResponse> response)
            {
                Log.e("hell","_______"+response.code());
                if (response.body() != null && response.code() == 200)
                {
                    serialnumberresponseCallbacks.onSuccess(response.body());
                }
            }
            @Override
            public void onFailure(Call<SerialNumberResponse> call, Throwable t)
            {

            }
        });
    }



    public interface ProductResCallbacks {
        void onSuccess(ProductResponse value);

        void onError(Throwable throwable);
    }

    public interface SubProductResCallbacks {
        void onSuccess(SubProductResponse value);

        void onError(Throwable throwable);
    }

    public interface AMCResCallbacks {
        void onSuccess(AMCResponse value);

        void onError(Throwable throwable);
    }

    public interface CountryResCallbacks {
        void onSuccess(CountryResponse value);

        void onError(Throwable throwable);
    }


    public interface StateResCallbacks {
        void onSuccess(StateResponse value);

        void onError(Throwable throwable);
    }

    public interface CityResCallbacks {
        void onSuccess(CityResponse value);

        void onError(Throwable throwable);
    }


    public interface LocationResCallbacks {
        void onSuccess(LocationResponse value);

        void onError(Throwable throwable);
    }

    public interface RegistrationResCallbacks {
        void onSuccess(RegistrationResponse value);

        void onError(Throwable throwable);
    }

    public interface AddProductResCallbacks {
        void onSuccess(AddProductResponse value);

        void onError(Throwable throwable);
    }

    public interface AddAMCResCallbacks
    {
        void onSuccess(AddAMCResponse value);

        void onError(Throwable throwable);
    }

    public interface SerialNumberResponseCallbacks
    {
        void onSuccess(SerialNumberResponse value);

        void onError(Throwable throwable);
    }

}
