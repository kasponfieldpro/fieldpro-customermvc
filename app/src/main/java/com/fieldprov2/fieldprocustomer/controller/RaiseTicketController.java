package com.fieldprov2.fieldprocustomer.controller;

import android.util.Log;

import com.fieldprov2.fieldprocustomer.model.amc_response.AMCResponse;
import com.fieldprov2.fieldprocustomer.model.call_category.CallCategoryResponse;
import com.fieldprov2.fieldprocustomer.model.city_res.CityResponse;
import com.fieldprov2.fieldprocustomer.model.country_response.CountryResponse;
import com.fieldprov2.fieldprocustomer.model.customer_product.Example;
import com.fieldprov2.fieldprocustomer.model.customer_sub_product.CustomerSubProductResponse;
import com.fieldprov2.fieldprocustomer.model.get_details.GetDetailsResponse;
import com.fieldprov2.fieldprocustomer.model.location_res.LocationResponse;
import com.fieldprov2.fieldprocustomer.model.product_root.ProductResponse;
import com.fieldprov2.fieldprocustomer.model.raiseticket_res.RaiseTicketResponse;
import com.fieldprov2.fieldprocustomer.model.registration_res.RegistrationResponse;
import com.fieldprov2.fieldprocustomer.model.state_res.StateResponse;
import com.fieldprov2.fieldprocustomer.model.sub_product_root.SubProductResponse;
import com.fieldprov2.fieldprocustomer.model.work_type_id.WorkTypeResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;
import com.fieldprov2.fieldprocustomer.view.RaiseTicketFragment;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RaiseTicketController
{
    public void getUProducts(ApiInterface api, final UProductResCallbacks uproductResCallbacks,String login_token ,String customer_code) {

        api.getUProduct(login_token,customer_code).enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                Log.d("TAG", response.code() + "");
                if (response.body() != null) {
                    uproductResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {

            }
        });

    }

    public void getUSubProducts(ApiInterface api, final USubProductResCallbacks usubProductResCallbacks,String login_token , String customer_code , int productCode)
    {

        api.getUSubProduct(login_token,customer_code,productCode).enqueue(new Callback<CustomerSubProductResponse>() {
            @Override
            public void onResponse(Call<CustomerSubProductResponse> call, Response<CustomerSubProductResponse> response) {
                Log.d("TAG", response.code() + "");
                if (response.body() != null) {
                    usubProductResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<CustomerSubProductResponse> call, Throwable t) {

            }
        });

    }

   public void getCallCategory(ApiInterface api, final CallCategoryResCallbacks callCategoryResCallbacks) {

        api.getCallCategory().enqueue(new Callback<CallCategoryResponse>() {
            @Override
            public void onResponse(Call<CallCategoryResponse> call, Response<CallCategoryResponse> response) {
                if (response.body() != null) {
                    callCategoryResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<CallCategoryResponse> call, Throwable t) {

            }
        });

    }

     public void getWorkType(ApiInterface api, final WorkTypeResCallbacks workTypeResCallbacks) {

        api.getWorkType().enqueue(new Callback<WorkTypeResponse>() {
            @Override
            public void onResponse(Call<WorkTypeResponse> call, Response<WorkTypeResponse> response) {
                if (response.body() != null) {
                    workTypeResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<WorkTypeResponse> call, Throwable t) {

            }
        });

    }

    public void getDetails(ApiInterface api, final DetailsResCallbacks detailsResCallbacks ,String login_token , String customer_code , String product_id, String sub_product_id) {

        api.getWorkType(login_token,customer_code,product_id,sub_product_id).enqueue(new Callback<GetDetailsResponse>() {
            @Override
            public void onResponse(Call<GetDetailsResponse> call, Response<GetDetailsResponse> response) {
                if (response.body() != null) {
                    detailsResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<GetDetailsResponse> call, Throwable t) {

            }
        });

    }


  public void postCustomerRaiseTicket(ApiInterface api, JsonObject jsonObject, final RaiseTicketResCallbacks raiseTicketResCallbacks,String details_token)
  {
      api.postCustomerRaiseTicket(details_token , jsonObject).enqueue(new Callback<RaiseTicketResponse>() {
          @Override
          public void onResponse(Call<RaiseTicketResponse> call, Response<RaiseTicketResponse> response) {
              Log.e("hell","_______"+response.code());
              if (response.body() != null && response.code() == 200) {
                  raiseTicketResCallbacks.onSuccess(response.body());
              }
          }

          @Override
          public void onFailure(Call<RaiseTicketResponse> call, Throwable t) {

          }
      });
  }


    public interface UProductResCallbacks {
        void onSuccess(Example value);

        void onError(Throwable throwable);
    }

    public interface USubProductResCallbacks {
        void onSuccess(CustomerSubProductResponse value);

        void onError(Throwable throwable);
    }

    public interface CallCategoryResCallbacks {
        void onSuccess(CallCategoryResponse value);

        void onError(Throwable throwable);
    }

    public interface WorkTypeResCallbacks {
        void onSuccess(WorkTypeResponse value);

        void onError(Throwable throwable);
    }


    public interface DetailsResCallbacks {
        void onSuccess(GetDetailsResponse value);

        void onError(Throwable throwable);
    }

    public interface RaiseTicketResCallbacks {
        void onSuccess(RaiseTicketResponse value);

        void onError(Throwable throwable);
    }




}
