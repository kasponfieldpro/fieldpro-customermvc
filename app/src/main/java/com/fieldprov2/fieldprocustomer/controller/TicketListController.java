package com.fieldprov2.fieldprocustomer.controller;

import android.util.Log;

import com.fieldprov2.fieldprocustomer.model.change_password.ChangePasswordResponse;
import com.fieldprov2.fieldprocustomer.model.ticket_list.TicketListResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TicketListController
{
        public void getTicketList(ApiInterface api, final TicketListController.TicketListResCallbacks ticketListResCallbacks,String profileactivity_token, String customer_code) {

        api.getTicketList(profileactivity_token,customer_code).enqueue(new Callback<TicketListResponse>() {
            @Override
            public void onResponse(Call<TicketListResponse> call, Response<TicketListResponse> response) {
                Log.d("TAG", response.code() + "");
                if (response.body() != null) {
                               ticketListResCallbacks.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<TicketListResponse> call, Throwable t) {

            }
        });

    }

    public interface TicketListResCallbacks {
        void onSuccess(TicketListResponse value);

        void onError(Throwable throwable);
    }


}
