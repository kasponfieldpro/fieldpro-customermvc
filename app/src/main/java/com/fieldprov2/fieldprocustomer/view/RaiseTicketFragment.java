package com.fieldprov2.fieldprocustomer.view;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.fragment.app.Fragment;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.fieldprov2.fieldprocustomer.R;
import com.fieldprov2.fieldprocustomer.controller.RaiseTicketController;
import com.fieldprov2.fieldprocustomer.model.call_category.CallCategoryResponse;
import com.fieldprov2.fieldprocustomer.model.customer_product.Datum;
import com.fieldprov2.fieldprocustomer.model.customer_product.Example;
import com.fieldprov2.fieldprocustomer.model.customer_sub_product.CustomerSubProductResponse;
import com.fieldprov2.fieldprocustomer.model.get_details.GetDetailsResponse;
import com.fieldprov2.fieldprocustomer.model.raiseticket_res.RaiseTicketResponse;
import com.fieldprov2.fieldprocustomer.model.registration_res.RegistrationResponse;
import com.fieldprov2.fieldprocustomer.model.work_type_id.WorkTypeResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;
import com.fieldprov2.fieldprocustomer.network.RetrofitClient;
import com.fieldprov2.fieldprocustomer.view.login.ForgotPasswordActivity;
import com.fieldprov2.fieldprocustomer.view.login.LoginActivity;
import com.fieldprov2.fieldprocustomer.view.profile.ProfileActivity;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;


public class RaiseTicketFragment extends Fragment implements  AdapterView.OnItemSelectedListener, RaiseTicketController.UProductResCallbacks , RaiseTicketController.USubProductResCallbacks,
        RaiseTicketController.CallCategoryResCallbacks,RaiseTicketController.WorkTypeResCallbacks ,RaiseTicketController.DetailsResCallbacks,RaiseTicketController.RaiseTicketResCallbacks  {


    //initializing the values
    private int PermissionCode = 1;
    private int GALLERYCODE = 100;
    private int CAMERA_CODE = 101;
    private String cus_code;
    private String login_token;
    private String product_token;
    private String pro_token;
    private String subpro_token;
    private String sub_product_token;
    private String contract_type;
    private String model_number;
    private String serial_number;
    private String con_number;
    private String mod_number;
    private String ser_number;
    private String details_tok;
    private String details_token;
    private String base64;
    private String date,token,priority_text;
    List<String> list = new ArrayList<String>();

    ApiInterface apiInterface;
    RaiseTicketController raiseTicketController;
    private String base64Format = "data:image/png;base64,";
    private String encImageBase64;

    Spinner spnProductId, spnSubProductId, spnCallCategoryId, spnWorkTypeId,spnSerialNumberId, spnPriorityId;
    EditText  exrProblemDesc;
    TextView txtDatePicker, txtTimePicker,extContractName, extModelNo,description_length;
    Button btnAttachImage, btnRaiseTicket;
    ImageView imageView,cancel_image;
    ProgressDialog pDialog ;
    private LinearLayout productspinnerlay, subcategoryspinnerlay, callcategorylay, datelayout, timelayout,worktypelay,serialnumberlay;


    private int spnProductPosition = 0, spnSubProductPosition = 0, spnCallCategoryPosition = 0,
            spnWorkTypePosition = 0, spnSerialNumberPosition = 0;

    //creating arraylist for spinners
    ArrayList<String> uproductItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.customer_product.Datum> uproductObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.customer_product.Datum>();

    ArrayList<String> usubproductItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.customer_sub_product.Datum> usubproductObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.customer_sub_product.Datum>();

    ArrayList<String> callCategoryItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.call_category.Datum> callCategoryObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.call_category.Datum>();

    ArrayList<String> workTypeItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.work_type_id.Datum> workTypeObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.work_type_id.Datum>();


    ArrayList<String> serialnumberItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.get_details.Datum> serialnumberObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.get_details.Datum>();

    private Context context;

    public RaiseTicketFragment() {
        // Required empty public constructor
    }

    public static RaiseTicketFragment newInstance() {
        return new RaiseTicketFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Objects.requireNonNull(getActivity()).setTitle("Raise Ticket");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        new ProgressDialog(getActivity());
        // Inflate the layout for this fragment
        //getting customer code and login token from shared preference to call the api
        cus_code = Pref_storage.getDetail(getActivity(), "customer_code");
        login_token = Pref_storage.getDetail(getActivity(), "Token");

        View v = inflater.inflate(R.layout.fragment_raise_ticket, container, false);
        context = v.getContext();

        spnProductId = v.findViewById(R.id.spnProduct);
        spnSubProductId = v.findViewById(R.id.spnSubProduct);
        spnCallCategoryId = v.findViewById(R.id.spnCallCategory);
        spnWorkTypeId = v.findViewById(R.id.spnWorkType);
        spnSerialNumberId = v.findViewById(R.id.spnSerialNumber);

        productspinnerlay = v.findViewById(R.id.productspinner_lay);
        subcategoryspinnerlay = v.findViewById(R.id.subcategoryspinner_lay);
        callcategorylay = v.findViewById(R.id.callcategory_lay);
        datelayout = v.findViewById(R.id.date_layout);
        timelayout = v.findViewById(R.id.time_layout);
        worktypelay= v.findViewById(R.id.worktype1);
        serialnumberlay = v.findViewById(R.id.serialtype1);

        extContractName = v.findViewById(R.id.extContractName);
        extModelNo = v.findViewById(R.id.extModelNo);
        exrProblemDesc = v.findViewById(R.id.exrProblemDesc);
        description_length = v.findViewById(R.id.description_length);

        txtDatePicker = v.findViewById(R.id.txtDatePicker);
        txtTimePicker = v.findViewById(R.id.txtTimePicker);

        btnAttachImage = v.findViewById(R.id.btnAttachImage);
        btnRaiseTicket = v.findViewById(R.id.btnRaiseTicket);

        imageView = v.findViewById(R.id.imageView);
        cancel_image = v.findViewById(R.id.cancel_image);

        spnProductId.setOnItemSelectedListener(this);
        spnSubProductId.setOnItemSelectedListener(this);
        spnCallCategoryId.setOnItemSelectedListener(this);
        spnWorkTypeId.setOnItemSelectedListener(this);
        spnSerialNumberId.setOnItemSelectedListener(this);

        apiInterface = RetrofitClient.getClient().create(ApiInterface.class);
        raiseTicketController = new RaiseTicketController();

        //get productList
       //calling the product ,call category, work type api's
        getUProductitem();
        getCallCategoryItems();
        getWorkTypeItem();

        //for counting the number of characters in description
        exrProblemDesc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                exrProblemDesc.setError(null);
                int length = exrProblemDesc.length();
                String convert = String.valueOf(length);
                description_length.setText(convert+"/250");
                //hdgshuchusdvsdhjvhzcvsghvSDGHcvSDHcvh
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //creating functions to close the keypad when the spinner is clicked
        spnProductId.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm=(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnProductId.getWindowToken(), 0);
                return false;
            }
        }) ;
        spnSubProductId.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm=(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnSubProductId.getWindowToken(), 0);
                return false;
            }
        }) ;
        spnCallCategoryId.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm=(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnCallCategoryId.getWindowToken(), 0);
                return false;
            }
        }) ;
        spnWorkTypeId.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm=(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnWorkTypeId.getWindowToken(), 0);
                return false;
            }
        }) ;
        spnSerialNumberId.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm=(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnSerialNumberId.getWindowToken(), 0);
                return false;
            }
        }) ;

        //creating button click functions for raise ticket
        btnRaiseTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validate())
                {

                    //loading graphics
                    pDialog = ProgressDialog.show(getActivity(), "FieldPro","Loading...", true);
                    pDialog.setCancelable(true);

                    //String base64 = encodeImage(imageView.getDrawingCache());
                   //sending values through json object
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("customer_code",cus_code);
                    jsonObject.addProperty("customer_contract_id",serialnumberObject.get(spnSerialNumberPosition).getCustomerContractId());
                    jsonObject.addProperty("product_id",uproductObject.get(spnProductPosition).getProductId());
                    jsonObject.addProperty("product_sub_id",usubproductObject.get(spnSubProductPosition).getProductSubId());
                    jsonObject.addProperty("model_no",serialnumberObject.get(spnSerialNumberPosition).getModelNo());
                    jsonObject.addProperty("serial_no",serialnumberObject.get(spnSerialNumberPosition).getSerialNo() );
                    jsonObject.addProperty("call_category_id",callCategoryObject.get(spnCallCategoryPosition).getCallCategoryId());
                    jsonObject.addProperty("cust_preference_date",date);
                    jsonObject.addProperty("contract_type_id",serialnumberObject.get(spnSerialNumberPosition).getContractTypeId());
                    jsonObject.addProperty("work_type_id",workTypeObject.get(spnWorkTypePosition).getWorkTypeId());
                    jsonObject.addProperty("problem_desc",exrProblemDesc.getEditableText().toString());
                    jsonObject.addProperty("image",base64Format+encImageBase64);
                    jsonObject.addProperty("priority","P1");

                    //calling the raise ticket api
                    callRaiseTicketCall(jsonObject);
                }
            }
        });

  //      raiseTicketFragmentController = new RaiseTicketFragment();

        return v;
    }

    //calling the raise ticket api
    private void callRaiseTicketCall(JsonObject jsonObject)
    {
        token = Pref_storage.getDetail(getActivity(),"Token");
        Log.e("hello","------------"+jsonObject);
        raiseTicketController.postCustomerRaiseTicket(apiInterface, jsonObject, this,token);
    }

   /* public String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encImage;
    }*/



   //calling the product api
   private void getUProductitem()
    {
      raiseTicketController.getUProducts(apiInterface,this,login_token,(cus_code));
    }

    //creating the product adapter for spinner
    private void setProductAdapter() {
        ArrayAdapter<String> ad = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, uproductItem);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnProductId.setAdapter(ad);
    }

    private void getSubProductItems(int product_id)
    {
        product_token = Pref_storage.getDetail(getActivity(), "Token");
        Log.e("producttok","------"+product_token);
        raiseTicketController.getUSubProducts(apiInterface,this,product_token,cus_code,product_id);
    }

    private void setSubProductAdapter() {
        ArrayAdapter<String> ad = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, usubproductItem);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSubProductId.setAdapter(ad);
    }

    private void getCallCategoryItems() {
        raiseTicketController.getCallCategory(apiInterface,  this);
    }

    private void setCallCategoryAdapter() {
        ArrayAdapter<String> ad = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, callCategoryItem);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnCallCategoryId.setAdapter(ad);
    }

    private void getWorkTypeItem()
    {
        raiseTicketController.getWorkType(apiInterface,this);
    }

    private void setWorkTypeAdapter() {
        ArrayAdapter<String> ad = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, workTypeItem);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnWorkTypeId.setAdapter(ad);
    }

    private void getDetailsItems(int sub_product_id,int product_id1)
    {
        sub_product_token = Pref_storage.getDetail(context, "Token");
        Log.e("producttok","------"+product_token);
        raiseTicketController.getDetails(apiInterface,this,sub_product_token,cus_code,String.valueOf(product_id1),String.valueOf(sub_product_id));
    }

    private void setSerialNumberAdapter() {
        ArrayAdapter<String> ad = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, serialnumberItem);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSerialNumberId.setAdapter(ad);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        //date picker for selecting the date
        txtDatePicker.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                datelayout.setBackgroundResource(R.drawable.bg_box_grey);
                datelayout.setPadding(23, 0, 23, 0);
                final Calendar cldr = Calendar.getInstance();

                int day = cldr.get(Calendar.DAY_OF_MONTH);
                final int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);

                Calendar currentDate = Calendar.getInstance();

                    // date picker dialog
                    DatePickerDialog picker = new DatePickerDialog(context,
                            new DatePickerDialog.OnDateSetListener()
                            {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                                {

                                    txtDatePicker.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                    date = String.valueOf(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                                }
                            }, year, month, day);
                picker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                Calendar maxDate = Calendar.getInstance();
               //for restricting to select the date for two weeks
                maxDate.set(Calendar.DAY_OF_MONTH, day + 14);
                maxDate.set(Calendar.MONTH, month);
                maxDate.set(Calendar.YEAR, year);

                picker.getDatePicker().setMaxDate(maxDate.getTimeInMillis());
                picker.show();
                }

        });

        //for selecting time
        txtTimePicker.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                timelayout.setBackgroundResource(R.drawable.bg_box_grey);
                timelayout.setPadding(23, 0, 23, 0);
                final Calendar cldr = Calendar.getInstance();
                int hour = cldr.get(Calendar.HOUR_OF_DAY);
                int minutes = cldr.get(Calendar.MINUTE);
                // time picker dialog
                TimePickerDialog picker = new TimePickerDialog(getContext(),
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker tp, int sHour, int sMinute) {
                                txtTimePicker.setText(sHour + ":" + sMinute);
                            }
                        }, hour, minutes, true);
                picker.show();
            }
        });

        //for attaching the image
        btnAttachImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] permissions = {
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA,
                };

                if (!hasPermissions(getActivity(), permissions)) {
                    ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()), permissions, PermissionCode);
                } else {
                    selectImage(getActivity());


//                    Intent intent = new Intent(Intent.ACTION_PICK);
//                    intent.setType("image/*");
//                    startActivityForResult(intent, GALLERYCODE);
                }

            }
        });

    }

    //code for photo selection from gallery or camera
    private void selectImage(Context context) {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, CAMERA_CODE);

                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    startActivityForResult(intent, GALLERYCODE);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private Boolean hasPermissions(Context context, String[] permissions) {
        if (context != null) {

            for (String permi : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permi) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    //code to validate the entries before raising the ticket
    private boolean validate() {
        boolean valid = true;

         if (exrProblemDesc.getEditableText().toString().isEmpty())
         {
            exrProblemDesc.setError("Please enter Problem Desc");
            exrProblemDesc.requestFocus();
            valid = false;
         }

  //       if (imageView.getVisibility() == View.GONE)
 //        {
  //          Toast.makeText(getActivity(), "Attach Image", Toast.LENGTH_SHORT).show();
 //           valid = false;
 //        }

         if (txtDatePicker.getText().toString().equals(getString(R.string.select_date)))
         {
             datelayout.setBackgroundResource(R.drawable.boarder);
             datelayout.setPadding(23, 0, 23, 0);
             valid = false;
        }


          if (txtTimePicker.getText().toString().equals(getString(R.string.select_time)))
          {
              timelayout.setBackgroundResource(R.drawable.boarder);
              timelayout.setPadding(23, 0, 23, 0);
              valid = false;
        }


          //code for displaying the spinner in red if it is not selected when raising the ticket
          if (spnProductPosition == 0  )
          {
            Toast.makeText(getActivity(), "Please Select All Fields", Toast.LENGTH_SHORT).show();
              productspinnerlay.setBackgroundResource(R.drawable.boarder);
              productspinnerlay.setPadding(23, 0, 23, 0);
              valid = false;
        }


          if (spnSubProductPosition == 0 )
        {
            subcategoryspinnerlay.setBackgroundResource(R.drawable.boarder);
            subcategoryspinnerlay.setPadding(23, 0, 23, 0);
            valid = false;
        }

          if (spnCallCategoryPosition == 0  )
        {
            callcategorylay.setBackgroundResource(R.drawable.boarder);
            callcategorylay.setPadding(23, 0, 23, 0);
            valid = false;
        }

        if (spnWorkTypePosition == 0 )
        {
            worktypelay.setBackgroundResource(R.drawable.boarder);
            worktypelay.setPadding(23, 0, 23, 0);
            valid = false;
        }

        if(spnSerialNumberPosition == 0)
        {
            serialnumberlay.setBackgroundResource(R.drawable.boarder);
            serialnumberlay.setPadding(23, 0, 23, 0);
            valid = false;        }

        return valid;
    }

    //code for accessing the gallery and camera and converting it into base 64 and sending it to api
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == CAMERA_CODE) {

                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
              /*  ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                File file = new File(Environment.getExternalStorageDirectory() + "/Field-Pro/images/" + System.currentTimeMillis() + ".jpg");
                try {
                    file.createNewFile();
                    FileOutputStream fo = new FileOutputStream(file);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                // Toast.makeText(this, "" + file, Toast.LENGTH_SHORT).show();
                camerapath = String.valueOf(file);*/

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Objects.requireNonNull(thumbnail).compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                encImageBase64 = Base64.encodeToString(b, Base64.DEFAULT);
                Log.e("status","Base64 image---> " + encImageBase64);

                imageView.setVisibility(View.VISIBLE);
                cancel_image.setVisibility(View.VISIBLE);
                cancel_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        imageView.setVisibility(View.INVISIBLE);
                        cancel_image.setVisibility(View.INVISIBLE);
                    }
                });
                imageView.setImageBitmap(thumbnail);

            } else if (requestCode == GALLERYCODE) {

                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getActivity().getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String camerapath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(camerapath));

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Objects.requireNonNull(thumbnail).compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                encImageBase64 = Base64.encodeToString(b, Base64.DEFAULT);

                Log.w("path of .", camerapath + "" + thumbnail);
                Log.w("path of .", camerapath + "" + encImageBase64);

                Log.e("status","Base64 image---> " + encImageBase64);
                imageView.setVisibility(View.VISIBLE);
                cancel_image.setVisibility(View.VISIBLE);
                cancel_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        imageView.setVisibility(View.INVISIBLE);
                        cancel_image.setVisibility(View.INVISIBLE);
                    }
                });
                imageView.setImageBitmap(thumbnail);
            }
        }
    /*
        if (resultCode == Activity.RESULT_OK && requestCode == GALLERYCODE) {
            imageView.setImageURI(data.getData());
            Bitmap bm = BitmapFactory.decodeFile(data.getDataString());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            if (bm != null) {
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); // bm is the bitmap object
                byte[] byteFormat = baos.toByteArray();
                String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
            }
            imageView.setVisibility(View.VISIBLE);
            //Log.e("status","Base ^4------> "+imgString);
        }

        if (resultCode == Activity.RESULT_OK && requestCode == CAMERA_CODE && data != null) {
            Bitmap thumbnail = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            if (selectedImage != null) {
                Cursor cursor = Objects.requireNonNull(getActivity()).getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    Log.e("status","Base ^4------> "+ picturePath);

                    try {
                        byte[] dataConvert = picturePath.getBytes("UTF-8");
                        base64 = Base64.encodeToString(dataConvert, Base64.DEFAULT);
                        Log.e("status","Base ^4------> "+base64);
                        imageView.setVisibility(View.VISIBLE);

                    } catch (UnsupportedEncodingException e) {
                        Log.e("status","Base ^4------> "+ e.getMessage());
                        e.printStackTrace();

                    }

                    imageView.setImageBitmap(thumbnail);
                    imageView.setVisibility(View.VISIBLE);

                    cursor.close();
                }
            }

        }*/
    }

    //code for camera functions
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PermissionCode) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, GALLERYCODE);
            }
        }
    }

 /*   @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.profile_menu, menu);

    }

    //code for opening the profile activity
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.
                    profileMenu: {
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                startActivity(intent);
            }
        }
        return super.onOptionsItemSelected(item);

    } */

   //code if the response from the api comes
    @Override
    public void onSuccess(Example value)
    {

        if (value.getResponse() != null)
        {

            pro_token = value.getResponse().getToken();
            Pref_storage.setDetail(getActivity(), "Token", pro_token);
            Log.e("test","--->"+new Gson().toJson(value.getResponse().getData()));
            uproductObject.add(value.getResponse().getData().get(0));
            uproductItem.add("Select Product");

            for ( Datum datum : value.getResponse().getData()) {
                uproductObject.add(datum);
                uproductItem.add(datum.getProductName());
                Log.e("test","--->"+new Gson().toJson(uproductItem));
            }
            setProductAdapter();

        }
    }

    @Override
    public void onSuccess(CustomerSubProductResponse value)
    {
        if (value.getResponse() != null)
        {
            usubproductObject.clear();
            usubproductItem.clear();


            subpro_token = value.getResponse().getToken();
            Pref_storage.setDetail(getActivity(),"Token",subpro_token);
            usubproductObject.add(value.getResponse().getData().get(0));
            usubproductItem.add("Select Sub Product");

            for ( com.fieldprov2.fieldprocustomer.model.customer_sub_product.Datum datum : value.getResponse().getData()) {
                usubproductObject.add(datum);
                usubproductItem.add(datum.getProductSubName());
            //    Log.e("test","--->"+new Gson().toJson(usubproductItem));
            }
            setSubProductAdapter();

        }
    }

    @Override
    public void onSuccess(CallCategoryResponse value)
    {
        if (value.getResponse() != null)
        {
            callCategoryObject.add(value.getResponse().getData().get(0));
            callCategoryItem.add("Select Call Category");

            for ( com.fieldprov2.fieldprocustomer.model.call_category.Datum datum : value.getResponse().getData()) {
                callCategoryObject.add(datum);
                callCategoryItem.add(datum.getCallCategory());
                //    Log.e("test","--->"+new Gson().toJson(usubproductItem));
            }
            setCallCategoryAdapter();

        }

    }

    @Override
    public void onSuccess(WorkTypeResponse value)
    {
        if (value.getResponse() != null)
        {
            workTypeObject.add(value.getResponse().getData().get(0));
            workTypeItem.add("Select Work Type");

            for ( com.fieldprov2.fieldprocustomer.model.work_type_id.Datum datum : value.getResponse().getData()) {
                workTypeObject.add(datum);
                workTypeItem.add(datum.getWorkType());
                //    Log.e("test","--->"+new Gson().toJson(usubproductItem));
            }
            setWorkTypeAdapter();

        }
    }

    @Override
    public void onSuccess(GetDetailsResponse value)
    {

        if (value.getResponse() != null) {

          serialnumberItem.clear();
          serialnumberObject.clear();

            token = value.getResponse().getToken();
            Pref_storage.setDetail(getActivity(),"Token",token);
            serialnumberObject.add(value.getResponse().getData().get(0));
            serialnumberItem.add("Select");

            for ( com.fieldprov2.fieldprocustomer.model.get_details.Datum datum : value.getResponse().getData()) {
                serialnumberObject.add(datum);
                serialnumberItem.add(datum.getSerialNo());
                //    Log.e("test","--->"+new Gson().toJson(usubproductItem));
            }

            setSerialNumberAdapter();
        }
    }

    @Override
    public void onSuccess(RaiseTicketResponse value)
    {
        if (value.getResponse() != null)
        {
            Log.e("hero","real"+value.getResponse());
            token = value.getResponse().getToken();
            Pref_storage.setDetail(getActivity(),"Token",token);

            //clearing all the values if the response is success
            exrProblemDesc.setText(null);
            extContractName.setText(null);
            extModelNo.setText(null);
            spnSerialNumberPosition = 0 ;
            spnProductPosition = 0 ;
            spnSubProductPosition = 0;
            spnCallCategoryPosition = 0;
            spnWorkTypePosition = 0;
            txtDatePicker.setText(null);
            txtTimePicker.setText(null);
            pDialog.dismiss();
            opendialog();
        }
    }

    @Override
        public void onError (Throwable throwable)
    {
        Toast.makeText(getActivity(),"Ticket cannot be rasied",Toast.LENGTH_SHORT);
        pDialog.dismiss();

        }

    public void opendialog()
    {
        RaiseTicketDialogBox raiseTicketDialogBox = new RaiseTicketDialogBox(getActivity());
        raiseTicketDialogBox.show(getFragmentManager(),"Raise_ticket_sucessfull");
    }

    //code for spinner after an item is selected
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        if (parent.getId() == R.id.spnProduct)
        {
            spnProductPosition = position;
            if (position > 0) {
                Log.e("hell","-----"+spnProductPosition);
                Log.e("hell","-----"+uproductObject.get(position).getProductId());
                //calling the sub product api
                productspinnerlay.setBackgroundResource(R.drawable.bg_box_grey);
                productspinnerlay.setPadding(23, 0, 23, 0);
                getSubProductItems(uproductObject.get(position).getProductId());
            } else
                {
                Log.e("hell","-----"+position);
                }
        }

        if (parent.getId() == R.id.spnSubProduct) {
            spnSubProductPosition = position;
            if (position > 0) {
                // Log.e("hell","-----"+spnSubProductPosition);
                // Log.e("hell","-----"+usubproductObject.get(position).getProductId());
                //code for calling the details api for getting the serial number and other stuffs
                subcategoryspinnerlay.setBackgroundResource(R.drawable.bg_box_grey);
                subcategoryspinnerlay.setPadding(23, 0, 23, 0);
                getDetailsItems((usubproductObject.get(position).getProductSubId()),(uproductObject.get(spnProductPosition).getProductId()));
            } else {
                Log.e("hell","-----"+position);
            }
        }

        if (parent.getId() == R.id.spnCallCategory)
        {
            spnCallCategoryPosition = position;
            callcategorylay.setBackgroundResource(R.drawable.bg_box_grey);
            callcategorylay.setPadding(23, 0, 23, 0);
        }

        if (parent.getId() == R.id.spnWorkType)
        {
            spnWorkTypePosition = position;
            worktypelay.setBackgroundResource(R.drawable.bg_box_grey);
            worktypelay.setPadding(23, 0, 23, 0);
        }


        if(parent.getId() == R.id.spnSerialNumber)
        {
            //getting the respective model no and contract name for the serial number
            spnSerialNumberPosition = position;
            if (position > 0)
            {
                extModelNo.setText(serialnumberObject.get(spnSerialNumberPosition).getModelNo());
                extContractName.setText(serialnumberObject.get(spnSerialNumberPosition).getContractType());
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }
}


