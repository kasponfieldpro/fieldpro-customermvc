package com.fieldprov2.fieldprocustomer.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fieldprov2.fieldprocustomer.R;
import com.fieldprov2.fieldprocustomer.model.Products;
import com.fieldprov2.fieldprocustomer.model.product_list.Datum;

import java.util.ArrayList;
import java.util.List;

public class MyProductAdapter extends RecyclerView.Adapter<MyProductAdapter.ViewHolder> {

    private List<Datum> list;
    private Context context;

    public MyProductAdapter(List<Datum> ticketsList, Context context) {
        list = ticketsList;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_myproduct, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        holder.txtProduct.setText("" + list.get(position).getProduct());
        holder.txtSubProduct.setText("" + list.get(position).getSubProduct());
        holder.txtSerialNumber.setText("" + list.get(position).getSerialNo());
        holder.txtModelNo.setText("" + list.get(position).getModelNo());
        holder.txtContractType.setText("" + list.get(position).getContractType());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtProduct, txtSubProduct, txtSerialNumber, txtModelNo, txtContractType;
        ViewGroup viewGroup;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtProduct = itemView.findViewById(R.id.txtProduct);
            txtSubProduct = itemView.findViewById(R.id.txtSubProduct);
            txtSerialNumber = itemView.findViewById(R.id.txtSerialNumber);
            txtModelNo = itemView.findViewById(R.id.txtModelNo);
            txtContractType = itemView.findViewById(R.id.txtContractType);
            viewGroup = itemView.findViewById(R.id.content);


        }
    }


}
