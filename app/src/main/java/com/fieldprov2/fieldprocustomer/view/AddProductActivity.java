package com.fieldprov2.fieldprocustomer.view;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.fieldprov2.fieldprocustomer.R;
import com.fieldprov2.fieldprocustomer.controller.RegistrationController;
import com.fieldprov2.fieldprocustomer.model.add_product_res.AddProductResponse;
import com.fieldprov2.fieldprocustomer.model.amc_response.AMCResponse;
import com.fieldprov2.fieldprocustomer.model.city_res.CityResponse;
import com.fieldprov2.fieldprocustomer.model.country_response.CountryResponse;
import com.fieldprov2.fieldprocustomer.model.location_res.LocationResponse;
import com.fieldprov2.fieldprocustomer.model.product_root.Datum;
import com.fieldprov2.fieldprocustomer.model.product_root.ProductResponse;
import com.fieldprov2.fieldprocustomer.model.registration_res.RegistrationResponse;
import com.fieldprov2.fieldprocustomer.model.state_res.StateResponse;
import com.fieldprov2.fieldprocustomer.model.sub_product_root.DataItem;
import com.fieldprov2.fieldprocustomer.model.sub_product_root.SubProductResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;
import com.fieldprov2.fieldprocustomer.network.RetrofitClient;
import com.fieldprov2.fieldprocustomer.view.login.LoginActivity;
import com.fieldprov2.fieldprocustomer.view.profile.ProfileActivity;
import com.fieldprov2.fieldprocustomer.view.registration.RegistrationActivity;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;
import java.util.regex.Pattern;

import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;

public class AddProductActivity extends AppCompatActivity implements RegistrationController.ProductResCallbacks,RegistrationController.SubProductResCallbacks,RegistrationController.AMCResCallbacks,RegistrationController.CountryResCallbacks,RegistrationController.StateResCallbacks,RegistrationController.CityResCallbacks,RegistrationController.LocationResCallbacks,RegistrationController.AddProductResCallbacks, AdapterView.OnItemSelectedListener {

    ApiInterface apiInterface;
    RegistrationController registrationController;

    private int PermissionCode = 1;
    TextInputLayout textInputProductName, textInputModel, textInputSerialNo, textInputProductDesc, textInputPlotNo, textInputStreet, textInputPostCode,textInputLandmark;
    TextInputEditText modelnumberID,serialnumberID,plotnumberID,streetID,postcodeID,landmarkID;
    Spinner spnProductId, spnProductSubId, spnAMCid;
    SpinnerDialog spinnerDialog_country,spinnerDialog_state,spinnerDialog_city,spinnerDialog_location;
    Button addproduct, spnCountryId, spnStateId, spnCityId, spnLocationId;
    private LinearLayout productspinnerlay, subcategoryspinnerlay, amclay, countrylay,statelay,citylay,locationlay;
    TextView txtDatePicker,textInputContractDuration;
    private String cus_code,Token,date,add_token;
    ProgressDialog pDialog;
    String conduration,plotnumber,postcode,street,country,state,city,location,country_id,state_id,city_id,location_id,location_position;


    int spnProductIdPosition = 0, spnProductSubIdPosition = 0,spnAMCidPosition = 0, spnCountryIdPosition = 0, spnStateIdPosition = 0,
            spnCityIdPosition = 0, spnLocationIdPosition = 0;;


            //creating the array list for the spinners
            ArrayList<String> productItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.product_root.Datum> productObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.product_root.Datum>();

    ArrayList<String> subProductItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.sub_product_root.DataItem> subProductObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.sub_product_root.DataItem>();

    ArrayList<String> amcItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.amc_response.DataItem> amcObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.amc_response.DataItem>();

    ArrayList<String> cityItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.city_res.DataItem> cityObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.city_res.DataItem>();

    ArrayList<String> countryItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.country_response.DataItem> countryObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.country_response.DataItem>();

    ArrayList<String> stateItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.state_res.DataItem> stateObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.state_res.DataItem>();

    ArrayList<String> locationItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.location_res.DataItem> locationObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.location_res.DataItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        textInputModel = findViewById(R.id.textInputModel);
        textInputSerialNo = findViewById(R.id.textInputSerialNo);

        spnProductId = findViewById(R.id.spnProduct);
        spnProductSubId = findViewById(R.id.spnSubCategory);
        spnAMCid = findViewById(R.id.spnAMC);

        txtDatePicker = findViewById(R.id.txtDatePicker);

        textInputContractDuration = findViewById(R.id.textInputContractDuration);

        addproduct = findViewById(R.id.btnAddproduct);

        textInputPlotNo = findViewById(R.id.textInputPlotNo);
        textInputStreet = findViewById(R.id.textInputStreet);
        textInputPostCode = findViewById(R.id.textInputPostCode);
        spnCountryId = findViewById(R.id.spnCountry);
        spnStateId = findViewById(R.id.spnState);
        spnCityId = findViewById(R.id.spnCity);
        spnLocationId = findViewById(R.id.spnLocation);
        textInputLandmark = findViewById(R.id.textInputLandmark);

        modelnumberID = findViewById(R.id.modelnumberID);
        serialnumberID = findViewById(R.id.serialnumberID);
        plotnumberID = findViewById(R.id.plotnumberID);
        streetID = findViewById(R.id.streetID);
        postcodeID = findViewById(R.id.postcodeID);
        landmarkID = findViewById(R.id.landmarkID);

        productspinnerlay = findViewById(R.id.productspinner_lay);
        subcategoryspinnerlay = findViewById(R.id.subcategoryspinner_lay);
        amclay = findViewById(R.id.amcTypespinner_lay);
        countrylay = findViewById(R.id.countryspinner_lay);
        statelay = findViewById(R.id.statespinner_lay);
        citylay = findViewById(R.id.cityspinner_lay);
        locationlay = findViewById(R.id.locationspinner_lay);

        registrationController = new RegistrationController();
        apiInterface = RetrofitClient.getClient().create(ApiInterface.class);

        getProductItems();
        getAMCItems();
        getCountryItems();

        spnProductId.setOnItemSelectedListener(this);
        spnAMCid.setOnItemSelectedListener(this);
        spnProductSubId.setOnItemSelectedListener(this);

        country_id = Pref_storage.getDetail(AddProductActivity.this,"country_id");
        state_id = Pref_storage.getDetail(AddProductActivity.this,"state_id");
        city_id = Pref_storage.getDetail(AddProductActivity.this,"city_id");
        location_id = Pref_storage.getDetail(AddProductActivity.this,"location_id");
        location_position = Pref_storage.getDetail(AddProductActivity.this,"location_position");

        if(country_id != null)
        {
            getStateItems(Integer.parseInt(country_id));
        }
        if(state_id != null)
        {
            getCityItems(Integer.parseInt(state_id));
        }
        if(city_id != null)
        {
            getLocationItems(Integer.parseInt(city_id));

        }


        //code for the spinner to close the keypad
        spnProductId.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnProductId.getWindowToken(), 0);
                return false;
            }
        }) ;
        spnAMCid.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnAMCid.getWindowToken(), 0);
                return false;
            }
        }) ;
        spnProductSubId.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnProductSubId.getWindowToken(), 0);
                return false;
            }
        }) ;

        spnStateId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(stateObject.size()>0) {
                    spinnerDialog_state.showSpinerDialog();
                    city_id =  String.valueOf(cityObject.get(spnCityIdPosition+1).getCityId());
                }
                else
                {
                    Toast.makeText(AddProductActivity.this,"Please select country first",Toast.LENGTH_SHORT).show();
                }
                InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnStateId.getWindowToken(), 0);
            }
        });

       spnCityId.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if(cityObject.size()>0) {
                   spinnerDialog_city.showSpinerDialog();
                   state_id = String.valueOf(stateObject.get(spnStateIdPosition+1).getStateId());
               }
               else
               {
                   Toast.makeText(AddProductActivity.this,"Please select country and state first",Toast.LENGTH_SHORT).show();
               }
               InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
               imm.hideSoftInputFromWindow(spnCityId.getWindowToken(), 0);
           }
       });

       spnLocationId.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if(locationObject.size()>0)
               {
                   spinnerDialog_location.showSpinerDialog();
                   location_id = String.valueOf(locationObject.get(spnLocationIdPosition+1).getLocationId());
               }
               else {
                   Toast.makeText(AddProductActivity.this,"Please select country state and city first",Toast.LENGTH_SHORT).show();
               }
               InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
               imm.hideSoftInputFromWindow(spnLocationId.getWindowToken(), 0);
           }
       });

       spnCountryId.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               spinnerDialog_country.showSpinerDialog();
               InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
               imm.hideSoftInputFromWindow(spnCountryId.getWindowToken(), 0);
               country_id = String.valueOf(countryObject.get(spnCountryIdPosition+1).getCountryId());
           }
       });



        modelnumberID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textInputModel.setError(null);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        serialnumberID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textInputSerialNo.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        plotnumberID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textInputPlotNo.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
   /*             Pattern plotnumber = Pattern.compile("[0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@,&#/]");
                if(!plotnumber.matcher(s.toString().trim()).find()){
                    textInputPlotNo.setError("The typed format is not right");
                }
                else
                {
                    textInputPlotNo.setError(null);
                } */

            }
        });

        streetID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textInputStreet.setError(null);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        postcodeID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textInputPostCode.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                postcode = postcodeID.getEditableText().toString();
            }
        });

        plotnumber = Pref_storage.getDetail(AddProductActivity.this,"plot_number");
        street = Pref_storage.getDetail(AddProductActivity.this,"street");
        postcode = Pref_storage.getDetail(AddProductActivity.this,"post_code");
        country = Pref_storage.getDetail(AddProductActivity.this,"country");
        state = Pref_storage.getDetail(AddProductActivity.this,"state");
        city = Pref_storage.getDetail(AddProductActivity.this,"city");
        location = Pref_storage.getDetail(AddProductActivity.this,"location");

        plotnumberID.setText(plotnumber);
        streetID.setText(street);
        postcodeID.setText(postcode);
        spnCountryId.setText(country);
        spnStateId.setText(state);
        spnCityId.setText(city);
        spnLocationId.setText(location);


        //code for date picker
        txtDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                final int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                DatePickerDialog picker = new DatePickerDialog(AddProductActivity.this,
                        new DatePickerDialog.OnDateSetListener()
                        {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                            {
                                txtDatePicker.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                date = String.valueOf(year +"-" +(monthOfYear +1)+ "-" + dayOfMonth);
                            }
                        }, year, month, day);
                picker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                Calendar maxDate = Calendar.getInstance();
                //code for limiting the date to two weeks
                maxDate.set(Calendar.DAY_OF_MONTH, day + 14);
                maxDate.set(Calendar.MONTH, month);
                maxDate.set(Calendar.YEAR, year);
                picker.getDatePicker().setMaxDate(maxDate.getTimeInMillis());

                picker.show();
            }
        });

        addproduct.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if(validate())
                {
                    //loading graphics and sending values to the api for adding a product
                    pDialog = ProgressDialog.show(AddProductActivity.this, "FieldPro","Loading...", true);
                    pDialog.setCancelable(true);
                    cus_code = Pref_storage.getDetail(AddProductActivity.this, "customer_code");
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("customer_code", cus_code);
                    jsonObject.addProperty("product_id", productObject.get(spnProductIdPosition).getProduct_id());
                    jsonObject.addProperty("product_sub_id", subProductObject.get(spnProductSubIdPosition).getProductSubId());
                    jsonObject.addProperty("model_no", textInputModel.getEditText().getText().toString());
                    jsonObject.addProperty("serial_no", textInputSerialNo.getEditText().getText().toString());
                    jsonObject.addProperty("contract_type_id", amcObject.get(spnAMCidPosition).getAmcId());
                    jsonObject.addProperty("contract_duration", Integer.parseInt(textInputContractDuration.getText().toString()));
                    jsonObject.addProperty("start_date",date);
                    jsonObject.addProperty("plot_number", plotnumberID.getText().toString());
                    jsonObject.addProperty("street",streetID.getText().toString());
                    jsonObject.addProperty("post_code", postcodeID.getText().toString());
                    jsonObject.addProperty("landmark", textInputLandmark.getEditText().getText().toString());
                    jsonObject.addProperty("country_id", country_id);
                    jsonObject.addProperty("state_id", state_id);
                    jsonObject.addProperty("city_id", city_id);
                    jsonObject.addProperty("location_id", locationObject.get(spnLocationIdPosition+1).getLocationId());

         //           Pref_storage.setDetail(AddProductActivity.this,"location_position",String.valueOf(spnLocationIdPosition));
         //           Pref_storage.setDetail(AddProductActivity.this,"location_id",String.valueOf(locationObject.get(spnLocationIdPosition).getLocationId()));

                    callAddProductCall(jsonObject);
                }
            }
        });



    }

    //calling the api
    private void callAddProductCall(JsonObject jsonObject)
    {
        Token = Pref_storage.getDetail(AddProductActivity.this, "Token");
        registrationController.postAddProduct(apiInterface, jsonObject, this,Token);
    }

    private void getProductItems() {
        registrationController.getProducts(apiInterface, this);
    }

    //setting the adapter
    private void setProductAdapter() {
        ArrayAdapter<String> ad = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, productItem);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnProductId.setAdapter(ad);
    }

    private void getSubProductItems(int product_id) {
        registrationController.getSubProducts(apiInterface, this, product_id);
    }

    private void setSubProductAdapter() {
        ArrayAdapter<String> ad = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, subProductItem);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnProductSubId.setAdapter(ad);
    }

    private void getAMCItems() {
        registrationController.getAMC(apiInterface, this);
    }

    private void setAMCAdapter() {
        ArrayAdapter<String> ad = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, amcItem);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnAMCid.setAdapter(ad);
    }

    private void getCountryItems() {
        registrationController.getCountry(apiInterface, this);
    }

    private void setCountryAdapter() {
        spinnerDialog_country = new SpinnerDialog(AddProductActivity.this,countryItem,"select country");
        spinnerDialog_country.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String s, int position) {
                spnCountryIdPosition = position;
                {
                    countrylay.setBackgroundResource(R.drawable.bg_box_grey);
                    countrylay.setPadding(23, 0, 23, 0);
                    stateItem.clear();
                    stateObject.clear();
                    spnStateId.setText("Select State");
                    spnStateIdPosition = 0;
                    spnCityIdPosition = 0;
                    spnLocationIdPosition = 0;
                    spnCountryId.setText(countryObject.get(spnCountryIdPosition+1).getCountryName());
                    getStateItems(countryObject.get(position+1).getCountryId());
                }
            }
        });

    }

    private void getStateItems(int countryId) {
        registrationController.getState(apiInterface, this, countryId);
    }

    private void setStateAdapter() {
        spinnerDialog_state = new SpinnerDialog(AddProductActivity.this,stateItem,"Select State");
        spinnerDialog_state.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String s, int position) {
                spnStateIdPosition = position;
                {
                    statelay.setBackgroundResource(R.drawable.bg_box_grey);
                    statelay.setPadding(23, 0, 23, 0);
                    cityItem.clear();
                    cityObject.clear();
                    spnCityId.setText("Select City");
                    spnStateId.setText(stateObject.get(spnStateIdPosition+1).getStateName());
                    getCityItems(stateObject.get(position+1).getStateId());
                }
            }
        });

    }

    private void getCityItems(int stateId) { registrationController.getCity(apiInterface, this, stateId);
    }

    private void setCityAdapter() {
        spinnerDialog_city = new SpinnerDialog(AddProductActivity.this,cityItem,"Select City");
        spinnerDialog_city.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String s, int position) {
                spnCityIdPosition = position;
                {
                    citylay.setBackgroundResource(R.drawable.bg_box_grey);
                    citylay.setPadding(23, 0, 23, 0);
                    locationItem.clear();
                    locationObject.clear();
                    spnLocationId.setText("Select Location");
                    spnCityId.setText(cityObject.get(spnCityIdPosition+1).getCityName());
                    getLocationItems(cityObject.get(position+1).getCityId());
                }
            }
        });
    }

    private void getLocationItems(int city_id) {
        registrationController.getLocation(apiInterface, this, city_id);
    }

    private void setLocationAdapter() {
        spinnerDialog_location = new SpinnerDialog(AddProductActivity.this,locationItem,"Select City");
        spinnerDialog_location.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String s, int position) {
                spnLocationIdPosition = position;

                {
                    locationlay.setBackgroundResource(R.drawable.bg_box_grey);
                    locationlay.setPadding(23, 0, 23, 0);
                    spnLocationId.setText(locationObject.get(spnLocationIdPosition+1).getLocationName());
                }
            }
        });


    }



   //code for the spinner when a item is selected
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        if (parent.getId() == R.id.spnProduct)
        {
            spnProductIdPosition = position;
            Log.e("message","-------"+spnProductIdPosition);
            if (position > 0)
            {
                productspinnerlay.setBackgroundResource(R.drawable.bg_box_grey);
                productspinnerlay.setPadding(23, 0, 23, 0);
                subProductItem.clear();
                subProductObject.clear();
                getSubProductItems(productObject.get(position).getProduct_id());
            }
        }
        if (parent.getId() == R.id.spnSubCategory) {
            spnProductSubIdPosition = position;
            subcategoryspinnerlay.setBackgroundResource(R.drawable.bg_box_grey);
            subcategoryspinnerlay.setPadding(23, 0, 23, 0);
//            if (position > 0) {
//                amcItem.clear();
//                amcObject.clear();
//                getAMCItems();
//            }
        }
        if (parent.getId() == R.id.spnAMC) {
            spnAMCidPosition = position;

            if (position > 0) {
                amclay.setBackgroundResource(R.drawable.bg_box_grey);
                amclay.setPadding(23, 0, 23, 0);
                conduration = String.valueOf(amcObject.get(spnAMCidPosition).getDuration());
                textInputContractDuration.setText(conduration + " months");
            }


//            if (position > 0) {
//                countryItem.clear();
//                countryObject.clear();
//                getSubProductItems(productObject.get(position).getProduct_id());
//            }
        }


   /*     if (parent.getId() == R.id.spnCountry) {
            spnCountryIdPosition = position;
            if (position > 0) {
                stateItem.clear();
                stateObject.clear();
                getStateItems(countryObject.get(position).getCountryId());
            }
        }
        if (parent.getId() == R.id.spnState) {
            spnStateIdPosition = position;
            if (position > 0) {
                cityItem.clear();
                cityObject.clear();
                getCityItems(stateObject.get(position).getStateId());
            }
        }
        if (parent.getId() == R.id.spnCity) {
            spnCityIdPosition = position;
            if (position > 0) {
                locationItem.clear();
                locationObject.clear();
                getLocationItems(cityObject.get(position).getCityId());
            }
        } */
//        if (parent.getId() == R.id.spnLocation) {
//            spnLocationIdPosition = position;
//            if(position>0)
//            {

 //           }
//            if (position > 0) {
//                locationItem.clear();
//                locationObject.clear();
//                getSubProductItems(productObject.get(position).getProduct_id());
//            }
 //       }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private Boolean hasPermissions(Context context, String[] permissions)
    {
        if (context != null) {

            for (String permi : permissions)
            {
                if (ActivityCompat.checkSelfPermission(context, permi) != PackageManager.PERMISSION_GRANTED)
                {
                    return false;
                }
            }
        }
        return true;
    }


    //validating the values if it is empty or not
    private boolean validate() {
        boolean valid = true;

        if (Objects.requireNonNull(textInputModel.getEditText()).getText().toString().equals(""))
        {
            textInputModel.setError("Please enter model no ");
            modelnumberID.requestFocus();
            valid = false;
        }

        if (Objects.requireNonNull(textInputSerialNo.getEditText()).getText().toString().equals("")) {
            textInputSerialNo.setError("Please enter serial number ");
            serialnumberID.requestFocus();
            valid = false;
        }


        if  (Objects.requireNonNull(plotnumberID.getText()).toString().equals(""))
        {
            textInputPlotNo.setError("Please enter input plot no ");
            plotnumberID.requestFocus();
            valid = false;
        }

   /*     Pattern plotnumber = Pattern.compile("[0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@,&#/]");
        if(!plotnumber.matcher(Objects.requireNonNull(plotnumberID.getText()).toString().trim()).find()){
            textInputPlotNo.setError("The typed format is not right");
            valid = false;
        } */


        if  (Objects.requireNonNull(textInputStreet.getEditText()).getText().toString().equals(""))
        {
            textInputStreet.setError("Please enter street no ");
            streetID.requestFocus();
            valid = false;
        }

        if (Objects.requireNonNull(textInputPostCode.getEditText()).getText().toString().equals(""))
        {
            textInputPostCode.setError("Please enter post code ");
            postcodeID.requestFocus();
            valid = false;
        }


        if (txtDatePicker.getText().toString().equals(getString(R.string.select_date)))
        {
            Toast.makeText(AddProductActivity.this, "Select Date", Toast.LENGTH_SHORT).show();
            txtDatePicker.requestFocus();
            valid = false;
        }

        //creating validation for spinners
        if  (spnProductIdPosition == 0)
        {
            productspinnerlay.setBackgroundResource(R.drawable.boarder);
            productspinnerlay.setPadding(23, 0, 23, 0);
            valid = false;
        }


        if  ( spnProductSubIdPosition == 0 )
        {
            subcategoryspinnerlay.setBackgroundResource(R.drawable.boarder);
            subcategoryspinnerlay.setPadding(23, 0, 23, 0);
            valid = false;
        }


        if  ( spnAMCidPosition == 0)
        {
            amclay.setBackgroundResource(R.drawable.boarder);
            amclay.setPadding(23, 0, 23, 0);
            valid = false;
        }


        if  (spnCountryId.getText().toString().equals("Select Country") )

        {
            countrylay.setBackgroundResource(R.drawable.boarder);
            countrylay.setPadding(23, 0, 23, 0);
            valid = false;
        }


        if  (spnStateId.getText().toString().equals("Select State"))

        {
            statelay.setBackgroundResource(R.drawable.boarder);
            statelay.setPadding(23, 0, 23, 0);
            valid = false;
        }



        if  (spnCityId.getText().toString().equals("Select City"))

        {
            citylay.setBackgroundResource(R.drawable.boarder);
            citylay.setPadding(23, 0, 23, 0);
            valid = false;
        }


        if  (spnLocationId.getText().toString().equals("Select Location"))
        {
            locationlay.setBackgroundResource(R.drawable.boarder);
            locationlay.setPadding(23, 0, 23, 0);
            valid = false;
        }

        return valid;
    }

    //code after getting the response
    @Override
    public void onSuccess(ProductResponse value)
    {
        if (value.getResponse() != null) {

            Log.e("test","--->"+new Gson().toJson(value.getResponse().getData()));
            productObject.add(value.getResponse().getData().get(0));
            productItem.add("Select Product");

            for (Datum datum : value.getResponse().getData()) {
                productObject.add(datum);
                productItem.add(datum.getProduct_name());
                Log.e("test","--->"+new Gson().toJson(productItem));
            }
            setProductAdapter();
        }
    }

    @Override
    public void onSuccess(SubProductResponse value)
    {
        if (value.getResponse() != null) {
            //if the product does not have any sub product then it does not open
            if (value.getResponse().getData().size() > 0) {
                subProductObject.add(value.getResponse().getData().get(0));
                subProductItem.add("Select Sub Product");

                for (DataItem datum : value.getResponse().getData()) {
                    subProductObject.add(datum);
                    subProductItem.add(datum.getProductSubName());
                }
                setSubProductAdapter();
            }
            else {
                subProductItem.add("There are no sub product for this product");
                setSubProductAdapter();
                Toast.makeText(AddProductActivity.this, "this product does not have a sub product", Toast.LENGTH_SHORT).show();
            }
        } else {
            subProductItem.add("There are no sub product for this product");
            setSubProductAdapter();
            Toast.makeText(AddProductActivity.this, "this product does not have a sub product", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSuccess(AMCResponse value)
    {
        if (value.getResponse() != null) {

            amcObject.add(value.getResponse().getData().get(0));
            amcItem.add("Select AMC Type");

            for (com.fieldprov2.fieldprocustomer.model.amc_response.DataItem datum : value.getResponse().getData())
            {
                amcObject.add(datum);
                amcItem.add(datum.getAmcType());
            }
            setAMCAdapter();
        }
    }



    @Override
    public void onSuccess(CountryResponse value)
    {
        if (value.getResponse() != null) {

            countryObject.add(value.getResponse().getData().get(0));
    //        countryItem.add("Select Country");

            for (com.fieldprov2.fieldprocustomer.model.country_response.DataItem datum : value.getResponse().getData())
            {
                countryObject.add(datum);
                countryItem.add(datum.getCountryName());
            }
            setCountryAdapter();
        }

    }

    @Override
    public void onSuccess(StateResponse value)
    {
        if (value.getResponse() != null) {
            if (value.getResponse().getData().size() > 0) {
                stateObject.add(value.getResponse().getData().get(0));
                //         stateItem.add("Select State");

                for (com.fieldprov2.fieldprocustomer.model.state_res.DataItem datum : value.getResponse().getData()) {
                    stateObject.add(datum);
                    stateItem.add(datum.getStateName());
                }
                setStateAdapter();
            } else {
                stateItem.add("This country does not have any state with it");
                setStateAdapter();
                Toast.makeText(AddProductActivity.this, "this product does not have a sub product", Toast.LENGTH_SHORT).show();
            }

        } else {
            stateItem.add("This country does not have any state with it");
            setStateAdapter();
            Toast.makeText(AddProductActivity.this, "this product does not have a sub product", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onSuccess(CityResponse value)
    {
        if (value.getResponse() != null) {

            if (value.getResponse().getData().size() > 0) {

                cityObject.add(value.getResponse().getData().get(0));
//            cityItem.add("Select City");

                for (com.fieldprov2.fieldprocustomer.model.city_res.DataItem datum : value.getResponse().getData()) {
                    cityObject.add(datum);
                    cityItem.add(datum.getCityName());
                }
                setCityAdapter();
            } else {
                cityItem.add("No City");
                setCityAdapter();
                Toast.makeText(AddProductActivity.this, "this state does not have a city", Toast.LENGTH_SHORT).show();
            }

        } else {
            cityItem.add("No city");
            setCityAdapter();
            Toast.makeText(AddProductActivity.this, "this state does not have a city", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onSuccess(LocationResponse value)
    {
        if (value.getResponse() != null ) {
            if (value.getResponse().getData().size() > 0) {

                locationObject.add(value.getResponse().getData().get(0));
                //          locationItem.add("Select Location ");

                for (com.fieldprov2.fieldprocustomer.model.location_res.DataItem datum : value.getResponse().getData()) {
                    locationObject.add(datum);
                    locationItem.add(datum.getLocationName());
                }
                setLocationAdapter();
            }
            else
            {
                locationItem.add("This state does have a location");
                setLocationAdapter();
                Toast.makeText(AddProductActivity.this,"This city does not have a location",Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            locationItem.add("This state does have a location");
            setLocationAdapter();
            Toast.makeText(AddProductActivity.this,"This city does not have a location",Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onSuccess(AddProductResponse value) {
        if (value.getResponse() != null)
        {
            Toast.makeText(this, value.getResponse().getMessage(), Toast.LENGTH_SHORT).show();
            add_token = value.getResponse().getToken();
            Pref_storage.setDetail(AddProductActivity.this,"Token",add_token);
            pDialog.dismiss();
            Intent intent = new Intent(AddProductActivity.this, MainActivity.class);
            intent.putExtra("thor","man");
            startActivity(intent);
        }
    }

    @Override
    public void onError(Throwable throwable) {
        pDialog.dismiss();

    }
}