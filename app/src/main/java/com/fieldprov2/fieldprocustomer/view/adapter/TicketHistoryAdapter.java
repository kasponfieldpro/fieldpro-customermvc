package com.fieldprov2.fieldprocustomer.view.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fieldprov2.fieldprocustomer.R;
import com.fieldprov2.fieldprocustomer.model.Tickets;
import com.fieldprov2.fieldprocustomer.model.ticket_list.Datum;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TicketHistoryAdapter extends RecyclerView.Adapter<TicketHistoryAdapter.ViewHolder> {

    private List<Datum> list;
    private Context context;
    String date;
    TextView product,subproduct,model,serial_number,technician_name,technician_number,work_type,description,status,ticket_id_header;
    ImageView cancel_image;

    public TicketHistoryAdapter( List<Datum> ticketsList, Context context) {
        list = ticketsList;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ticket_history, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position)
    {
        holder.txtTicketId.setText("" + list.get(position).getTicketId());
        holder.txtTicketRaiseDate.setText(changedate(list.get(position).getRaisedDateTime()));
        holder.txtWorkType.setText("" + list.get(position).getWorkType());
        holder.txtStatus.setText("" + list.get(position).getStatusName());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ViewGroup viewGroup = holder.itemView.findViewById(android.R.id.content);

                //then we will inflate the custom alert dialog xml that we created
                View dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_ticket, viewGroup, false);


                //Now we need an AlertDialog.Builder object
                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                //setting the view of the builder to our custom view that we already inflated
                builder.setView(dialogView);

                //finally creating the alert dialog and displaying it
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();

                ticket_id_header = dialogView.findViewById(R.id.ticket_id_header);
                product = dialogView.findViewById(R.id.txtproduct);
                subproduct = dialogView.findViewById(R.id.txtSubProduct);
                model = dialogView.findViewById(R.id.txtmodelno);
                serial_number = dialogView.findViewById(R.id.txtSerialnumber);
                technician_name = dialogView.findViewById(R.id.txttechnicianname);
                technician_number = dialogView.findViewById(R.id.txttechniciannumber);
                work_type = dialogView.findViewById(R.id.txtworktype);
                description = dialogView.findViewById(R.id.txtdescription);
                status = dialogView.findViewById(R.id.txtstatus);
                cancel_image = dialogView.findViewById(R.id.cancel_button_1);

                ticket_id_header.setText(list.get(position).getTicketId());
                product.setText(list.get(position).getProduct());
                subproduct.setText(list.get(position).getSubProduct());
                model.setText(list.get(position).getModel());
                serial_number.setText(list.get(position).getSerialNo());
                technician_name.setText((CharSequence) list.get(position).getTechnicainName());
                technician_number.setText((CharSequence) list.get(position).getTechnicianNumber());
                work_type.setText(list.get(position).getWorkType());
                description.setText(list.get(position).getProblemDesc());
                status.setText(list.get(position).getStatusName());

                cancel_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });


                //               AlertDialog.Builder builder = new AlertDialog.Builder(context);
 //               View dialogView = LayoutInflater.from(v.getContext()).inflate(R.layout.customview, holder.itemView, false);
 //               builder.setView(dialogView);
//                AlertDialog alertDialog = builder.create();
//                alertDialog.show();

            }
        });

    }
    public String changedate(String changedate)
    {
        SimpleDateFormat targetFormat = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd" );
        Date date = null;
        try {
            date = originalFormat.parse(changedate);
            System.out.println("Old Format :   " + originalFormat.format(date));
            System.out.println("New Format :   " + targetFormat.format(date));

        } catch (ParseException ex) {
            // Handle Exception.
        }
        return targetFormat.format(date);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTicketId, txtTicketRaiseDate, txtWorkType, txtStatus;
        ViewGroup viewGroup;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtTicketId = itemView.findViewById(R.id.txtTicketId);
            txtTicketRaiseDate = itemView.findViewById(R.id.txtTicketRaiseDate);
            txtWorkType = itemView.findViewById(R.id.txtWorkType);
            txtStatus = itemView.findViewById(R.id.txtStatus);
            viewGroup = itemView.findViewById(R.id.content);




        }
    }


}
