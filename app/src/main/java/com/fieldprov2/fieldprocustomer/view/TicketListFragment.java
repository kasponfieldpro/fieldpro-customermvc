package com.fieldprov2.fieldprocustomer.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.fieldprov2.fieldprocustomer.R;
import com.fieldprov2.fieldprocustomer.controller.TicketListController;
import com.fieldprov2.fieldprocustomer.model.Tickets;
import com.fieldprov2.fieldprocustomer.model.change_password.ChangePasswordResponse;
import com.fieldprov2.fieldprocustomer.model.ticket_list.Datum;
import com.fieldprov2.fieldprocustomer.model.ticket_list.TicketListResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;
import com.fieldprov2.fieldprocustomer.network.RetrofitClient;
import com.fieldprov2.fieldprocustomer.view.adapter.TicketHistoryAdapter;
import com.fieldprov2.fieldprocustomer.view.profile.ProfileActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TicketListFragment extends Fragment implements TicketListController.TicketListResCallbacks {
    ApiInterface apiInterface;
    TicketListController ticketListController;
    private String cus_code;
    private String profileactivity_token;
    private String ticket_raisedate;
    private String work_type;
    private Datum status;
    private String ticket_id,ticketlist_token;
    ProgressDialog pDialog ;
    SwipeRefreshLayout pullToRefresh;
    public TicketListFragment() {
        // Required empty public constructor
    }

    public static TicketListFragment newInstance() {
        return new TicketListFragment();
    }

    RecyclerView recyclerView;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Objects.requireNonNull(getActivity()).setTitle("Ticket list");

        //loading graphics
        pDialog = ProgressDialog.show(getActivity(), "FieldPro","Loading...", true);
        pDialog.setCancelable(true);


    }

    //calling the api
    private void getProfileActivity()
    {
        profileactivity_token = Pref_storage.getDetail(getActivity(),"Token");
        cus_code = Pref_storage.getDetail(getActivity(), "customer_code");
        ticketListController.getTicketList(apiInterface,this,profileactivity_token,cus_code);
    }

    //creating the recycler view
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ticket_list, container, false);
        pullToRefresh = view.findViewById(R.id.pullToRefresh);

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullToRefresh.setRefreshing(true);
                pullToRefresh.setRefreshing(false);
            }
        });

        recyclerView = view.findViewById(R.id.rv);
        apiInterface = RetrofitClient.getClient().create(ApiInterface.class);
        ticketListController = new TicketListController();

      //  cus_code = Pref_storage.getDetail(getActivity(), "Token");
        getProfileActivity();

        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

     /*   ArrayList<Tickets> list = new ArrayList<Tickets>();
        list.add(new Tickets("100", "21/10/2020", "jb new", "Completed"));
        list.add(new Tickets("101", "22/10/2020", "PdfgI new", "Pemding"));
        list.add(new Tickets("102", "1/10/2020", "PdfgI new", "Completed"));
        list.add(new Tickets("103", "22/10/2020", "PgdfI new", "Completed"));*/


    }

    //code for the response
    @Override
    public void onSuccess(TicketListResponse value)
    {
        if (value.getResponse() != null && value.getResponse().getResponseCode().equals("200"))
        {
            List<Datum> data= value.getResponse().getData();
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setHasFixedSize(true);
            TicketHistoryAdapter ticketHistoryAdapter = new TicketHistoryAdapter(data, getActivity());
            recyclerView.setAdapter(ticketHistoryAdapter);
            ticketlist_token = value.getResponse().getToken();
            Pref_storage.setDetail(getActivity(),"Token",ticketlist_token);
            pDialog.dismiss();
        }
        else if(value.getResponse().getResponseCode().equals("400"))
        {
            pDialog.dismiss();
            ticketlist_token = value.getResponse().getToken();
            Pref_storage.setDetail(getActivity(),"Token",ticketlist_token);
            Toast.makeText(getActivity(),"No data to display",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onError(Throwable throwable) {
        pDialog.dismiss();

    }
}