package com.fieldprov2.fieldprocustomer.view;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fieldprov2.fieldprocustomer.R;
import com.fieldprov2.fieldprocustomer.controller.RegistrationController;
import com.fieldprov2.fieldprocustomer.model.AddProductPojo;
import com.fieldprov2.fieldprocustomer.model.SerialNoPojos;
import com.fieldprov2.fieldprocustomer.model.add_amc_res.AddAMCResponse;
import com.fieldprov2.fieldprocustomer.model.amc_response.AMCResponse;
import com.fieldprov2.fieldprocustomer.model.city_res.CityResponse;
import com.fieldprov2.fieldprocustomer.model.country_response.CountryResponse;
import com.fieldprov2.fieldprocustomer.model.location_res.LocationResponse;
import com.fieldprov2.fieldprocustomer.model.product_root.Datum;
import com.fieldprov2.fieldprocustomer.model.product_root.ProductResponse;
import com.fieldprov2.fieldprocustomer.model.serialNumberResponse.SerialNumberResponse;
import com.fieldprov2.fieldprocustomer.model.state_res.StateResponse;
import com.fieldprov2.fieldprocustomer.model.sub_product_root.DataItem;
import com.fieldprov2.fieldprocustomer.model.sub_product_root.SubProductResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;
import com.fieldprov2.fieldprocustomer.network.RetrofitClient;
import com.fieldprov2.fieldprocustomer.view.adapter.SerialNumberAdapter;
import com.fieldprov2.fieldprocustomer.view.registration.RegistrationActivity;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.TreeSet;
import java.util.stream.Collectors;

import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;

import static com.fieldprov2.fieldprocustomer.R.id.postcodeID;

public class AddAmcActivity extends AppCompatActivity implements SerialNumberAdapter.DeleteInterface, AdapterView.OnItemSelectedListener, RegistrationController.ProductResCallbacks, RegistrationController.SubProductResCallbacks
        , RegistrationController.AMCResCallbacks, RegistrationController.CountryResCallbacks, RegistrationController.StateResCallbacks, RegistrationController.CityResCallbacks, RegistrationController.LocationResCallbacks
        , RegistrationController.AddAMCResCallbacks, RegistrationController.SerialNumberResponseCallbacks {

    ApiInterface apiInterface;
    RegistrationController registrationController;
    ProgressDialog pDialog ;

    TextInputLayout txtInputModelNo, txtInputInvoiceNo, txtInputQuantity, txtInputAmount, txtInputProductSerialNo, textInputLandmark;
    TextInputLayout txtInputCustomerName, txtInputEmail, textInputMobileNo, textInputAlternateMobileNo, textInputPlotNo, textInputStreet, textInputPostCode;
    TextInputEditText modelnumberID,invoicenumberID,serialnumberID,plotnumberID,streetID,postcodeID,landmarkID;
    EditText textInputContractDuration, extStartDate;
    Spinner spnAMCId, spnProductId, spnSubProductId;
    SpinnerDialog spinnerDialog_country,spinnerDialog_state,spinnerDialog_city,spinnerDialog_location;
    TextView txtAddProductSeries, txtSaveProductSeries, txtDatePicker;
    ConstraintLayout constProductSerialNumber;
    List<String> listSerialNumber = new ArrayList<>();
    String country_id,state_id,city_id,location_id;
    List<SerialNoPojos> serialNoPojosList = new ArrayList<>();
    RecyclerView recyclerView;
    SerialNumberAdapter serialNumberAdapter;
    Button btnGenerate, spnCountryId, spnStateId, spnCityId, spnLocationId;
    String languageknown;
    String name, email, mobile_no, alt_mobile_no, plot_no, street, post_code, cus_code, date, Token, model,landmark,amc_token,country,state,city,location;
    int product_id;
    int product_sub_id,a,b;
    String conduration,serialnumber_size,amc_cost,c,location_position;
    private LinearLayout productspinnerlay, subcategoryspinnerlay, amclay, countrylay, statelay, citylay, locationlay, prioritylay;
    TreeSet<String> personalInfolanguagesPojos = new TreeSet<>();
    String[] scriptsSerialNo;

    int spnProductIdPosition = 0, spnProductSubIdPosition = 0,
            spnAMCidPosition = 0, spnCountryIdPosition = 0, spnStateIdPosition = 0,
            spnCityIdPosition = 0, spnLocationIdPosition = 0,spnPriorityPosition = 0;


    //creating array list for spinners
    ArrayList<String> productItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.product_root.Datum> productObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.product_root.Datum>();

    ArrayList<String> subProductItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.sub_product_root.DataItem> subProductObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.sub_product_root.DataItem>();

    ArrayList<String> cityItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.city_res.DataItem> cityObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.city_res.DataItem>();

    ArrayList<String> countryItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.country_response.DataItem> countryObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.country_response.DataItem>();

    ArrayList<String> stateItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.state_res.DataItem> stateObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.state_res.DataItem>();

    ArrayList<String> locationItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.location_res.DataItem> locationObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.location_res.DataItem>();

    ArrayList<String> amcItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.amc_response.DataItem> amcObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.amc_response.DataItem>();

    ArrayList<Integer> contractduration_item = new ArrayList<>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.amc_response.DataItem> contractduration_object = new ArrayList<com.fieldprov2.fieldprocustomer.model.amc_response.DataItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_amc);

        txtInputCustomerName = findViewById(R.id.txtInputCustomerName);
        txtInputEmail = findViewById(R.id.txtInputEmail);
        textInputMobileNo = findViewById(R.id.textInputMobileNo);
        textInputAlternateMobileNo = findViewById(R.id.textInputAlternateMobileNo);
        textInputPlotNo = findViewById(R.id.textInputPlotNo);
        textInputStreet = findViewById(R.id.textInputStreet);
        textInputPostCode = findViewById(R.id.textInputPostCode);
        textInputLandmark = findViewById(R.id.textInputLandmark);

        txtInputModelNo = findViewById(R.id.txtInputModelNo);
        txtInputInvoiceNo = findViewById(R.id.txtInputInvoiceNo);
        txtInputQuantity = findViewById(R.id.txtInputQuantity);
        txtInputAmount = findViewById(R.id.txtInputAmount);

        modelnumberID = findViewById(R.id.modelnumberID);
        invoicenumberID = findViewById(R.id.invoicenumberID);
        serialnumberID = findViewById(R.id.serialnumberID);
        plotnumberID = findViewById(R.id.plotnumberID);
        streetID = findViewById(R.id.streetID);
        postcodeID = findViewById(R.id.postcodeID);
        landmarkID = findViewById(R.id.landmarkID);


        textInputContractDuration = findViewById(R.id.extAMCPeriod);
        txtDatePicker = findViewById(R.id.txtDatePicker);

        spnAMCId = findViewById(R.id.spnAMCType);
        spnProductId = findViewById(R.id.spnProduct);
        spnSubProductId = findViewById(R.id.spnSubProduct);
        spnCountryId = findViewById(R.id.spnCountryId);
        spnStateId = findViewById(R.id.spnStateId);
        spnCityId = findViewById(R.id.spnCityId);
        spnLocationId = findViewById(R.id.spnLocationId);

        productspinnerlay = findViewById(R.id.productspinner_lay);
        subcategoryspinnerlay = findViewById(R.id.subcategoryspinner_lay);
        amclay = findViewById(R.id.amcTypespinner_lay);
        countrylay = findViewById(R.id.countryspinner_lay);
        statelay = findViewById(R.id.statespinner_lay);
        citylay = findViewById(R.id.cityspinner_lay);
        locationlay = findViewById(R.id.locationspinner_lay);

        txtAddProductSeries = findViewById(R.id.txtAddProductSeries);
        txtSaveProductSeries = findViewById(R.id.txtSaveProductSeries);
        constProductSerialNumber = findViewById(R.id.constProductSerialNumber);
        txtInputProductSerialNo = findViewById(R.id.txtInputProductSerialNo);
        recyclerView = findViewById(R.id.recyclerView);
        btnGenerate = findViewById(R.id.btnGenerate);


        registrationController = new RegistrationController();
        apiInterface = RetrofitClient.getClient().create(ApiInterface.class);

        spnProductId.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);
        spnAMCId.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);
        spnSubProductId.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);
    //    spnLocationId.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);

        name = Pref_storage.getDetail(AddAmcActivity.this, "customer_name");
        email = Pref_storage.getDetail(AddAmcActivity.this, "email_id");
        mobile_no = Pref_storage.getDetail(AddAmcActivity.this, "contact_number");
        alt_mobile_no = Pref_storage.getDetail(AddAmcActivity.this, "alternate_number");
        plot_no = Pref_storage.getDetail(AddAmcActivity.this, "plot_number");
        street = Pref_storage.getDetail(AddAmcActivity.this, "street");
        post_code = Pref_storage.getDetail(AddAmcActivity.this, "post_code");
        cus_code = Pref_storage.getDetail(AddAmcActivity.this, "customer_code");
        landmark = Pref_storage.getDetail(AddAmcActivity.this,"landmark");
        country = Pref_storage.getDetail(AddAmcActivity.this,"country");
        state = Pref_storage.getDetail(AddAmcActivity.this,"state");
        city = Pref_storage.getDetail(AddAmcActivity.this,"city");
        location = Pref_storage.getDetail(AddAmcActivity.this,"location");
        location_position = Pref_storage.getDetail(AddAmcActivity.this,"location_position");
      //  location = Pref_storage.getDetail(AddAmcActivity.this,"location")
        Log.e("superman","--------"+location_position);

        Objects.requireNonNull(txtInputCustomerName.getEditText()).setText(name);
        Objects.requireNonNull(txtInputEmail.getEditText()).setText(email);
        Objects.requireNonNull(textInputMobileNo.getEditText()).setText(mobile_no);
        Objects.requireNonNull(textInputAlternateMobileNo.getEditText()).setText(alt_mobile_no);

        plotnumberID.setText(plot_no);
        streetID.setText(street);
        postcodeID.setText(post_code);
        landmarkID.setText(landmark);

        spnCountryId.setText(country);
        spnStateId.setText(state);
        spnCityId.setText(city);
        spnLocationId.setText(location);

        country_id = Pref_storage.getDetail(AddAmcActivity.this,"country_id");
        state_id = Pref_storage.getDetail(AddAmcActivity.this,"state_id");
        city_id = Pref_storage.getDetail(AddAmcActivity.this,"city_id");
        location_id = Pref_storage.getDetail(AddAmcActivity.this,"location_id");



        if(country_id != null)
        {
            getStateItems(Integer.parseInt(country_id));
        }
        if(state_id != null)
        {
            getCityItems(Integer.parseInt(state_id));
        }
        if(city_id != null)
        {
            getLocationItems(Integer.parseInt(city_id));

        }



       // spnLocationId.setAdapter(location);

        listSerialNumber = new ArrayList<>();

        final String[] example = {"hjdfkjdf", "dfbdfjkdf"};

        //serialNumberAdapter = new SerialNumberAdapter(listSerialNumber, this, this);
        getProductItems();
        getAMCItems();
        getCountryItems();


        spnProductId.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnProductId.getWindowToken(), 0);
                return false;
            }
        }) ;
        spnAMCId.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnAMCId.getWindowToken(), 0);
                return false;
            }
        }) ;
        spnSubProductId.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnSubProductId.getWindowToken(), 0);
                return false;
            }
        }) ;

        spnStateId.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(stateObject.size()>0)
                {
                    spinnerDialog_state.showSpinerDialog();
                    state_id = String.valueOf(stateObject.get(spnStateIdPosition+1).getStateId());
                }
                else
                {
                    Toast.makeText(AddAmcActivity.this,"Please select country first",Toast.LENGTH_SHORT).show();
                }
                InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnStateId.getWindowToken(), 0);
            }
        });

        spnCityId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(cityObject.size()>0) {
                   spinnerDialog_city.showSpinerDialog();
                   city_id = String.valueOf(cityObject.get(spnCityIdPosition+1).getCityId());
               }
               else
               {
                   Toast.makeText(AddAmcActivity.this,"Please select country and state first",Toast.LENGTH_SHORT).show();
               }
                InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnCityId.getWindowToken(), 0);
            }
        });

        spnLocationId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(locationObject.size()>0)
                {
                    spinnerDialog_location.showSpinerDialog();
                    location_id = String.valueOf(locationObject.get(spnLocationIdPosition+1).getLocationId());
                }
                else
                {
                    Toast.makeText(AddAmcActivity.this,"Please select country ,state and city first",Toast.LENGTH_SHORT).show();
                }

                InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnLocationId.getWindowToken(), 0);
            }
        });

        spnCountryId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerDialog_country.showSpinerDialog();
                InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnCountryId.getWindowToken(), 0);
                country_id = String.valueOf(countryObject.get(spnCountryIdPosition+1).getCountryId());
            }
        });


        modelnumberID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtInputModelNo.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        invoicenumberID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtInputInvoiceNo.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        serialnumberID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                for (int i = 0; i < listSerialNumber.size(); i++) {
                    if (txtInputProductSerialNo.getEditText().getText().toString().equals(listSerialNumber.get(i))) {
                        txtInputProductSerialNo.setError("serial number already added");
                    } else {
                        txtInputProductSerialNo.setError("");
                    }
                }
            }
        });

        //code for selecting the date
        txtDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                final int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                DatePickerDialog picker = new DatePickerDialog(AddAmcActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                txtDatePicker.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                date = String.valueOf(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                            }
                        }, year, month, day);
                picker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                Calendar maxDate = Calendar.getInstance();
                maxDate.set(Calendar.DAY_OF_MONTH, day + 14);
                maxDate.set(Calendar.MONTH, month);
                maxDate.set(Calendar.YEAR, year);
                picker.getDatePicker().setMaxDate(maxDate.getTimeInMillis());

                picker.show();
            }
        });

        //code for making the add serial number visible
        txtAddProductSeries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                constProductSerialNumber.setVisibility(View.VISIBLE);
            }
        });

        //code for saving the serial number and validating the conditions
        txtSaveProductSeries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(validate1())
                {
                    if(validate2()) {
                        JsonObject jsonObject = new JsonObject();

                        jsonObject.addProperty("customer_code", cus_code);
                        jsonObject.addProperty("product_id", productObject.get(spnProductIdPosition).getProduct_id());
                        jsonObject.addProperty("product_sub_id", subProductObject.get(spnProductSubIdPosition).getProductSubId());
                        jsonObject.addProperty("model_no", txtInputModelNo.getEditText().getText().toString());
                        jsonObject.addProperty("serial_no", txtInputProductSerialNo.getEditText().getText().toString());

                        postserialnumberResponse(jsonObject);
                    }
                }

            }
        });

        //code for adding the amc and validating the values
        btnGenerate.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                if (validate())
                {
                    pDialog = ProgressDialog.show(AddAmcActivity.this, "FieldPro","Loading...", true);
                    pDialog.setCancelable(true);
                    languageknown = personalInfolanguagesPojos.stream().collect(Collectors.joining(",")).concat(getString(R.string.openbrace) + "" + getString(R.string.closebrash));
                    for (int i=0; i < listSerialNumber.size(); i++ ) {

                        SerialNoPojos serialNoPojos = new SerialNoPojos(listSerialNumber.get(i));
                        serialNoPojosList.add(serialNoPojos);
                        //scriptsSerialNo = new String [] {listSerialNumber.get(i)};
                    }

                    AddProductPojo addProductPojo = new AddProductPojo();
                    addProductPojo.setCustomerCode(cus_code);
                    addProductPojo.setCustomerName(name);
                    addProductPojo.setEmailId(email);
                    addProductPojo.setContactNumber(mobile_no);
                    addProductPojo.setAlternateNumber(alt_mobile_no);
                    addProductPojo.setProductId(productObject.get(spnProductIdPosition).getProduct_id());
                    addProductPojo.setProductSubId(subProductObject.get(spnProductSubIdPosition).getProductSubId());
                    addProductPojo.setModelNo(Objects.requireNonNull(txtInputModelNo.getEditText()).getText().toString());
                    addProductPojo.setSerialArray(serialNoPojosList);
                    addProductPojo.setContractType(amcObject.get(spnAMCidPosition).getAmcId());
                    addProductPojo.setPlotNumber(Objects.requireNonNull(plotnumberID.getText()).toString());
                    addProductPojo.setStreet(Objects.requireNonNull(streetID.getText()).toString());
                    addProductPojo.setPostCode(Integer.parseInt(Objects.requireNonNull(postcodeID.getText()).toString()));
                    addProductPojo.setCountryId(Integer.parseInt(country_id));
                    addProductPojo.setStateId(Integer.parseInt(state_id));
                    addProductPojo.setCityId(Integer.parseInt(city_id));
                    addProductPojo.setLocationId(locationObject.get(spnLocationIdPosition+1).getLocationId());
                    addProductPojo.setLandmark(Objects.requireNonNull(landmarkID.getText()).toString());
                    addProductPojo.setInvoiceId(Objects.requireNonNull(txtInputInvoiceNo.getEditText()).getText().toString());
                    addProductPojo.setCustPreferenceDate(date);
                    addProductPojo.setAmmount(Integer.parseInt(Objects.requireNonNull(txtInputAmount.getEditText()).getText().toString()));
                    addProductPojo.setContractPeriod(amcObject.get(spnAMCidPosition).getDuration());
                    addProductPojo.setPriority("P1");

                //    Pref_storage.setDetail(AddAmcActivity.this,"location_position",String.valueOf(spnLocationIdPosition));
              //      Pref_storage.setDetail(AddAmcActivity.this,"location_id",String.valueOf(locationObject.get(spnLocationIdPosition).getLocationId()));

//
//                    JsonObject jsonObject = new JsonObject();
//                    JSONArray jsonArray = new JSONArray(serialNoPojosList);
//                    jsonObject.addProperty("customer_code", cus_code);
//                    jsonObject.addProperty("customer_name", name);
//                    jsonObject.addProperty("email_id", email);
//                    jsonObject.addProperty("contact_number", mobile_no);
//                    jsonObject.addProperty("alternate_number", alt_mobile_no);
//                    jsonObject.addProperty("product_id", productObject.get(spnProductIdPosition).getProduct_id());
//                    jsonObject.addProperty("product_sub_id", subProductObject.get(spnProductSubIdPosition).getProductSubId());
//                    jsonObject.addProperty("model_no", txtInputModelNo.getEditText().getText().toString());
//                    jsonObject.addProperty("serial_no", String.valueOf(jsonArray));
//                    jsonObject.addProperty("contract_type", amcObject.get(spnAMCidPosition).getAmcId());
//                    jsonObject.addProperty("contract_duration",amcObject.get(spnAMCidPosition).getDuration() );
//                    jsonObject.addProperty("plot_number", plot_no);
//                    jsonObject.addProperty("street", street);
//                    jsonObject.addProperty("post_code", post_code);
//                    jsonObject.addProperty("country_id", countryObject.get(spnCountryIdPosition).getCountryId());
//                    jsonObject.addProperty("state_id", stateObject.get(spnStateIdPosition).getStateId());
//                    jsonObject.addProperty("city_id", cityObject.get(spnCityIdPosition).getCityId());
//                    jsonObject.addProperty("location_id", locationObject.get(spnLocationIdPosition).getLocationId());
//                    jsonObject.addProperty("landmark", textInputLandmark.getEditText().getText().toString());
//                    jsonObject.addProperty("invoice_id", txtInputInvoiceNo.getEditText().getText().toString());
//                    jsonObject.addProperty("cust_preference_date", date);
//                    jsonObject.addProperty("ammount", txtInputAmount.getEditText().getText().toString());
                    callAddAMCCall(addProductPojo);
                }
            }
        });


    }


    //setting the spinner with adapter

    private void callAddAMCCall(AddProductPojo addProductPojo) {
        Token = Pref_storage.getDetail(AddAmcActivity.this, "Token");
        registrationController.postAddAMC(apiInterface, addProductPojo, this, Token);
    }

    private void postserialnumberResponse(JsonObject jsonObject) {
        product_id = productObject.get(spnProductIdPosition).getProduct_id();
        product_sub_id = subProductObject.get(spnProductSubIdPosition).getProductSubId();
        model = Objects.requireNonNull(txtInputModelNo.getEditText()).getText().toString();
        registrationController.postserialNumberResponse(apiInterface, jsonObject, this);
    }

    private void getProductItems() {
        registrationController.getProducts(apiInterface, this);
    }

    private void setProductAdapter() {
        ArrayAdapter<String> ad = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, productItem);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnProductId.setAdapter(ad);
    }


    private void getSubProductItems(int product_id) {
        registrationController.getSubProducts(apiInterface, this, product_id);
    }

    private void setSubProductAdapter() {
        ArrayAdapter<String> ad = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, subProductItem);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSubProductId.setAdapter(ad);
    }

    private void getAMCItems() {
        registrationController.getAMC(apiInterface, this);
    }

    private void setAMCAdapter() {
        ArrayAdapter<String> ad = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, amcItem);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnAMCId.setAdapter(ad);
    }

    private void getCountryItems() {
        registrationController.getCountry(apiInterface, this);
    }

    private void setCountryAdapter() {
        spinnerDialog_country = new SpinnerDialog(AddAmcActivity.this,countryItem,"select country");
        spinnerDialog_country.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String s, int position) {
                spnCountryIdPosition = position;
                {
                    stateItem.clear();
                    stateObject.clear();
                    spnStateId.setText("Select State");
                    spnStateIdPosition = 0;
                    spnCityIdPosition = 0;
                    spnLocationIdPosition = 0;
                    spnCountryId.setText(countryObject.get(spnCountryIdPosition+1).getCountryName());
                    getStateItems(countryObject.get(position+1).getCountryId());
                }
            }
        });

    }

    private void getStateItems(int countryId) {
        registrationController.getState(apiInterface, this, countryId);
    }

    private void setStateAdapter() {
        spinnerDialog_state = new SpinnerDialog(AddAmcActivity.this,stateItem,"Select State");
        spinnerDialog_state.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String s, int position) {
                spnStateIdPosition = position;
                {
                    cityItem.clear();
                    cityObject.clear();
                    spnCityId.setText("Select City");
                    spnStateId.setText(stateObject.get(spnStateIdPosition+1).getStateName());
                    getCityItems(stateObject.get(position+1).getStateId());
                }
            }
        });

    }


    private void getCityItems(int stateId) {
        registrationController.getCity(apiInterface, this, stateId);
    }

    private void setCityAdapter() {
        spinnerDialog_city = new SpinnerDialog(AddAmcActivity.this,cityItem,"Select City");
        spinnerDialog_city.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String s, int position) {
                spnCityIdPosition = position;
                {
                    locationItem.clear();
                    locationObject.clear();
                    spnLocationId.setText("Select Location");
                    spnCityId.setText(cityObject.get(spnCityIdPosition+1).getCityName());
                    getLocationItems(cityObject.get(position+1).getCityId());
                }
            }
        });

    }

    private void getLocationItems(int city_id)
    {
        registrationController.getLocation(apiInterface, this, city_id);
    }

    private void setLocationAdapter()
    {
        spinnerDialog_location = new SpinnerDialog(AddAmcActivity.this,locationItem,"Select Location");
        spinnerDialog_location.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String s, int position) {
                spnLocationIdPosition = position;

                {
                    spnLocationId.setText(locationObject.get(spnLocationIdPosition+1).getLocationName());
                }
            }
        });
    }

    //code for deleting the serial number
    @Override
    public void onDelete(int size, int position)
    {
        Log.e("status","-------"+listSerialNumber.size());
        if (listSerialNumber.size() > 0)
        {
            listSerialNumber.remove(position);
            serialNumberAdapter.notifyDataSetChanged();

            serialnumber_size = String.valueOf(listSerialNumber.size());
            Objects.<EditText>requireNonNull(txtInputQuantity.getEditText()).setText(serialnumber_size);

            b = listSerialNumber.size();

            c= String.valueOf(a*b);

            Objects.requireNonNull(txtInputAmount.getEditText()).setText(c);

        }
    }

    //code for when a item is selected in the spinner
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.spnProduct) {
            spnProductIdPosition = position;
            if (position > 0) {
                productspinnerlay.setBackgroundResource(R.drawable.bg_box_grey);
                productspinnerlay.setPadding(23, 0, 23, 0);
                subProductItem.clear();
                subProductObject.clear();
                getSubProductItems(productObject.get(position).getProduct_id());
            }
        }
        if (parent.getId() == R.id.spnSubProduct) {
            spnProductSubIdPosition = position;

            if (position > 0) {
            subcategoryspinnerlay.setBackgroundResource(R.drawable.bg_box_grey);
            subcategoryspinnerlay.setPadding(23, 0, 23, 0);
            }
        }
        if (parent.getId() == R.id.spnAMCType) {
            spnAMCidPosition = position;

            if (position > 0) {
                amclay.setBackgroundResource(R.drawable.bg_box_grey);
                amclay.setPadding(23, 0, 23, 0);
                conduration = String.valueOf(amcObject.get(spnAMCidPosition).getDuration());
                textInputContractDuration.setText(conduration+" months");
                amc_cost = String.valueOf(amcObject.get(spnAMCidPosition).getCost()) ;
                a= (int) amcObject.get(spnAMCidPosition).getCost();

                b = listSerialNumber.size();

                c= String.valueOf(a*b);

                Objects.requireNonNull(txtInputAmount.getEditText()).setText(c);
            }
        }
  /*      if (parent.getId() == R.id.spnCountryId
        ) {
            spnCountryIdPosition = position;
            if (position > 0) {
                stateItem.clear();
                stateObject.clear();
                getStateItems(countryObject.get(position).getCountryId());
            }
        }
        if (parent.getId() == R.id.spnStateId) {
            spnStateIdPosition = position;
            if (position > 0) {
                cityItem.clear();
                cityObject.clear();
                getCityItems(stateObject.get(position).getStateId());
            }
        } */


//        if (parent.getId() == R.id.spnCityId) {
//            if (position > 0) {
//                locationItem.clear();
//                locationObject.clear();
//                getLocationItems(cityObject.get(position).getCityId());
 //           }
 //       }
 //       if (parent.getId() == R.id.spnLocationId)
  //      {
 //           spnLocationIdPosition = position;
 //           if(position>0) {
 //               locationlay.setBackgroundResource(R.drawable.bg_box_grey);
//                locationlay.setPadding(23, 0, 23, 0);
 //           }
//            if (position > 0) {
//                locationItem.clear();
//                locationObject.clear();
//                getSubProductItems(productObject.get(position).getProduct_id());
//            }
  //      }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    //code to check if the serial number is already added
    private boolean validate2()
    {
        boolean valid = true;

        if(Objects.requireNonNull(txtInputProductSerialNo.getEditText()).getText().toString().equals(""))
        {
            txtInputProductSerialNo.setError("Please enter serial number");
            valid = false;
        }
        else
            {
            for (int i = 0; i < listSerialNumber.size(); i++)
            {
                if (txtInputProductSerialNo.getEditText().getText().toString().equals(listSerialNumber.get(i)))
                    txtInputProductSerialNo.setError("serial number already added");
                    valid = false;
            }
        }
        return valid;
    }

    //code to validate the addition of serial number
    private boolean validate1() {
        boolean valid = true;

        if (Objects.requireNonNull(txtInputModelNo.getEditText()).getText().toString().isEmpty()) {
            txtInputModelNo.setError("Please enter model number ");
            modelnumberID.requestFocus();
            valid = false;
        }

        if(Objects.requireNonNull(txtInputProductSerialNo.getEditText()).getText().toString().isEmpty())
        {
            txtInputProductSerialNo.setError("please enter serial number");
            serialnumberID.requestFocus();
            valid = false;
        }


        if (spnProductIdPosition == 0) {
            productspinnerlay.setBackgroundResource(R.drawable.boarder);
            productspinnerlay.setPadding(23, 0, 23, 0);
            valid = false;
        }


        if (spnProductSubIdPosition == 0)
        {
            subcategoryspinnerlay.setBackgroundResource(R.drawable.boarder);
            subcategoryspinnerlay.setPadding(23, 0, 23, 0);
            valid = false;
        }

        Log.e("thor","----------"+valid);
        return valid;

    }


    //code for the addition of amc contract
    private boolean validate() {
        boolean valid = true;


        if ((Objects.requireNonNull(txtInputModelNo.getEditText())).getText().toString().isEmpty())
        {
            txtInputModelNo.setError("Please enter model number ");
            modelnumberID.requestFocus();
            valid = false;
        }


        if (Objects.requireNonNull(txtInputInvoiceNo.getEditText()).getText().toString().isEmpty())
        {
            txtInputInvoiceNo.setError("Please enter invoice number ");
            invoicenumberID.requestFocus();
            valid = false;
        }


        if (spnProductIdPosition == 0)
        {
            productspinnerlay.setBackgroundResource(R.drawable.boarder);
            productspinnerlay.setPadding(23, 0, 23, 0);
            valid = false;
        }

        if (spnProductSubIdPosition == 0)
        {
            subcategoryspinnerlay.setBackgroundResource(R.drawable.boarder);
            subcategoryspinnerlay.setPadding(23, 0, 23, 0);
            valid = false;
        }

        if (spnAMCidPosition == 0)
        {
            amclay.setBackgroundResource(R.drawable.boarder);
            amclay.setPadding(23, 0, 23, 0);
            valid = false;
        }

        if (spnCountryId.getText().toString().equals("Select Country"))
        {
            countrylay.setBackgroundResource(R.drawable.boarder);
            countrylay.setPadding(23, 0, 23, 0);
            valid = false;
        }

        if (spnStateId.getText().toString().equals("Select State"))
        {
            statelay.setBackgroundResource(R.drawable.boarder);
            statelay.setPadding(23, 0, 23, 0);
            valid = false;
        }

        if (spnCityId.getText().toString().equals("Select City"))
        {
            citylay.setBackgroundResource(R.drawable.boarder);
            citylay.setPadding(23, 0, 23, 0);
            valid = false;
        }

        if (spnLocationId.getText().toString().equals("Select Location"))
        {
            locationlay.setBackgroundResource(R.drawable.boarder);
            locationlay.setPadding(23, 0, 23, 0);
            valid = false;
        }


        return valid;
    }

    //code for response
    @Override
    public void onSuccess(ProductResponse value)
    {
        if (value.getResponse() != null)
        {

            Log.e("test", "--->" + new Gson().toJson(value.getResponse().getData()));
            productObject.add(value.getResponse().getData().get(0));
            productItem.add("Select Product");

            for (Datum datum : value.getResponse().getData())
            {
                productObject.add(datum);
                productItem.add(datum.getProduct_name());
                Log.e("test", "--->" + new Gson().toJson(productItem));
            }
            setProductAdapter();
        }

    }

    @Override
    public void onSuccess(SubProductResponse value) {
        if (value.getResponse() != null) {

            subProductObject.add(value.getResponse().getData().get(0));
            subProductItem.add("Select Sub Product");

            for (DataItem datum : value.getResponse().getData()) {
                subProductObject.add(datum);
                subProductItem.add(datum.getProductSubName());
            }
            setSubProductAdapter();
        }

    }

    @Override
    public void onSuccess(AMCResponse value) {
        if (value.getResponse() != null) {
            amcObject.add(value.getResponse().getData().get(0));
            amcItem.add("Select AMC Type");
            contractduration_object.add(value.getResponse().getData().get(0));

            for (com.fieldprov2.fieldprocustomer.model.amc_response.DataItem datum : value.getResponse().getData()) {
                amcObject.add(datum);
                amcItem.add(datum.getAmcType());
                contractduration_item.add(datum.getDuration());
                Log.e("master", "-------" + contractduration_item.add(datum.getDuration()));

            }

            setAMCAdapter();
        }

    }

    @Override
    public void onSuccess(CountryResponse value) {
        if (value.getResponse() != null) {

            countryObject.add(value.getResponse().getData().get(0));
    //        countryItem.add("Select Country");

            for (com.fieldprov2.fieldprocustomer.model.country_response.DataItem datum : value.getResponse().getData()) {
                countryObject.add(datum);
                countryItem.add(datum.getCountryName());
            }
            setCountryAdapter();
        }

    }

    @Override
    public void onSuccess(StateResponse value) {
        if (value.getResponse() != null) {

            stateObject.add(value.getResponse().getData().get(0));
   //         stateItem.add("Select State");

            for (com.fieldprov2.fieldprocustomer.model.state_res.DataItem datum : value.getResponse().getData()) {
                stateObject.add(datum);
                stateItem.add(datum.getStateName());
            }
            setStateAdapter();
        }

    }

    @Override
    public void onSuccess(CityResponse value) {
        if (value.getResponse() != null && value.getResponse().getData().size() > 0) {

            cityObject.add(value.getResponse().getData().get(0));
   //         cityItem.add("Select City");

            for (com.fieldprov2.fieldprocustomer.model.city_res.DataItem datum : value.getResponse().getData()) {
                cityObject.add(datum);
                cityItem.add(datum.getCityName());
            }
            setCityAdapter();
        }
        else {
            cityItem.add("Select City");
            Toast.makeText(AddAmcActivity.this, "this state does not have a city", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onSuccess(LocationResponse value) {
        if (value.getResponse() != null && value.getResponse().getData().size() > 0) {

            locationObject.add(value.getResponse().getData().get(0));
     //       locationItem.add("Select Location ");

            for (com.fieldprov2.fieldprocustomer.model.location_res.DataItem datum : value.getResponse().getData()) {
                locationObject.add(datum);
                locationItem.add(datum.getLocationName());
            }
            setLocationAdapter();
        }
        else {
            locationItem.add("Select location");
            Toast.makeText(AddAmcActivity.this, "This city does not have a location", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onSuccess(AddAMCResponse value)
    {
        Toast.makeText(this, value.getResponse().getMessage(), Toast.LENGTH_SHORT).show();
        amc_token = value.getResponse().getToken();
        Pref_storage.setDetail(AddAmcActivity.this,"Token",amc_token);
        finish();
        pDialog.dismiss();
        Intent intent = new Intent(AddAmcActivity.this, MainActivity.class);
        intent.putExtra("thor","america");
        startActivity(intent);
    }

    @Override
    public void onSuccess(SerialNumberResponse value) {
        //if (!Objects.requireNonNull(txtInputProductSerialNo.getEditText()).getText().toString().equals("")) {
        Log.e("ham", "------");

        listSerialNumber.add(Objects.requireNonNull(txtInputProductSerialNo.getEditText()).getText().toString());
        serialnumber_size = String.valueOf(listSerialNumber.size());
        //to get the size of the array for amount calculation
        b = listSerialNumber.size();
        personalInfolanguagesPojos.add(txtInputProductSerialNo.getEditText().getText().toString());
        //setting the recycler view
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        serialNumberAdapter = new SerialNumberAdapter(listSerialNumber, this, this);
        recyclerView.setAdapter(serialNumberAdapter);

        Objects.requireNonNull(txtInputQuantity.getEditText()).setText(serialnumber_size);

        //amount calculation
        c= String.valueOf(a*b);

        Objects.requireNonNull(txtInputAmount.getEditText()).setText(c);

        txtInputProductSerialNo.getEditText().setText("");
        Log.e("ham", "scriptsSerialNo ------> " + Arrays.toString(scriptsSerialNo));
    }

    @Override
    public void onError(Throwable throwable)
    {
        pDialog.dismiss();
    }
}