package com.fieldprov2.fieldprocustomer.view.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fieldprov2.fieldprocustomer.R;
import com.fieldprov2.fieldprocustomer.controller.ChangePasswordController;
import com.fieldprov2.fieldprocustomer.model.change_password.ChangePasswordResponse;
import com.fieldprov2.fieldprocustomer.model.registration_res.RegistrationResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;
import com.fieldprov2.fieldprocustomer.network.RetrofitClient;
import com.fieldprov2.fieldprocustomer.view.MainActivity;
import com.fieldprov2.fieldprocustomer.view.Pref_storage;
import com.fieldprov2.fieldprocustomer.view.login.LoginActivity;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;

public class ChangePasswordActivity extends AppCompatActivity implements ChangePasswordController.ChangePasswordResCallbacks {
    ApiInterface apiInterface;

    Button updatePassword;
    TextInputLayout oldPassword,newPassword,confirmPassword;
    ChangePasswordController changePasswordController;
    private String email_id;
    private String password;
    ProgressDialog pDialog ;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        apiInterface = RetrofitClient.getClient().create(ApiInterface.class);

        updatePassword = findViewById(R.id.buttonchangepassword);
        oldPassword = findViewById(R.id.editTextTextPersonName7);
        newPassword = findViewById(R.id.editTextTextPersonName9);
        confirmPassword = findViewById(R.id.editTextTextPersonName10);

        changePasswordController = new ChangePasswordController();

        email_id = Pref_storage.getDetail(ChangePasswordActivity.this,"username1");
        password =  Pref_storage.getDetail(ChangePasswordActivity.this,"password1");

        updatePassword.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(validate())
                {
                    pDialog = ProgressDialog.show(ChangePasswordActivity.this, "FieldPro","Loading...", true);
                    pDialog.setCancelable(true);
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("email",email_id);
                    jsonObject.addProperty("old_password",oldPassword.getEditText().getText().toString());
                    jsonObject.addProperty("new_password",newPassword.getEditText().getText().toString());
                    jsonObject.addProperty("confirm_password",confirmPassword.getEditText().getText().toString());
                    callRegistrationCall(jsonObject);
                }
            }
        });

    }

    private void callRegistrationCall(JsonObject jsonObject)
    {
        Log.e("hello","------------"+jsonObject);
        changePasswordController.postChangePassword(apiInterface, jsonObject, this);
    }


    private boolean validate()
    {
        boolean valid = true;

        if(oldPassword.getEditText().getText().toString().equals(""))
        {
            oldPassword.setError("Please enter oldpassword ");
            oldPassword.getEditText().setText(null);
            valid = false;
        }
        else
        {
           if(!oldPassword.getEditText().getText().toString().equals(password))
           {
               oldPassword.setError("The entered password is not the right password");
               oldPassword.getEditText().setText(null);
               valid = false;
           }
           if(oldPassword.getEditText().getText().toString().equals(password)) {
               oldPassword.setError(null);
               valid = true;
           }
        }


        if(newPassword.getEditText().getText().toString().equals(""))
        {
            newPassword.setError("Please enter new password");
            newPassword.getEditText().setText(null);
            valid = false;
        }


        if(confirmPassword.getEditText().getText().toString().equals(""))
        {
            confirmPassword.setError("Please enter confirmpassword");
            confirmPassword.getEditText().setText(null);
            valid = false;
        }
        else
        {
            if(!newPassword.getEditText().getText().toString().equals(confirmPassword.getEditText().getText().toString()))
            {
                confirmPassword.setError("New password and confirm password does not match");
                confirmPassword.getEditText().setText(null);
                newPassword.setError(null);
                valid = false ;
            }
            else {
                confirmPassword.setError(null);
                newPassword.setError(null);
                valid = true;
            }
        }

        return valid;

    }

    @Override
    public void onSuccess(ChangePasswordResponse value)
    {
        if (value.getResponse() != null && value.getResponse().getResponseCode().equals("200")) {
            Toast.makeText(getApplicationContext(), "Password changed successfully", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
            startActivity(intent);
            pDialog.dismiss();
        }
        else
        {
            pDialog.dismiss();
        }
    }

    @Override
    public void onError(Throwable throwable) {
        pDialog.dismiss();

    }
}