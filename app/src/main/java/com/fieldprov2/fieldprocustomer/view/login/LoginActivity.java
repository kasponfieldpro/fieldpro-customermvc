package com.fieldprov2.fieldprocustomer.view.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.fieldprov2.fieldprocustomer.R;
import com.fieldprov2.fieldprocustomer.model.login_response.LoginResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;
import com.fieldprov2.fieldprocustomer.network.RetrofitClient;
import com.fieldprov2.fieldprocustomer.view.MainActivity;
import com.fieldprov2.fieldprocustomer.view.Pref_storage;
import com.fieldprov2.fieldprocustomer.view.registration.RegistrationActivity;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    //initialize the values
    ApiInterface apiInterface;

    Button btnLogin;
    TextView txtNewUser, txtResetPassword;
    TextInputEditText emailID,passwordtxt;
    TextInputLayout txtInputEmail, txtInputPassword;
    private String username = null;
    private String username11 = null;
    private String password11 = null;
    public String cus_code;
    public String Token, name, email, mobile_no, alt_mobile_no, plot_no, street, landmark;
    public int postcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //creating objects and assiging the respective values in the xml
        apiInterface = RetrofitClient.getClient().create(ApiInterface.class);

        btnLogin = findViewById(R.id.btnLogin);
        txtNewUser = findViewById(R.id.txtNewUser);
        txtResetPassword = findViewById(R.id.txtResetPassword);
        txtInputEmail = findViewById(R.id.txtInputEmail);
        emailID = findViewById(R.id.emailId);
        passwordtxt = findViewById(R.id.passwordtxt);
        txtInputPassword = findViewById(R.id.txtInputPassword);


        //fetching the data from shared preference to check for the credentials
        username11 = Pref_storage.getDetail(LoginActivity.this, "username1");
        Log.e("name", "----" + username11);
        password11 = Pref_storage.getDetail(LoginActivity.this, "password1");

        emailID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!Patterns.EMAIL_ADDRESS.matcher(Objects.requireNonNull(emailID.getText()).toString()).matches()) {
                    txtInputEmail.setError("Please enter valid email address ");
                } else {
                    txtInputEmail.setError(null);
                }
            }
        });

        passwordtxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtInputPassword.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

            if(username11 == null)
            {


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creating loading graphics
                final ProgressDialog pDialog = new ProgressDialog(LoginActivity.this);
                pDialog.setMessage("Loading...");

                if (validate()) {

                    pDialog.show();
                    //assigning the values to the json object
                    JsonObject object = new JsonObject();
                    object.addProperty("username", Objects.requireNonNull(txtInputEmail.getEditText()).getText().toString());
                    object.addProperty("password", Objects.requireNonNull(txtInputPassword.getEditText()).getText().toString());

                    apiInterface.login(object).enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            if (response.body() != null && response.body().getResponse().getResponseCode().equals("200")) {
                                pDialog.cancel();

                                Log.e("hello", "-----" + response.body().getResponse().getResponseCode());
                                Log.e("hello", "-----" + response.body().getResponse().getMessage());
                                Log.e("hello", "-----" + response.body().getResponse().getToken());

                                //getting the credentials of the user that is sent through the login response and storing it in the shared preference
                                Toast.makeText(LoginActivity.this, response.body().getResponse().getMessage(), Toast.LENGTH_SHORT).show();
                                cus_code = (response.body().getResponse().getCustomerDetails().getCustomerCode());
                                Token = (response.body().getResponse().getToken());
                                name = (response.body().getResponse().getCustomerDetails().getCustomerName());
                                email = (response.body().getResponse().getCustomerDetails().getEmailId());
                                mobile_no = (response.body().getResponse().getCustomerDetails().getContactNumber());
                                alt_mobile_no = (response.body().getResponse().getCustomerDetails().getAlternateNumber());
                                plot_no = (response.body().getResponse().getCustomerDetails().getPlotNumber());
                                street = (response.body().getResponse().getCustomerDetails().getStreet());
                                postcode = (response.body().getResponse().getCustomerDetails().getPostCode());
                                landmark = (response.body().getResponse().getCustomerDetails().getLandmark());
                                String countryName = (response.body().getResponse().getCustomerDetails().getCountryName());
                                String stateName = (response.body().getResponse().getCustomerDetails().getStateName());
                                String cityName = (response.body().getResponse().getCustomerDetails().getCityName());
                                String locationName = (response.body().getResponse().getCustomerDetails().getLocationName());
                                int country_id = (response.body().getResponse().getCustomerDetails().getCountryId());
                                int state_id =  (response.body().getResponse().getCustomerDetails().getStateId());
                                int city_id = (response.body().getResponse().getCustomerDetails().getCityId());
                                int location_id =  (response.body().getResponse().getCustomerDetails().getLocationId());

                                Pref_storage.setDetail(LoginActivity.this, "username1", (txtInputEmail.getEditText()).getText().toString());
                                Pref_storage.setDetail(LoginActivity.this, "password1", (txtInputPassword.getEditText()).getText().toString());
                                Pref_storage.setDetail(LoginActivity.this, "customer_code", cus_code);
                                Pref_storage.setDetail(LoginActivity.this, "Token", Token);

                                Pref_storage.setDetail(LoginActivity.this, "customer_name", name);
                                Pref_storage.setDetail(LoginActivity.this, "email_id", email);
                                Pref_storage.setDetail(LoginActivity.this, "contact_number", mobile_no);
                                Pref_storage.setDetail(LoginActivity.this, "alternate_number", alt_mobile_no);
                                Pref_storage.setDetail(LoginActivity.this, "plot_number", plot_no);
                                Pref_storage.setDetail(LoginActivity.this, "street", street);
                                Pref_storage.setDetail(LoginActivity.this, "post_code", String.valueOf(postcode));
                                Pref_storage.setDetail(LoginActivity.this, "landmark", landmark);
                                Pref_storage.setDetail(LoginActivity.this, "country", countryName);
                                Pref_storage.setDetail(LoginActivity.this, "state", stateName);
                                Pref_storage.setDetail(LoginActivity.this, "city", cityName);
                                Pref_storage.setDetail(LoginActivity.this, "location", locationName);
                                Pref_storage.setDetail(LoginActivity.this,"country_id",String.valueOf(country_id));
                                Pref_storage.setDetail(LoginActivity.this,"state_id",String.valueOf(state_id));
                                Pref_storage.setDetail(LoginActivity.this,"city_id",String.valueOf(city_id));
                                Pref_storage.setDetail(LoginActivity.this,"location_id",String.valueOf(location_id));
                                Log.e("matt","location_id"+location_id);

                                //creating intent to move to the next page
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);


                            } else {
                                //creating toast message and closing the loading graphics
                                Toast.makeText(LoginActivity.this, "Invalid credentials", Toast.LENGTH_SHORT).show();
                                pDialog.dismiss();
                            }
                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            //creating toast message and closing the loading graphics
                            Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            pDialog.dismiss();
                        }
                    });

                }
            }
        });
            }
                  else
               {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                   startActivity(intent);
           }


        txtNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creating intent to move to registration page
                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(intent);
            }
        });
        txtResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creating intent to move to forgot password page
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    //creating conditions to check if the entries are empty or not
    private boolean validate() {

        boolean valid = true;
        if (Objects.requireNonNull(emailID).getText().toString().isEmpty()) {

            txtInputEmail.setError("Please enter email address ");
            valid = false;

        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailID.getText().toString()).matches()) {
            txtInputEmail.setError("Please enter valid email address ");
            valid = false;

        }
        if (Objects.requireNonNull(passwordtxt).getText().toString().isEmpty()) {
            txtInputPassword.setError("Please enter password");
            valid = false;
        }
        return valid;
    }
}