package com.fieldprov2.fieldprocustomer.view.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.fieldprov2.fieldprocustomer.R;
import com.fieldprov2.fieldprocustomer.controller.ForgotPasswordController;
import com.fieldprov2.fieldprocustomer.model.change_password.ChangePasswordResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;
import com.fieldprov2.fieldprocustomer.network.RetrofitClient;
import com.fieldprov2.fieldprocustomer.view.Pref_storage;
import com.fieldprov2.fieldprocustomer.view.profile.ChangePasswordActivity;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;

import java.util.Objects;

public class ForgotPasswordActivity extends AppCompatActivity implements ForgotPasswordController.ForgotPasswordResCallbacks {

    //initializing the variables
    ApiInterface apiInterface;

    ForgotPasswordController forgotPasswordController;

    TextInputLayout txtInputEmail;
    Button btnResetPassword;
    TextView txtBackToLogin;
    private String email_id;
    ProgressDialog pDialog ;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        //creating objects for the created variables
        forgotPasswordController = new ForgotPasswordController();


        apiInterface = RetrofitClient.getClient().create(ApiInterface.class);

        txtInputEmail = findViewById(R.id.txtInputEmail);
        btnResetPassword = findViewById(R.id.btnResetPassword);
        txtBackToLogin = findViewById(R.id.txtBackToLogin);

        //getting the email from shared preference for verification
        email_id = Pref_storage.getDetail(ForgotPasswordActivity.this,"username1");


        btnResetPassword.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v) {
                if (validate())
                {

                    //creating loading graphics
                    //assigning the values to the json object
                    pDialog = ProgressDialog.show(ForgotPasswordActivity.this, "FieldPro","Loading...", true);
                    pDialog.setCancelable(true);
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("email",txtInputEmail.getEditText().getText().toString());
                    //calling the api function
                    callForgotPasswordCall((jsonObject));
                }
            }
        });

        txtBackToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    private void callForgotPasswordCall(JsonObject jsonObject)
    {
        Log.e("hello","------------"+jsonObject);
        forgotPasswordController.postForgotPassword(apiInterface, jsonObject, this);

    }

   //creating conditions for validating the entries
    private Boolean validate() {
        boolean valid = true;

        if (txtInputEmail.getEditText().getText().toString().equals(""))
        {
            txtInputEmail.setError("Please enter email address ");
            valid = false;
        }
        else
        {
            //checking if the email is of the right format
            if (!Patterns.EMAIL_ADDRESS.matcher(txtInputEmail.getEditText().getText().toString()).matches())
            {
                txtInputEmail.setError("Please enter valid email address ");
                valid = false;
            }

            else
            {
                txtInputEmail.setError(null);
                    valid = true;
            }
        }

        return valid;
    }

    @Override
    public void onSuccess(ChangePasswordResponse value)
    {

        if (value.getResponse() != null)
    {
        //checking if the response from the backend is 200 to proceed with the process
        if(value.getResponse().getResponseCode().equals("200")) {
            Toast.makeText(ForgotPasswordActivity.this, "Password changed successfully Please check your mail", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
            startActivity(intent);
            pDialog.dismiss();
        }
        //if the response is 500 display error message
        else if(value.getResponse().getResponseCode().equals("500"))
        {
            Toast.makeText(ForgotPasswordActivity.this, "This email is not registered", Toast.LENGTH_SHORT).show();
            pDialog.dismiss();
        }
    }
    }

    @Override
    public void onError(Throwable throwable)
    {
        pDialog.dismiss();

    }
}

