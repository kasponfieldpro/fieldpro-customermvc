package com.fieldprov2.fieldprocustomer.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.fieldprov2.fieldprocustomer.controller.LogoutController;
import com.fieldprov2.fieldprocustomer.model.logout.LogoutResponse;
import com.fieldprov2.fieldprocustomer.model.profile_activity.ProfileActivityResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;
import com.fieldprov2.fieldprocustomer.network.RetrofitClient;
import com.fieldprov2.fieldprocustomer.view.login.LoginActivity;
import com.fieldprov2.fieldprocustomer.view.profile.ProfileActivity;

public class RaiseTicketDialogBox extends AppCompatDialogFragment {

    private Activity activity;

    public RaiseTicketDialogBox(Activity activity) {
        this.activity = activity;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Ticket status")
                .setMessage("Ticket raised sucessfully")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        Intent intent = new Intent(activity, MainActivity.class);
                        intent.putExtra("thor","loki");
                        activity.startActivity(intent);
                    }
                });
        return builder.create();
    }



}
