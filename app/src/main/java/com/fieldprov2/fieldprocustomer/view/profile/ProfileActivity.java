package com.fieldprov2.fieldprocustomer.view.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.fieldprov2.fieldprocustomer.R;
import com.fieldprov2.fieldprocustomer.controller.ProfileActivityController;
import com.fieldprov2.fieldprocustomer.model.EditCustomer.ProfileDetailsResponse;
import com.fieldprov2.fieldprocustomer.model.change_password.ChangePasswordResponse;
import com.fieldprov2.fieldprocustomer.model.customer_product.Example;
import com.fieldprov2.fieldprocustomer.model.login_response.LoginResponse;
import com.fieldprov2.fieldprocustomer.model.profile_activity.ProfileActivityResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;
import com.fieldprov2.fieldprocustomer.network.RetrofitClient;
import com.fieldprov2.fieldprocustomer.view.LogoutDialogBox;
import com.fieldprov2.fieldprocustomer.view.Pref_storage;
import com.fieldprov2.fieldprocustomer.view.login.ForgotPasswordActivity;
import com.fieldprov2.fieldprocustomer.view.login.LoginActivity;
import com.google.gson.JsonObject;

import java.util.zip.Inflater;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity implements ProfileActivityController.ProfileActivityResCallbacks,ProfileActivityController.ProfileDetailsResCallbacks {

    //initializing the variables
    Button btnChangePassword,savechange;
    private String cus_code, Token, full_name, mail_id, mobile_no, profileactivity_token,country,state,city,location;
    ProfileActivityController profileActivityController;
    ApiInterface apiInterface;
    EditText editTextpersonname1, editTextpersonname2, editTextpersonname3, editTextpersonname4;
    ProgressDialog pDialog ;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        pDialog = ProgressDialog.show(ProfileActivity.this, "FieldPro","Loading...", true);
        pDialog.setCancelable(true);

        //getting the values from the shared preference for displaying
        country = Pref_storage.getDetail(ProfileActivity.this,"country");
        state = Pref_storage.getDetail(ProfileActivity.this,"state");
        city = Pref_storage.getDetail(ProfileActivity.this,"city");
        location = Pref_storage.getDetail(ProfileActivity.this,"location");

        editTextpersonname1 = findViewById(R.id.editTextTextPersonName);
        editTextpersonname2 = findViewById(R.id.editTextTextPersonName2);
        editTextpersonname3 = findViewById(R.id.editTextTextPersonName3);
        editTextpersonname4 = findViewById(R.id.edittectlocation);

        apiInterface = RetrofitClient.getClient().create(ApiInterface.class);
        profileActivityController = new ProfileActivityController();
        btnChangePassword = findViewById(R.id.btnChangePassword);
        savechange = findViewById(R.id.save_change);

        //making the displayed values uneditable
        editTextpersonname1.setEnabled(false);
        editTextpersonname2.setEnabled(false);
        editTextpersonname3.setEnabled(false);
        editTextpersonname4.setEnabled(false);


        //getting customer code from the shared preference
        cus_code = Pref_storage.getDetail(ProfileActivity.this, "customer_code");

        getProfileActivity();
        JsonObject object = new JsonObject();

        //making the values editable

            savechange.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (savechange.getText().equals("Edit Profile")) {
                        editTextpersonname1.setEnabled(true);
                        editTextpersonname2.setEnabled(true);
                        editTextpersonname3.setEnabled(true);
                        editTextpersonname4.setEnabled(true);
                        savechange.setText("Update Profile");
                    } else if (savechange.getText().equals("Update Profile"))
                    {
                        JsonObject jsonObject = new JsonObject();

                        jsonObject.addProperty("customer_code", cus_code);
                        jsonObject.addProperty("customer_name", editTextpersonname1.getText().toString());
                        jsonObject.addProperty("email_id", editTextpersonname2.getText().toString());
                        jsonObject.addProperty("contact_number", editTextpersonname3.getText().toString());
                        jsonObject.addProperty("alternate_number", editTextpersonname4.getText().toString());

                        setProfileDetails(jsonObject);

                        editTextpersonname1.setEnabled(false);
                        editTextpersonname2.setEnabled(false);
                        editTextpersonname3.setEnabled(false);
                        editTextpersonname4.setEnabled(false);
                        savechange.setText("Edit Profile");
                    }
                }  });


        //going to change password page
        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, ChangePasswordActivity.class);
                startActivity(intent);
            }
        });
    }

    public void onResponse(Call<ProfileActivityResponse> call, Response<ProfileActivityResponse> response) {

    }

    private void getProfileActivity() {
        Token = Pref_storage.getDetail(ProfileActivity.this, "Token");
        profileActivityController.getProfileActivity(apiInterface, this, Token, cus_code);
    }

    private void setProfileDetails(JsonObject jsonObject)
    {
        Token = Pref_storage.getDetail(ProfileActivity.this,"Token");
        profileActivityController.setProfileDetails(apiInterface,this,Token,jsonObject);
    }


    @Override
    public void onSuccess(ProfileActivityResponse value)
    {
        if (value.getResponse() != null && value.getResponse().getResponseCode().equals("200")) {
            profileactivity_token = value.getResponse().getToken();
            Pref_storage.setDetail(ProfileActivity.this, "Token", profileactivity_token);
            full_name = value.getResponse().getData().getCustomerName();
            mail_id = value.getResponse().getData().getEmailId();
            mobile_no = value.getResponse().getData().getContactNumber();

            //getting the values from the api and setting it in the respective values
            editTextpersonname1.setText(full_name);
            editTextpersonname2.setText(mail_id);
            editTextpersonname3.setText(mobile_no);
            editTextpersonname4.setText(location+","+city+","+state+","+country);
            pDialog.dismiss();
        }
        else
        {
            pDialog.dismiss();
        }

    }

public boolean onCreateOptionsMenu(Menu menu)
{
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.logout_menu,menu);
    return true;
}

//moving to logout menu from this
public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
        case R.id.
                logoutMenu: {
            opendialog();
        }
    }
    return super.onOptionsItemSelected(item);

}


public void opendialog()
    {
        LogoutDialogBox logoutDialogBox = new LogoutDialogBox(ProfileActivity.this);
        logoutDialogBox.show(getSupportFragmentManager(),"logout_dialog");
    }


    @Override
    public void onSuccess(ProfileDetailsResponse value)
    {
        if (value.getResponse() != null && value.getResponse().getResponseCode().equals("200"))
        {
            profileactivity_token = value.getResponse().getToken();
            Pref_storage.setDetail(ProfileActivity.this,"Token",profileactivity_token);
            Pref_storage.setDetail(ProfileActivity.this,"customer_name",editTextpersonname1.getText().toString());
            Pref_storage.setDetail(ProfileActivity.this,"email_id",editTextpersonname2.getText().toString());
            Pref_storage.setDetail(ProfileActivity.this,"contact_number",editTextpersonname3.getText().toString());
        }

    }

    @Override
    public void onError(Throwable throwable)
    {
        pDialog.dismiss();
    }
}