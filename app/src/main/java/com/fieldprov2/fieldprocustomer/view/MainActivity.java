package com.fieldprov2.fieldprocustomer.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.fieldprov2.fieldprocustomer.ExitDialogBox;
import com.fieldprov2.fieldprocustomer.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {


    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationView = findViewById(R.id.botNavView);
       String odin = getIntent().getStringExtra("thor");
       //for fragment transaction from raise ticket to ticket list

       if(odin != null) {
           if (odin.equals("loki")) {
               getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, TicketListFragment.newInstance(), "Raise").commit();
               bottomNavigationView.setSelectedItemId(R.id.ticketList);
           } else if (odin.equals("man")) {
               getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, MyProductFragment.newInstance(), "Product").commit();
               bottomNavigationView.setSelectedItemId(R.id.myProduct);
           } else if (odin.equals("america")) {
               getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, AmcFragment.newInstance(), "Amc").commit();
               bottomNavigationView.setSelectedItemId(R.id.amc);
           }
       } else {
           getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, HomeFragment.newInstance(), "Home").commitNowAllowingStateLoss();
       }

        //creating case id for fragment transaction
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId())
                {
                    case R.id.home: {
                        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, HomeFragment.newInstance(), "Home").commit();
                        break;
                    }

                    case R.id.raiseTicket: {
                        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, RaiseTicketFragment.newInstance(), "Raise").commit();
                        break;
                    }

                    case R.id.ticketList: {
                        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, TicketListFragment.newInstance(), "Ticket").commit();
                        break;
                    }

                    case R.id.myProduct: {
                        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, MyProductFragment.newInstance(), "Product").commit();
                        break;
                    }

                    case R.id.amc:
                        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, AmcFragment.newInstance(), "Amc").commit();
                        break;


                }

                return true;
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frameLayout);
        Objects.requireNonNull(fragment).onActivityResult(requestCode,resultCode,data);
    }

    public void opendialogbox()
    {
        ExitDialogBox exitDialogBox = new ExitDialogBox(MainActivity.this);
        exitDialogBox.show(getSupportFragmentManager(),"Do_you_want_to_exit");
    }

    @Override
    public void onBackPressed()
    {
        //super.onBackPressed();
        opendialogbox();
    }
}