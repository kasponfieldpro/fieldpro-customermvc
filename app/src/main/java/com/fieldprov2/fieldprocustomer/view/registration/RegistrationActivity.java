package com.fieldprov2.fieldprocustomer.view.registration;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.fieldprov2.fieldprocustomer.R;
import com.fieldprov2.fieldprocustomer.controller.RegistrationController;
import com.fieldprov2.fieldprocustomer.model.amc_response.AMCResponse;
import com.fieldprov2.fieldprocustomer.model.city_res.CityResponse;
import com.fieldprov2.fieldprocustomer.model.country_response.CountryResponse;
import com.fieldprov2.fieldprocustomer.model.location_res.LocationResponse;
import com.fieldprov2.fieldprocustomer.model.product_root.Datum;
import com.fieldprov2.fieldprocustomer.model.product_root.ProductResponse;
import com.fieldprov2.fieldprocustomer.model.registration_res.RegistrationResponse;
import com.fieldprov2.fieldprocustomer.model.state_res.StateResponse;
import com.fieldprov2.fieldprocustomer.model.sub_product_root.DataItem;
import com.fieldprov2.fieldprocustomer.model.sub_product_root.SubProductResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;
import com.fieldprov2.fieldprocustomer.network.RetrofitClient;
import com.fieldprov2.fieldprocustomer.view.AddProductActivity;
import com.fieldprov2.fieldprocustomer.view.Pref_storage;
import com.fieldprov2.fieldprocustomer.view.login.LoginActivity;
import com.fieldprov2.fieldprocustomer.view.profile.ChangePasswordActivity;
import com.fieldprov2.fieldprocustomer.view.profile.ProfileActivity;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Objects;
import java.util.regex.Pattern;

import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;

public class RegistrationActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener,
        RegistrationController.ProductResCallbacks, RegistrationController.SubProductResCallbacks,
        RegistrationController.AMCResCallbacks, RegistrationController.CountryResCallbacks, RegistrationController.StateResCallbacks, RegistrationController.CityResCallbacks,
        RegistrationController.LocationResCallbacks, RegistrationController.RegistrationResCallbacks {

    //initialization
    ApiInterface apiInterface;

    TextInputLayout txtInputCustomerName, txtInputEmail, textInputMobileNo, textInputAlternateMobileNo, textInputModelNo, textInputSerialNo, textInputPriority, textInputPlotNo, textInputStreet, textInputPostCode,
            textInputLandmark;
    TextView textInputContractDuration;

    TextInputEditText customernameID, emailID, mobilenumberID, alternatenumberID, modelnumberID, serialnumberID, priorityID, plotnumberID, streetID, postcodeID, landmarkID;

    Button btnRegister, spnCountryId, spnStateId, spnCityId, spnLocationId;
    SpinnerDialog spinnerDialog_country, spinnerDialog_state, spinnerDialog_city, spinnerDialog_location;
    Spinner spnProductId, spnProductSubId, spnAMCid;
    RegistrationController registrationController;
    String registration_token, conduration, country, state, city, location;
    ProgressDialog pDialog;
    private LinearLayout productspinnerlay, subcategoryspinnerlay, amclay, countrylay, statelay, citylay, locationlay;

    //spinner's position initialization
    int spnProductIdPosition = 0, spnProductSubIdPosition = 0,
            spnAMCidPosition = 0, spnCountryIdPosition = 0, spnStateIdPosition = 0,
            spnCityIdPosition = 0, spnLocationIdPosition = 0;
    //spinner's variable initialization for getting values from the api
    ArrayList<String> productItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.product_root.Datum> productObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.product_root.Datum>();

    ArrayList<String> subProductItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.sub_product_root.DataItem> subProductObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.sub_product_root.DataItem>();

    ArrayList<String> cityItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.city_res.DataItem> cityObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.city_res.DataItem>();

    ArrayList<String> countryItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.country_response.DataItem> countryObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.country_response.DataItem>();

    ArrayList<String> stateItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.state_res.DataItem> stateObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.state_res.DataItem>();

    ArrayList<String> locationItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.location_res.DataItem> locationObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.location_res.DataItem>();

    ArrayList<String> amcItem = new ArrayList<String>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.amc_response.DataItem> amcObject = new ArrayList<com.fieldprov2.fieldprocustomer.model.amc_response.DataItem>();

    ArrayList<Integer> contractduration_item = new ArrayList<>();
    ArrayList<com.fieldprov2.fieldprocustomer.model.amc_response.DataItem> contractduration_object = new ArrayList<com.fieldprov2.fieldprocustomer.model.amc_response.DataItem>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Assigning values in the xml
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        txtInputCustomerName = findViewById(R.id.txtInputCustomerName);
        txtInputEmail = findViewById(R.id.txtInputEmail);
        textInputMobileNo = findViewById(R.id.textInputMobileNo);
        textInputAlternateMobileNo = findViewById(R.id.textInputAlternateMobileNo);
        textInputModelNo = findViewById(R.id.textInputModelNo);
        textInputSerialNo = findViewById(R.id.textInputSerialNo);
        spnAMCid = findViewById(R.id.spnAMCId);
        textInputContractDuration = findViewById(R.id.textInputContractDuration);
        textInputPriority = findViewById(R.id.textInputPriority);
        textInputPlotNo = findViewById(R.id.textInputPlotNo);
        textInputStreet = findViewById(R.id.textInputStreet);
        textInputPostCode = findViewById(R.id.textInputPostCode);
        spnCountryId = findViewById(R.id.spnCountryId);
        spnStateId = findViewById(R.id.spnStateId);
        spnCityId = findViewById(R.id.spnCityId);
        spnLocationId = findViewById(R.id.spnLocationId);
        textInputLandmark = findViewById(R.id.textInputLandmark);

        productspinnerlay = findViewById(R.id.productspinner_lay);
        subcategoryspinnerlay = findViewById(R.id.subcategoryspinner_lay);
        amclay = findViewById(R.id.amcTypespinner_lay);
        countrylay = findViewById(R.id.countryspinner_lay);
        statelay = findViewById(R.id.statespinner_lay);
        citylay = findViewById(R.id.cityspinner_lay);
        locationlay = findViewById(R.id.locationspinner_lay);

        spnProductId = findViewById(R.id.spnProductId);
        spnProductSubId = findViewById(R.id.spnProductSubId);

        customernameID = findViewById(R.id.customernameID);
        emailID = findViewById(R.id.emailID);
        mobilenumberID = findViewById(R.id.mobilenumberID);
        alternatenumberID = findViewById(R.id.alternatenumberID);
        modelnumberID = findViewById(R.id.modelnumberID);
        serialnumberID = findViewById(R.id.serialnumberID);
        plotnumberID = findViewById(R.id.plotnumberID);
        streetID = findViewById(R.id.streetID);
        postcodeID = findViewById(R.id.postcodeID);
        landmarkID = findViewById(R.id.landmarkID);

        //spinner's on item selected listeners
        spnProductId.setOnItemSelectedListener(this);
        spnAMCid.setOnItemSelectedListener(this);
        spnProductSubId.setOnItemSelectedListener(this);
        //    spnLocationId.setOnItemSelectedListener(this);

        btnRegister = findViewById(R.id.btnRegister);

        //creating object for controller class and api
        registrationController = new RegistrationController();
        apiInterface = RetrofitClient.getClient().create(ApiInterface.class);


        //calling api's for product, amc and country list
        //get productList
        getProductItems();
        getAMCItems();
        getCountryItems();


        //creating on touch activity to close the keypad when opening the spinner
        spnProductId.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnProductId.getWindowToken(), 0);
                return false;
            }
        });
        spnAMCid.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnAMCid.getWindowToken(), 0);
                return false;
            }
        });
        spnProductSubId.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnProductSubId.getWindowToken(), 0);
                return false;
            }
        });

        spnCountryId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerDialog_country.showSpinerDialog();

                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnCountryId.getWindowToken(), 0);
            }
        });

        spnStateId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stateObject.size() > 0) {
                    spinnerDialog_state.showSpinerDialog();
                } else {
                    Toast.makeText(RegistrationActivity.this, "Please select a country first", Toast.LENGTH_SHORT).show();
                }

                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnStateId.getWindowToken(), 0);
            }
        });

        spnCityId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cityObject.size() > 0) {
                    spinnerDialog_city.showSpinerDialog();

                } else {
                    Toast.makeText(RegistrationActivity.this, "Please select state and country first", Toast.LENGTH_SHORT).show();
                }
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnCityId.getWindowToken(), 0);
            }
        });

        spnLocationId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (locationObject.size() > 0) {
                    spinnerDialog_location.showSpinerDialog();
                }

                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(spnLocationId.getWindowToken(), 0);
            }
        });


        customernameID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtInputCustomerName.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        emailID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!Patterns.EMAIL_ADDRESS.matcher(Objects.requireNonNull(emailID.getText()).toString()).matches()) {
                    txtInputEmail.setError("Please enter valid email address ");
                } else {
                    txtInputEmail.setError(null);

                }
            }
        });

        mobilenumberID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Objects.requireNonNull(mobilenumberID.getText()).toString().length() < 10) {
                    textInputMobileNo.setError("Please enter a 10 digit mobile number ");
                } else {
                    textInputMobileNo.setError(null);
                }
            }
        });

        modelnumberID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textInputModelNo.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        serialnumberID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textInputSerialNo.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        plotnumberID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textInputPlotNo.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
      /*          Pattern plotnumber = Pattern.compile("[0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@,&#/]");
                if(!plotnumber.matcher(Objects.requireNonNull(plotnumberID.getText()).toString().trim()).find()){
                    textInputPlotNo.setError("The typed format is not right");
                }
                else
                {
                    textInputPlotNo.setError(null);
                }  */
            }
        });

        streetID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textInputStreet.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        postcodeID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Objects.requireNonNull(postcodeID.getText()).toString().length() != 6) {
                    textInputPostCode.setError("Please enter post code with 6 digits");
                } else {
                    textInputPostCode.setError(null);
                }
            }
        });


        //Registration process
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate()) {

                    //loading graphics
                    pDialog = ProgressDialog.show(RegistrationActivity.this, "FieldPro", "Loading...", true);
                    pDialog.setCancelable(true);

                    Log.e("hello1", "--------" + spnProductIdPosition);
                    Log.e("hello2", "--------" + spnProductSubIdPosition);
                    Log.e("hello3", "--------" + spnAMCidPosition);
                    Log.e("hello4", "--------" + spnCountryIdPosition);
                    Log.e("hello5", "--------" + spnStateIdPosition);
                    Log.e("hello6", "--------" + spnCityIdPosition);
                    Log.e("hello7", "--------" + spnLocationIdPosition);

                    Pref_storage.setDetail(RegistrationActivity.this, "country_id", String.valueOf(countryObject.get(spnCountryIdPosition).getCountryId()));
                    Pref_storage.setDetail(RegistrationActivity.this, "state_id", String.valueOf(stateObject.get(spnStateIdPosition).getStateId()));
                    Pref_storage.setDetail(RegistrationActivity.this, "city_id", String.valueOf(cityObject.get(spnCityIdPosition).getCityId()));
                    Pref_storage.setDetail(RegistrationActivity.this, "location_id", String.valueOf(locationObject.get(spnLocationIdPosition).getLocationId()));

                    //sending values to the json
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("customer_name", txtInputCustomerName.getEditText().getText().toString());
                    jsonObject.addProperty("email_id", txtInputEmail.getEditText().getText().toString());
                    jsonObject.addProperty("contact_number", textInputMobileNo.getEditText().getText().toString());
                    jsonObject.addProperty("alternate_number", textInputAlternateMobileNo.getEditText().getText().toString());
                    jsonObject.addProperty("product_id", productObject.get(spnProductIdPosition).getProduct_id());
                    jsonObject.addProperty("product_sub_id", subProductObject.get(spnProductSubIdPosition).getProductSubId());
                    jsonObject.addProperty("model_no", textInputModelNo.getEditText().getText().toString());
                    jsonObject.addProperty("serial_no", textInputSerialNo.getEditText().getText().toString());
                    jsonObject.addProperty("amc_id", amcObject.get(spnAMCidPosition).getAmcId());
                    jsonObject.addProperty("contract_duration", (amcObject.get(spnAMCidPosition).getDuration()));
                    jsonObject.addProperty("plot_number", textInputPlotNo.getEditText().getText().toString());
                    jsonObject.addProperty("street", Objects.requireNonNull(textInputStreet.getEditText()).getText().toString());
                    jsonObject.addProperty("post_code", Integer.parseInt(Objects.requireNonNull(textInputPostCode.getEditText()).getText().toString()));
                    jsonObject.addProperty("country_id", countryObject.get(spnCountryIdPosition + 1).getCountryId());
                    jsonObject.addProperty("state_id", stateObject.get(spnStateIdPosition + 1).getStateId());
                    jsonObject.addProperty("city_id", cityObject.get(spnCityIdPosition + 1).getCityId());
                    jsonObject.addProperty("location_id", locationObject.get(spnLocationIdPosition + 1).getLocationId());
                    jsonObject.addProperty("landmark", textInputLandmark.getEditText().getText().toString());

                    //calling the post api for registering the user
                    callRegistrationCall(jsonObject);
                }
            }
        });
    }

    public void savedata() {

        //saving the registered customer response
        Pref_storage.setDetail(RegistrationActivity.this, "customer_name", txtInputCustomerName.getEditText().getText().toString());
        Pref_storage.setDetail(RegistrationActivity.this, "email_id", txtInputEmail.getEditText().getText().toString());
        Pref_storage.setDetail(RegistrationActivity.this, "contact_number", textInputMobileNo.getEditText().getText().toString());
        Pref_storage.setDetail(RegistrationActivity.this, "alternate_number", textInputAlternateMobileNo.getEditText().getText().toString());
        Pref_storage.setDetail(RegistrationActivity.this, "product_id", spnProductId.toString());
        Pref_storage.setDetail(RegistrationActivity.this, "product_sub_id", spnProductSubId.toString());
        Pref_storage.setDetail(RegistrationActivity.this, "model_no", textInputModelNo.getEditText().getText().toString());
        Pref_storage.setDetail(RegistrationActivity.this, "serial_no", textInputSerialNo.getEditText().getText().toString());
        Pref_storage.setDetail(RegistrationActivity.this, "amc_id", spnAMCid.toString());
        Pref_storage.setDetail(RegistrationActivity.this, "contract_duration", String.valueOf(amcObject.get(spnAMCidPosition).getDuration()));
        Pref_storage.setDetail(RegistrationActivity.this, "plot_number", textInputPlotNo.getEditText().getText().toString());
        Pref_storage.setDetail(RegistrationActivity.this, "street", textInputStreet.getEditText().getText().toString());
        Pref_storage.setDetail(RegistrationActivity.this, "post_code", textInputPostCode.getEditText().getText().toString());
        Pref_storage.setDetail(RegistrationActivity.this, "country", countryObject.get(spnCountryIdPosition + 1).getCountryName());
        Pref_storage.setDetail(RegistrationActivity.this, "state", state);
        Pref_storage.setDetail(RegistrationActivity.this, "city", city);
        Pref_storage.setDetail(RegistrationActivity.this, "location", location);
        Pref_storage.setDetail(RegistrationActivity.this, "landmark", textInputLandmark.getEditText().getText().toString());

    }

    //calling the api for registering
    private void callRegistrationCall(JsonObject jsonObject) {
        Log.e("hello", "------------" + jsonObject);
        registrationController.postCustomerRegistration(apiInterface, jsonObject, this);
    }


    //calling the api for getting the products
    private void getProductItems() {
        registrationController.getProducts(apiInterface, this);
    }

    //setting the product spinner adapter
    private void setProductAdapter() {
        ArrayAdapter<String> ad = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, productItem);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnProductId.setAdapter(ad);
    }

    //calling the api for getting the sub products
    private void getSubProductItems(int product_id) {

        registrationController.getSubProducts(apiInterface, this, product_id);
    }

    //setting the sub product spinner adapter
    private void setSubProductAdapter() {
        ArrayAdapter<String> ad = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, subProductItem);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnProductSubId.setAdapter(ad);
    }

    //calling the api for getting the amc
    private void getAMCItems() {

        registrationController.getAMC(apiInterface, this);
    }

    //setting the amc spinner adapter
    private void setAMCAdapter() {
        ArrayAdapter<String> ad = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, amcItem);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnAMCid.setAdapter(ad);
    }

    //calling the api for getting the country
    private void getCountryItems() {
        registrationController.getCountry(apiInterface, this);
    }

    //setting the country spinner adapter
    private void setCountryAdapter() {
        //      ArrayAdapter<String> ad = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, countryItem);
        //      ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //      spnCountryId.setAdapter(ad);
        spinnerDialog_country = new SpinnerDialog(RegistrationActivity.this, countryItem, "select country");
        spinnerDialog_country.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String s, int position) {
                spnCountryIdPosition = position;
                countrylay.setBackgroundResource(R.drawable.bg_box_grey);
                countrylay.setPadding(23, 0, 23, 0);
                //calling state api based on the selection of country
                 {
                    stateItem.clear();
                    stateObject.clear();
                    spnStateId.setText("Select State");
                    spnStateIdPosition = 0;
                    spnCityIdPosition = 0;
                    spnLocationIdPosition = 0;
                    country = countryObject.get(position + 1).toString();
                    spnCountryId.setText(countryObject.get(spnCountryIdPosition + 1).getCountryName());
                    getStateItems(countryObject.get(position + 1).getCountryId());
                }
            }
        });
    }


    //calling the api for getting the state
    private void getStateItems(int countryId) {
        registrationController.getState(apiInterface, this, countryId);
    }

    //setting the state spinner adapter
    private void setStateAdapter() {
        //       ArrayAdapter<String> ad = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stateItem);
        //      ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //     spnStateId.setAdapter(ad);

        spinnerDialog_state = new SpinnerDialog(RegistrationActivity.this, stateItem, "Select State");

        spinnerDialog_state.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String s, int position) {
                spnStateIdPosition = position;
                //calling city api based on the selection of state
                {
                    statelay.setBackgroundResource(R.drawable.bg_box_grey);
                    statelay.setPadding(23, 0, 23, 0);
                    cityItem.clear();
                    cityObject.clear();
                    spnCityId.setText("Select City");
                    state = stateObject.get(position + 1).toString();
                    spnStateId.setText(stateObject.get(spnStateIdPosition + 1).getStateName());
                    getCityItems(stateObject.get(position + 1).getStateId());
                }


            }
        });
    }

    //calling the api for getting the city
    private void getCityItems(int stateId) {
        registrationController.getCity(apiInterface, this, stateId);
    }

    //setting the city spinner adapter
    private void setCityAdapter() {
//        ArrayAdapter<String> ad = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, cityItem);
//        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spnCityId.setAdapter(ad);

        spinnerDialog_city = new SpinnerDialog(RegistrationActivity.this, cityItem, "Select City");
        spinnerDialog_city.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String s, int position) {
                spnCityIdPosition = position;
                citylay.setBackgroundResource(R.drawable.bg_box_grey);
                citylay.setPadding(23, 0, 23, 0);
                //calling location api based on the selection of the city
                {
                    locationItem.clear();
                    locationObject.clear();
                    city = cityObject.get(position + 1).toString();
                    spnCityId.setText(cityObject.get(spnCityIdPosition + 1).getCityName());
                    getLocationItems(cityObject.get(position + 1).getCityId());
                }

            }
        });

    }

    //calling the api for getting the location
    private void getLocationItems(int city_id)
    {
        registrationController.getLocation(apiInterface, this, city_id);
    }

    //setting the location spinner adapter
    private void setLocationAdapter()
    {
        spinnerDialog_location = new SpinnerDialog(RegistrationActivity.this, locationItem, "Select Location");
        spinnerDialog_location.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String s, int position) {
                spnLocationIdPosition = position;
                locationlay.setBackgroundResource(R.drawable.bg_box_grey);
                locationlay.setPadding(23, 0, 23, 0);

                {
                    location = locationObject.get(position + 1).toString();
                    spnLocationId.setText(locationObject.get(position + 1).getLocationName());
                }

            }
        });
    }

    //setting the functionalities after a item is selected in the spinner
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.spnProductId) {
            spnProductIdPosition = position;
            //setting condition to call the sub product api
            if (position > 1) {
                subProductItem.clear();
                subProductObject.clear();
                productspinnerlay.setBackgroundResource(R.drawable.bg_box_grey);
                productspinnerlay.setPadding(23, 0, 23, 0);
                getSubProductItems(productObject.get(position).getProduct_id());
            }
        }
        if (parent.getId() == R.id.spnProductSubId) {
            spnProductSubIdPosition = position;
            subcategoryspinnerlay.setBackgroundResource(R.drawable.bg_box_grey);
            subcategoryspinnerlay.setPadding(23, 0, 23, 0);
//            if (position > 0) {
//                amcItem.clear();
//                amcObject.clear();
//                getAMCItems();
//            }
        }
        if (parent.getId() == R.id.spnAMCId) {
            spnAMCidPosition = position;
            amclay.setBackgroundResource(R.drawable.bg_box_grey);
            amclay.setPadding(23, 0, 23, 0);
            //getting the contract duration from the api and setting it
            if (position > 0) {
                conduration = String.valueOf(amcObject.get(spnAMCidPosition).getDuration());
                textInputContractDuration.setText(conduration + " months");
            }


        }
  /*      if (parent.getId() == R.id.spnCountryId) {
            spnCountryIdPosition = position;
           //calling state api based on the selection of country
            if (position > 0) {
                stateItem.clear();
                stateObject.clear();
                country = countryObject.get(position).toString();
                getStateItems(countryObject.get(position).getCountryId());
            }
        }
        if (parent.getId() == R.id.spnStateId) {
            spnStateIdPosition = position;
            //calling city api based on the selection of state
            if (position > 0) {
                cityItem.clear();
                cityObject.clear();
                state = stateObject.get(position).toString();
                getCityItems(stateObject.get(position).getStateId());
            }
        }
        if (parent.getId() == R.id.spnCityId) {
            spnCityIdPosition = position;
            //calling location api based on the selection of the city
            if (position > 0) {
                locationItem.clear();
                locationObject.clear();
                city = parent.getItemAtPosition(position).toString();
               getLocationItems(cityObject.get(position).getCityId());
            }
        }  */
        //       if (parent.getId() == R.id.spnLocationId) {
        //          spnLocationIdPosition = position;

        //          if (position > 0) {
        //              location = locationObject.get(position).getLocationName();
        //              Pref_storage.setDetail(RegistrationActivity.this,"location_position",String.valueOf(position));
        //           }
////            if (position > 0) {
//                locationItem.clear();
//                locationObject.clear();
//                getSubProductItems(productObject.get(position).getProduct_id());
//            }
        //    }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    //validating all the values, if it is empty.
    private boolean validate() {
        boolean valid = true;
        Log.e("hello1", "--------" + spnProductIdPosition);
        Log.e("hello2", "--------" + spnProductSubIdPosition);
        Log.e("hello3", "--------" + spnAMCidPosition);
        Log.e("hello4", "--------" + spnCountryIdPosition);
        Log.e("hello5", "--------" + spnStateIdPosition);
        Log.e("hello6", "--------" + spnCityIdPosition);
        Log.e("hello7", "--------" + spnLocationIdPosition);
        Log.e("hamster","----------"+spnCountryId.getText().toString());


        if (Objects.requireNonNull(customernameID.getText()).toString().isEmpty()) {
            txtInputCustomerName.setError("Please enter customer name ");
            customernameID.requestFocus();
            valid = false;
        }


        if (Objects.requireNonNull(emailID.getText()).toString().isEmpty()) {
            txtInputEmail.setError("Please enter Email ");
            emailID.requestFocus();
            valid = false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailID.getText().toString()).matches()) {
            txtInputEmail.setError("Please enter valid email address ");
            valid = false;
        }


        if ((Objects.requireNonNull(mobilenumberID.getText()).toString().isEmpty())) {
            textInputMobileNo.setError("Please enter Mobile no ");
            mobilenumberID.requestFocus();
            valid = false;
        } else if (Objects.requireNonNull(mobilenumberID.getText()).toString().length() != 10) {
            textInputMobileNo.setError("Please enter a 10 digit mobile number ");
            valid = false;
        }

        //changing the layout for spinners to red if they are not selected
        if (spnProductIdPosition == 0) {
            productspinnerlay.setBackgroundResource(R.drawable.boarder);
            productspinnerlay.setPadding(23, 0, 23, 0);
            spnProductId.requestFocus();
            valid = false;
        }


        if (spnProductSubIdPosition == 0) {
            subcategoryspinnerlay.setBackgroundResource(R.drawable.boarder);
            subcategoryspinnerlay.setPadding(23, 0, 23, 0);
            spnProductSubId.requestFocus();
            valid = false;
        }


  /*      if  (textInputAlternateMobileNo.getEditText().getText().toString().equals("")) {
            textInputAlternateMobileNo.setError("Please enter Model no ");
            valid = false;
        }
        else {
            textInputAlternateMobileNo.setError(null);
            valid = true;
        } */

        if (Objects.requireNonNull(modelnumberID.getText()).toString().isEmpty()) {
            textInputModelNo.setError("Please enter model number ");
            modelnumberID.requestFocus();
            valid = false;
        }


        if ((Objects.requireNonNull((serialnumberID).getText())).toString().isEmpty()) {
            textInputSerialNo.setError("Please enter serial no ");
            serialnumberID.requestFocus();
            valid = false;
        }

        if (spnAMCidPosition == 0) {
            amclay.setBackgroundResource(R.drawable.boarder);
            amclay.setPadding(23, 0, 23, 0);
            spnAMCid.requestFocus();
            valid = false;
        }


        //   if (textInputContractDuration.getEditText().getText().toString().equals("")) {
        //        textInputContractDuration.setError("Please enter contract duration ");
        //         valid = false;
        //     }

        if (Objects.requireNonNull(Objects.requireNonNull(plotnumberID).getText()).toString().isEmpty()) {
            textInputPlotNo.setError("Please enter input plot no ");
            plotnumberID.requestFocus();
            valid = false;
        }


  /*      Pattern plotnumber = Pattern.compile("[0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@,&#/]");
        if(!plotnumber.matcher(Objects.requireNonNull(plotnumberID.getText()).toString().trim()).find())
       {
            textInputPlotNo.setError("The typed format is not right");
           plotnumberID.requestFocus();
           valid = false;
        }  */


        if (Objects.requireNonNull(streetID.getText()).toString().isEmpty()) {
            textInputStreet.setError("Please enter street no ");
            streetID.requestFocus();
            valid = false;
        }


        if (Objects.requireNonNull(postcodeID.getText()).toString().isEmpty()) {
            textInputPostCode.setError("Please enter post code ");
            postcodeID.requestFocus();
            valid = false;
        } else if (Objects.requireNonNull(postcodeID.getText()).toString().length() != 6) {
            textInputPostCode.setError("Please enter post code with 6 digits");
            valid = false;
        }

        if (spnCountryId.getText().toString().equals("Select Country")) {
            countrylay.setBackgroundResource(R.drawable.boarder);
            countrylay.setPadding(23, 0, 23, 0);
            spnCountryId.requestFocus();
            valid = false;
        }


        if (spnStateId.getText().toString().equals("Select State")) {
            statelay.setBackgroundResource(R.drawable.boarder);
            statelay.setPadding(23, 0, 23, 0);
            spnStateId.requestFocus();
            valid = false;
        }


        if (spnCityId.getText().toString().equals("Select City"))
        {
            citylay.setBackgroundResource(R.drawable.boarder);
            citylay.setPadding(23, 0, 23, 0);
            spnCityId.requestFocus();
            valid = false;
        }


        if (spnLocationId.getText().toString().equals("Select Location")) {
            locationlay.setBackgroundResource(R.drawable.boarder);
            locationlay.setPadding(23, 0, 23, 0);
            spnLocationId.requestFocus();
            valid = false;
        }


        return valid;
    }


    @Override
    public void onSuccess(ProductResponse value) {
        //if the response is not null we assign the values to the spinner
        if (value.getResponse() != null) {
            Log.e("test", "--->" + new Gson().toJson(value.getResponse().getData()));
            productObject.add(value.getResponse().getData().get(0));
            productItem.add("Select Product");

            for (Datum datum : value.getResponse().getData()) {
                //storing the values to set it in the product adapter
                productObject.add(datum);
                productItem.add(datum.getProduct_name());
                Log.e("test", "--->" + new Gson().toJson(productItem));
            }
            setProductAdapter();
        }

    }

    @Override
    public void onSuccess(SubProductResponse value) {
        //if the response is not null we assign the values to the spinner
        if (value.getResponse() != null) {
            //if the product does not have any sub product then it does not open
            if (value.getResponse().getData().size() > 0) {
                subProductObject.add(value.getResponse().getData().get(0));
                subProductItem.add("Select Sub Product");

                for (DataItem datum : value.getResponse().getData()) {
                    subProductObject.add(datum);
                    subProductItem.add(datum.getProductSubName());
                }
                setSubProductAdapter();
            }
            else {
                subProductItem.add("There are no sub product for this product");
                setSubProductAdapter();
                Toast.makeText(RegistrationActivity.this, "this product does not have a sub product", Toast.LENGTH_SHORT).show();
            }
        } else {
            subProductItem.add("There are no sub product for this product");
            setSubProductAdapter();
            Toast.makeText(RegistrationActivity.this, "this product does not have a sub product", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onSuccess(AMCResponse value) {
        //if the response is not null we assign the values to the spinner
        if (value.getResponse() != null) {

            amcObject.add(value.getResponse().getData().get(0));
            amcItem.add("Select AMC Type");
            contractduration_object.add(value.getResponse().getData().get(0));

            for (com.fieldprov2.fieldprocustomer.model.amc_response.DataItem datum : value.getResponse().getData()) {
                amcObject.add(datum);
                amcItem.add(datum.getAmcType());
                contractduration_item.add(datum.getDuration());
                Log.e("master", "-------" + contractduration_item.add(datum.getDuration()));

            }

            setAMCAdapter();
        }
    }

    @Override
    public void onSuccess(CountryResponse value) {
        //if the response is not null we assign the values to the spinner
        if (value.getResponse() != null) {

            countryObject.add(value.getResponse().getData().get(0));
            //           countryItem.add("Select Country");

            for (com.fieldprov2.fieldprocustomer.model.country_response.DataItem datum : value.getResponse().getData()) {
                countryObject.add(datum);
                countryItem.add(datum.getCountryName());
            }
            setCountryAdapter();
        }

    }

    @Override
    public void onSuccess(StateResponse value) {
        //if the response is not null we assign the values to the spinner
        if (value.getResponse() != null) {
            if (value.getResponse().getData().size() > 0) {
                stateObject.add(value.getResponse().getData().get(0));
                //         stateItem.add("Select State");

                for (com.fieldprov2.fieldprocustomer.model.state_res.DataItem datum : value.getResponse().getData()) {
                    stateObject.add(datum);
                    stateItem.add(datum.getStateName());
                }
                setStateAdapter();
            } else {
                stateItem.add("This country does not have any state with it");
                setStateAdapter();
                Toast.makeText(RegistrationActivity.this, "this product does not have a sub product", Toast.LENGTH_SHORT).show();
            }

        } else {
            stateItem.add("This country does not have any state with it");
            setStateAdapter();
            Toast.makeText(RegistrationActivity.this, "this product does not have a sub product", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSuccess(CityResponse value) {
        //if the response is not null we assign the values to the spinner
        if (value.getResponse() != null) {

            if (value.getResponse().getData().size() > 0) {

                cityObject.add(value.getResponse().getData().get(0));
//            cityItem.add("Select City");

                for (com.fieldprov2.fieldprocustomer.model.city_res.DataItem datum : value.getResponse().getData()) {
                    cityObject.add(datum);
                    cityItem.add(datum.getCityName());
                }
                setCityAdapter();
            } else {
                cityItem.add("No City");
                setCityAdapter();
                Toast.makeText(RegistrationActivity.this, "this state does not have a city", Toast.LENGTH_SHORT).show();
            }

        } else {
            cityItem.add("No city");
            setCityAdapter();
            Toast.makeText(RegistrationActivity.this, "this state does not have a city", Toast.LENGTH_SHORT).show();
        }
    }




    @Override
    public void onSuccess(LocationResponse value)
    {
        //if the response is not null we assign the values to the spinner
        if (value.getResponse() != null ) {
            if (value.getResponse().getData().size() > 0) {

                locationObject.add(value.getResponse().getData().get(0));
      //          locationItem.add("Select Location ");

                for (com.fieldprov2.fieldprocustomer.model.location_res.DataItem datum : value.getResponse().getData()) {
                    locationObject.add(datum);
                    locationItem.add(datum.getLocationName());
                }
                setLocationAdapter();
            }
            else
            {
                locationItem.add("This state does have a location");
                setLocationAdapter();
                Toast.makeText(RegistrationActivity.this,"This city does not have a location",Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            locationItem.add("This state does have a location");
            setLocationAdapter();
            Toast.makeText(RegistrationActivity.this,"This city does not have a location",Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onSuccess(RegistrationResponse value) {
        //if the response is not null we assign the values to the spinner
        if (value.getResponse() != null)
        {
            if(value.getResponse().getResponseCode().equals("200") )
            {
                Toast.makeText(this, value.getResponse().getMessage(), Toast.LENGTH_SHORT).show();
                registration_token = value.getResponse().getToken();
                Pref_storage.setDetail(RegistrationActivity.this,"Token",registration_token);
                savedata();
                Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                startActivity(intent);
                pDialog.dismiss();
            }
            if(value.getResponse().getResponseCode().equals("500"))
            {
                Toast.makeText(this,value.getResponse().getMessage(),Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }

        }
        else
        {
            pDialog.dismiss();
        }
    }

    @Override
    public void onError(Throwable throwable) {
        pDialog.dismiss();
    }
}

