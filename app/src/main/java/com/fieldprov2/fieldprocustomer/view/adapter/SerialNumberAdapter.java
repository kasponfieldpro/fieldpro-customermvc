package com.fieldprov2.fieldprocustomer.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fieldprov2.fieldprocustomer.R;

import java.util.List;

public class SerialNumberAdapter extends RecyclerView.Adapter<SerialNumberAdapter.ViewHolder> {

    private List<String> list;
    private Context context;
    private DeleteInterface deleteInterface;

    public SerialNumberAdapter(List<String> list, Context context, DeleteInterface deleteInterface) {
        this.list = list;
        this.context = context;
        this.deleteInterface = deleteInterface;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_serial_number, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        holder.txtTitle.setText("" + list.get(position));
        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteInterface.onDelete(list.size(),position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTitle;
        public ImageView imgDelete;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtTitle = itemView.findViewById(R.id.txtTitle);
            imgDelete = itemView.findViewById(R.id.imgDelete);

        }
    }

    public interface DeleteInterface {
        void onDelete(int size, int position);
    }

}
