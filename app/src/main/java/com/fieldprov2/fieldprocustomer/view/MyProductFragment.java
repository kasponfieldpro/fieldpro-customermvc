package com.fieldprov2.fieldprocustomer.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.fieldprov2.fieldprocustomer.R;
import com.fieldprov2.fieldprocustomer.controller.ProductListController;
import com.fieldprov2.fieldprocustomer.controller.TicketListController;
import com.fieldprov2.fieldprocustomer.model.Products;
import com.fieldprov2.fieldprocustomer.model.product_list.Datum;
import com.fieldprov2.fieldprocustomer.model.product_list.ProductListResponse;
import com.fieldprov2.fieldprocustomer.model.ticket_list.TicketListResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;
import com.fieldprov2.fieldprocustomer.network.RetrofitClient;
import com.fieldprov2.fieldprocustomer.view.adapter.MyProductAdapter;
import com.fieldprov2.fieldprocustomer.view.adapter.TicketHistoryAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MyProductFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyProductFragment extends Fragment implements ProductListController.ProductListResCallbacks {
    ApiInterface apiInterface;
    ProductListController productListController;
    private String email;
    private String token,myProducts_token;
    SwipeRefreshLayout pullToRefresh_addproduct;
    ProgressDialog pDialog ;


    public MyProductFragment() {
        // Required empty public constructor
    }

    RecyclerView recyclerView;


    public static MyProductFragment newInstance()
    {
        return new MyProductFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Objects.requireNonNull(getActivity()).setTitle("My Product");

        pDialog = ProgressDialog.show(getActivity(), "FieldPro","Loading...", true);
        pDialog.setCancelable(true);

    }

    //calling the api
    private void getProductList()
    {
        token = Pref_storage.getDetail(getActivity(),"Token");
        email = Pref_storage.getDetail(getActivity(), "username1");
        productListController.getProductList(apiInterface,this,token,email);
    }


    //code for the recycler view
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_product, container, false);

        pullToRefresh_addproduct = view.findViewById(R.id.pullToRefresh_addproduct);

        pullToRefresh_addproduct.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullToRefresh_addproduct.setRefreshing(true);
                pullToRefresh_addproduct.setRefreshing(false);
            }
        });

        recyclerView = view.findViewById(R.id.rv);
        apiInterface = RetrofitClient.getClient().create(ApiInterface.class);
        productListController = new ProductListController();
        getProductList();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);


        ArrayList<Products> list = new ArrayList<Products>();
 //       list.add(new Products("ENGG.PLOTTERS", "HP DNJ", "test serial 1", "test model 1", "AMC"));
  //      list.add(new Products("ENGG.PLOTTERS", "HP DNJ", "test serial 2", "test model 2", "AMC"));
  //      list.add(new Products("ENGG.PLOTTERS", "HP DNJ", "test serial 3", "test model 3", "AMC"));
 //       list.add(new Products("ENGG.PLOTTERS", "HP DNJ", "test serial 4", "test model 4", "AMC"));
 //       list.add(new Products("ENGG.PLOTTERS", "HP DNJ", "test serial 5", "test model 5", "AMC"));



    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.addproduct_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {

        switch (item.getItemId())
        {
            case R.id.addProductMenu:
                {
                Intent intent = new Intent(getActivity(), AddProductActivity.class);
                startActivity(intent);
            }
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onSuccess(ProductListResponse value)
    {
        if (value.getResponse() != null && value.getResponse().getResponseCode().equals("200")) {
            List<Datum> data = value.getResponse().getData();
            //getting the values and setting them in the adapter
            LinearLayoutManager linearLayoutManager  = new LinearLayoutManager(getActivity());
            linearLayoutManager.setStackFromEnd(true);
            linearLayoutManager.setReverseLayout(true);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setHasFixedSize(true);
            MyProductAdapter myProductAdapter = new MyProductAdapter(data, getActivity());
            recyclerView.setAdapter(myProductAdapter);
            myProducts_token = value.getResponse().getToken();
            Pref_storage.setDetail(getActivity(),"Token",myProducts_token);
            pDialog.dismiss();
        }
        else if(value.getResponse().getResponseCode().equals("400"))
        {
            pDialog.dismiss();
            myProducts_token = value.getResponse().getToken();
            Pref_storage.setDetail(getActivity(),"Token",myProducts_token);
            Toast.makeText(getActivity(),"No data to display",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onError(Throwable throwable) {
        pDialog.dismiss();

    }
}