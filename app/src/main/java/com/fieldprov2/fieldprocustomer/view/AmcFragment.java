package com.fieldprov2.fieldprocustomer.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.fieldprov2.fieldprocustomer.R;
import com.fieldprov2.fieldprocustomer.controller.AMCListController;
import com.fieldprov2.fieldprocustomer.model.Amc;
import com.fieldprov2.fieldprocustomer.model.amc_list.AMCListResponse;
import com.fieldprov2.fieldprocustomer.model.amc_list.Datum;
import com.fieldprov2.fieldprocustomer.model.product_list.ProductListResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;
import com.fieldprov2.fieldprocustomer.network.RetrofitClient;
import com.fieldprov2.fieldprocustomer.view.adapter.AMCAdapter;
import com.fieldprov2.fieldprocustomer.view.adapter.MyProductAdapter;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AmcFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AmcFragment extends Fragment implements AMCListController.AMCListResCallbacks {

    ProgressDialog pDialog ;
    ApiInterface apiInterface;
AMCListController amcListController;
    private String cus_code;
    SwipeRefreshLayout pullToRefresh_addamc;
    private String token,myProducts_token;


    public AmcFragment() {
        // Required empty public constructor
    }

    RecyclerView recyclerView;

    public static AmcFragment newInstance()
    {
        return new AmcFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Objects.requireNonNull(getActivity()).setTitle("AMC");

        pDialog = ProgressDialog.show(getActivity(), "FieldPro","Loading...", true);
        pDialog.setCancelable(true);
    }

    //calling the api
    private void jsonobject()
    {
        JsonObject jsonObject = new JsonObject();
        cus_code = Pref_storage.getDetail(getActivity(), "customer_code");
        jsonObject.addProperty("customer_code", cus_code);
        getAMCList(jsonObject);

    }

    private void getAMCList(JsonObject jsonObject)
    {
        token = Pref_storage.getDetail(getActivity(),"Token");
        amcListController.getAMCList(apiInterface,jsonObject,this,token);
    }

    //code for the adapter
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_amc, container, false);

        pullToRefresh_addamc = view.findViewById(R.id.pullToRefresh_addamc);

        pullToRefresh_addamc.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullToRefresh_addamc.setRefreshing(true);
                pullToRefresh_addamc.setRefreshing(false);
            }
        });

        recyclerView = view.findViewById(R.id.rv);
        apiInterface = RetrofitClient.getClient().create(ApiInterface.class);
        amcListController = new AMCListController();
        jsonobject();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArrayList<Amc> list = new ArrayList<Amc>();
 //       list.add(new Amc("ENGG.PLOTTERS", "HP DNJ", "test serial 1", "test model 1"));
//        list.add(new Amc("ENGG.PLOTTERS", "HP DNJ", "test serial 2", "test model 2"));
//        list.add(new Amc("ENGG.PLOTTERS", "HP DNJ", "test serial 3", "test model 3"));
//        list.add(new Amc("ENGG.PLOTTERS", "HP DNJ", "test serial 4", "test model 4"));
 //       list.add(new Amc("ENGG.PLOTTERS", "HP DNJ", "test serial 5", "test model 5"));


    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.addproduct_menu, menu);

    }

    //moving to the add product page
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.addProductMenu: {
                Intent intent = new Intent(getActivity(), AddAmcActivity.class);
                startActivity(intent);
            }
        }
        return super.onOptionsItemSelected(item);

    }

    //writing the response code
    @Override
    public void onSuccess(AMCListResponse value)
    {
        if (value.getResponse() != null && value.getResponse().getResponseCode().equals("200")) {
            List<Datum> data = value.getResponse().getData();
            //setting the values in the adapter
            LinearLayoutManager linearLayoutManager  = new LinearLayoutManager(getActivity());
            linearLayoutManager.setStackFromEnd(true);
            linearLayoutManager.setReverseLayout(true);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setHasFixedSize(true);
            AMCAdapter amcAdapter = new AMCAdapter(data, getActivity());
            recyclerView.setAdapter(amcAdapter);
            myProducts_token = value.getResponse().getToken();
            Pref_storage.setDetail(getActivity(),"Token",myProducts_token);
            pDialog.dismiss();
        }
        else if(value.getResponse().getResponseCode().equals("400"))
        {
            pDialog.dismiss();
            myProducts_token = value.getResponse().getToken();
            Pref_storage.setDetail(getActivity(),"Token",myProducts_token);
            Toast.makeText(getActivity(),"No data to display",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onError(Throwable throwable) {
        pDialog.dismiss();

    }
}