package com.fieldprov2.fieldprocustomer.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fieldprov2.fieldprocustomer.R;
import com.fieldprov2.fieldprocustomer.model.Amc;
import com.fieldprov2.fieldprocustomer.model.amc_list.Datum;

import java.util.ArrayList;
import java.util.List;

public class AMCAdapter extends RecyclerView.Adapter<AMCAdapter.ViewHolder> {

    private List<Datum> list;
    private Context context;

    public AMCAdapter(List<Datum> ticketsList, Context context) {
        list = ticketsList;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_amc_list, parent, false));
    }

    //adding the product from the response to the right value
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        holder.txtProduct.setText("" + list.get(position).getProduct());
        holder.txtSubCategory.setText("" + list.get(position).getSubProduct());
        holder.txtAMCType.setText("" + list.get(position).getAmcType());
        holder.txtSerialNumber.setText("" + list.get(position).getSerialNo());
        if(list.get(position).getFlag() == 1)
        {
            holder.txtStatus.setText(""+list.get(position).getDaysLeft());
        }
        else
        {
            holder.txtStatus.setText("Not live");
        }

    }

    //code for the adapter
    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtProduct, txtSubCategory, txtAMCType, txtSerialNumber,txtStatus;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtProduct = itemView.findViewById(R.id.txtProduct);
            txtSubCategory = itemView.findViewById(R.id.txtSubCategory);
            txtAMCType = itemView.findViewById(R.id.txtAMCType);
            txtSerialNumber = itemView.findViewById(R.id.txtSerialNumber);
            txtStatus = itemView.findViewById(R.id.txtStatus);
        }
    }


}
