package com.fieldprov2.fieldprocustomer.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.fieldprov2.fieldprocustomer.controller.LogoutController;
import com.fieldprov2.fieldprocustomer.model.logout.LogoutResponse;
import com.fieldprov2.fieldprocustomer.model.profile_activity.ProfileActivityResponse;
import com.fieldprov2.fieldprocustomer.network.ApiInterface;
import com.fieldprov2.fieldprocustomer.network.RetrofitClient;
import com.fieldprov2.fieldprocustomer.view.login.LoginActivity;
import com.fieldprov2.fieldprocustomer.view.profile.ProfileActivity;

public class
LogoutDialogBox extends AppCompatDialogFragment implements LogoutController.LogoutResCallbacks {

    private Activity activity;

    public LogoutDialogBox(Activity activity) {
        this.activity = activity;
    }

    LogoutController logoutController;
    ApiInterface apiInterface;
    private String username11;


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState)
    {
        apiInterface = RetrofitClient.getClient().create(ApiInterface.class);
        logoutController = new LogoutController();


        //creating values for the logout dialog box
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Logout")
                .setMessage("Are you sure you want to Logout")
                .setNeutralButton("No", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {

                    }
                }
                )
                .setPositiveButton("yes", new DialogInterface.OnClickListener()
                {
                   @Override
                    public void onClick(DialogInterface dialog, int which)
                   {
                       Pref_storage.setDetail(getActivity(), "username1", null);
                       getLogout(activity);
                   }
                });
                return builder.create();
    }
    private void getLogout(Activity activity)
    {
        username11 = Pref_storage.getDetail(getActivity(),"username1");
        logoutController.getLogout(apiInterface, this, username11);
    }



    @Override
    public void onSuccess(LogoutResponse value)
    {
        Pref_storage.allclear(activity);
      //  Toast.makeText(getActivity(), value.getResponse().getMessage(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }
    @Override
    public void onError(Throwable throwable)
    {

    }
}
